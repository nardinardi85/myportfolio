﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesBehaviour : MonoBehaviour
{
    private float timer;
    public float minCD = 0.5f;
    public float maxCD = 2.5f;
    
    public int damage;
    
    public int hp;
    public float speed;
    public float speedMod = 200f;

    public Rigidbody2D rb;

    public GameObject bulletPrefab;
    public Transform bulletSpawner;

    public bool isAsteroid;
    public bool isMeteor;
    public bool isSatellite;
    public bool isDog;
    public bool isMouse;
    public bool isSheep;

    [Space]
    public GameObject[] drops;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        timer = Random.Range(minCD, maxCD);
        rb = GetComponent<Rigidbody2D>();
        if (isAsteroid)
        {
            float _speed = (gameManager.energyBarLvl / speedMod);

            speed += _speed;
        }

        rb.velocity = transform.right * -speed;
    }

    private void Update()
    {
        if(gameManager.isCharging)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {                

                if (isDog)
                {
                    GameObject enemyBullet = Instantiate(bulletPrefab, bulletSpawner.position, Quaternion.identity);
                }
                else if(isMouse)
                {
                    GameObject enemyBullet = Instantiate(bulletPrefab, bulletSpawner.position, Quaternion.identity);
                }
                else if (isSheep)
                {
                    List<Vector3> randomSheepBulletSpawns = new List<Vector3>();
                    randomSheepBulletSpawns.Add(new Vector3 (gameObject.transform.position.x, gameManager.railTop.transform.position.y, gameManager.railTop.transform.position.z));
                    randomSheepBulletSpawns.Add(new Vector3(gameObject.transform.position.x, gameManager.railMid.transform.position.y, gameManager.railMid.transform.position.z));
                    randomSheepBulletSpawns.Add(new Vector3(gameObject.transform.position.x, gameManager.railBottom.transform.position.y, gameManager.railBottom.transform.position.z));
                    GameObject enemyBullet = Instantiate(bulletPrefab, randomSheepBulletSpawns[Random.Range(0, randomSheepBulletSpawns.Count)], Quaternion.identity);
                    enemyBullet.GetComponent<BulletBehaviour>().isSheep = true;
                }

                timer = Random.Range(minCD, maxCD);
            }
        }


        if(hp < 0)
        {
            if(drops != null && Random.Range(0, 1000f) < 300f)
            {
                int rng = Random.Range(0, drops.Length);
                Instantiate(drops[rng], transform.position, Quaternion.identity);
            }



            Destroy(gameObject);
        }
    }
}
