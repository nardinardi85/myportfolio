﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndMapBulletDestroyer : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<BulletBehaviour>() != null)
        {
            if (collision.gameObject.GetComponent<BulletBehaviour>().isCat) Destroy(collision.gameObject);
        }
    }
}
