﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour
{
    public List<SpriteRenderer> panels = new List<SpriteRenderer>();
    
    [Header("Background Runner")]
    public Sprite backgroundRunner_A1;
    public Sprite backgroundRunner_A2;
    public Sprite backgroundRunner_A3;
    public Sprite backgroundRunner_A4;
    public Sprite backgroundRunner_A5;
    [Space]
    public Sprite backgroundRunner_A6;
    public Sprite backgroundRunner_A7;
    public Sprite backgroundRunner_A8;
    public Sprite backgroundRunner_A9;
    public Sprite backgroundRunner_A10;
    [Space]
    public Sprite backgroundRunner_B1;
    public Sprite backgroundRunner_B2;
    public Sprite backgroundRunner_B3;
    public Sprite backgroundRunner_B4;
    [Space]
    public Sprite backgroundRunner_B5;
    public Sprite backgroundRunner_B6;
    public Sprite backgroundRunner_B7;
    public Sprite backgroundRunner_B8;


    [Header("Background Shooter")]
    public Sprite backgroundShooterOne;
    public Sprite backgroundShooterTwo;
    public Sprite backgroundShooterTree;

    [Header("Planets")]
    public Sprite planet_1;
    public Sprite planet_2;
    public Sprite planet_3;
    public Sprite planet_4;
    public Sprite planet_5;
    public Sprite planet_6;
    public Sprite planet_7;
    public Sprite planet_8;

    private GameManager gm;

    private float speed;
    private int distanceCounter;

    private List<List<Sprite>> bgsRunnerLists = new List<List<Sprite>>();

    private List<Sprite> bgsRunnerList_A1 = new List<Sprite>();
    private List<Sprite> bgsRunnerList_A2 = new List<Sprite>();
    private List<Sprite> bgsRunnerList_B1 = new List<Sprite>();
    private List<Sprite> bgsRunnerList_B2 = new List<Sprite>();
    private List<Sprite> bgsRunnerList_B3 = new List<Sprite>();
    private List<Sprite> bgsRunnerList_B4 = new List<Sprite>();

    private List<Sprite> currentBgList = new List<Sprite>();


    private List<Sprite> bgsShooterList = new List<Sprite>();
    private List<Sprite> planetsList = new List<Sprite>();

    
    private float heightCamera;
    private float widthCamera;

    private Vector3 PositionCam;
    private Camera cam;
    private Transform resetPosition;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();

        cam = Camera.main;
        heightCamera = 2f * cam.orthographicSize;
        widthCamera = heightCamera * cam.aspect;

        PopulateLists();
        ChooseBackground();

        panels[0].sprite = currentBgList[0];
        panels[1].sprite = currentBgList[1];
        
    }

    // Update is called once per frame
    void Update()
    {
        speed = gm.backgroundSpeed;
        MoveTheBg();
    }

    
    private void MoveTheBg()
    {
        for (int i = 0; i < panels.Count; i++)
        {
            if (panels[i].transform.position.x + panels[i].bounds.size.x / 2 < cam.transform.position.x - widthCamera / 2)
            {
                SpriteRenderer p = panels[0];
                foreach (var pan in panels)
                {
                    if (pan.transform.position.x > p.transform.position.x)
                    {
                        p = pan;
                    }
                }

                if(i == 0) ChangeSprite(panels[i], 1);
                else ChangeSprite(panels[i], 0);
                
                panels[i].transform.position = new Vector3((p.transform.position.x + (p.bounds.size.x / 2) + (panels[i].bounds.size.x / 2)), p.transform.position.y, 20f);
                if(gm.started)
                {
                    distanceCounter++;
                    gm.distance = distanceCounter;
                }

            }

            panels[i].transform.Translate(new Vector3(Time.deltaTime * speed * -1, 0, 0));
        }
    }

    private void ChangeSprite(SpriteRenderer sr, int index)
    {
        if(panels[index].sprite == currentBgList[currentBgList.Count -1])
        {
            ChooseBackground();
            sr.sprite = currentBgList[0];
        }
        else
        {
            for (int i = 0; i < currentBgList.Count; i++)
            {
                if (panels[index].sprite == currentBgList[i])
                {
                    sr.sprite = currentBgList[i + 1];
                }
            }
        }
    }

    private void ChooseBackground()
    {
        currentBgList.Clear();
        int _listIndex = Random.Range(0, bgsRunnerLists.Count);

        foreach (var s in bgsRunnerLists[_listIndex])
        {
            currentBgList.Add(s);
        }

    }


    private void PopulateLists()
    {
        //populate the lists
        bgsRunnerList_A1.Add(backgroundRunner_A1);
        bgsRunnerList_A1.Add(backgroundRunner_A2);
        bgsRunnerList_A1.Add(backgroundRunner_A3);
        bgsRunnerList_A1.Add(backgroundRunner_A4);
        bgsRunnerList_A1.Add(backgroundRunner_A5);

        bgsRunnerList_A2.Add(backgroundRunner_A6);
        bgsRunnerList_A2.Add(backgroundRunner_A7);
        bgsRunnerList_A2.Add(backgroundRunner_A8);
        bgsRunnerList_A2.Add(backgroundRunner_A9);
        bgsRunnerList_A2.Add(backgroundRunner_A10);

        bgsRunnerList_B1.Add(backgroundRunner_B1);
        bgsRunnerList_B1.Add(backgroundRunner_B2);

        bgsRunnerList_B2.Add(backgroundRunner_B3);
        bgsRunnerList_B2.Add(backgroundRunner_B4);

        bgsRunnerList_B3.Add(backgroundRunner_B5);
        bgsRunnerList_B3.Add(backgroundRunner_B6);

        bgsRunnerList_B4.Add(backgroundRunner_B7);
        bgsRunnerList_B4.Add(backgroundRunner_B8);

        bgsRunnerLists.Add(bgsRunnerList_A1);
        bgsRunnerLists.Add(bgsRunnerList_A2);
        bgsRunnerLists.Add(bgsRunnerList_B1);
        bgsRunnerLists.Add(bgsRunnerList_B2);
        bgsRunnerLists.Add(bgsRunnerList_B3);
        bgsRunnerLists.Add(bgsRunnerList_B4);




        bgsShooterList.Add(backgroundShooterOne);
        bgsShooterList.Add(backgroundShooterTwo);
        bgsShooterList.Add(backgroundShooterTree);

        planetsList.Add(planet_1);
        planetsList.Add(planet_2);
        planetsList.Add(planet_3);
        planetsList.Add(planet_4);
        planetsList.Add(planet_5);
        planetsList.Add(planet_6);
        planetsList.Add(planet_7);
        planetsList.Add(planet_8);
    }
}
