﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject playerPref;
    [Space]
    public Slider energyBar;
    [Space]
    public Text distanceText;
    [Space]
    public Text scoreText;
    [Space]

    public float energyBarLvl;
    public float clickValue;
    public float holdvalue;
    public float decreaseValue = 1f;
    public float decreaseTimer = 0.05f;
    
    [Space]
    public float shootingPhaseSpeed;
    [Space]
    public GameObject deathPanel;
    [Space]
    public GameObject button;

    [Space]
    public Text deathDistance;
    [Space]
    public Text deathCollectable;

    [Header("Binari")]
    public GameObject railTop;
    public GameObject railMid;
    public GameObject railBottom;

    [Space]
    public GameObject heartsPanel;

    [Space]
    public bool started;
    //Per testing
    public bool isCharging = false;

    [HideInInspector]
    public float backgroundSpeed;

    private float timer;
    private GameObject cat;
    private CatMovement catMovement;

    public List<Transform> railsList = new List<Transform>();

    public float distance;
    public float score;
    
    public Image[] hearts;
    float filling;
    public float fillingLerpValue = 0.2f;

    private readonly int maxHearts = 3;
    [HideInInspector]
    public int currentHearts;

    private int _currentHearts;

    private void Awake()
    {
        Time.timeScale = 1;

        currentHearts = 3;
        _currentHearts = currentHearts;

        energyBarLvl = 250f;
        foreach (Image heart in hearts)
        {
            heart.fillAmount = 1;
        }

        distance = 0;

        GetRailsPosition();

        cat = Instantiate(playerPref, railMid.transform.position, Quaternion.identity);
        catMovement = cat.GetComponent<CatMovement>();
        catMovement.pos = CatMovement.Position.Mid;

        started = false;
        timer = decreaseTimer;

        heartsPanel.SetActive(false);
    }
  

    void Update()
    {
        CalculateBackgroundSpeed();
        CheckCharging();

        if (started)
        {            
            if (isCharging)
            {
                if (!heartsPanel.activeSelf)
                {
                    currentHearts = _currentHearts;
                }

                heartsPanel.SetActive(true);

                if (currentHearts > maxHearts)
                {
                    currentHearts = maxHearts;
                }
                if (currentHearts < 0)
                {
                    currentHearts = 0;
                }
            }
            else
            {
                if (heartsPanel.activeSelf)
                {
                    energyBarLvl = 500f;
                    _currentHearts = currentHearts;
                }

                heartsPanel.SetActive(false);

                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    energyBarLvl -= decreaseValue;
                    timer = decreaseTimer;
                }
            }
            UpdateEnergyBar();
            DeathConditions();
        }

        UpdateDistace();
        UpgradeScore();


    }


    private void CheckCharging()
    {
        if(SystemInfo.batteryStatus == BatteryStatus.Charging)
        {
            isCharging = true;
        }

        if(SystemInfo.batteryStatus == BatteryStatus.Discharging)
        {
            isCharging = false;
        }
    }

    private void DeathConditions()
    {
        if(!isCharging && energyBarLvl <= 0)
        {
            Debug.Log("Son morto");
            Time.timeScale = 0;
            deathPanel.SetActive(true);
            button.SetActive(false);
            UpdateDeathText();
        }
        if (isCharging && currentHearts <= 0)
        {
            Debug.Log("Son morto");
            Time.timeScale = 0;
            deathPanel.SetActive(true);
            button.SetActive(false);
            UpdateDeathText();
        }
    }


    private void UpdateDeathText()
    {
        float _panelDist = 148f;

        float _distance = (_panelDist * distance) / 100f;

        deathDistance.text = _distance.ToString("F2");

        deathCollectable.text = score.ToString();
    }

    public void UpdateHearts()
    {
        switch (currentHearts)
        {
            case 3:
                hearts[0].fillAmount = 1;
                hearts[1].fillAmount = 1;
                hearts[2].fillAmount = 1;
                break;
            case 2:
                hearts[0].fillAmount = 1;
                hearts[1].fillAmount = 1;
                hearts[2].fillAmount = 0;
                break;
            case 1:
                hearts[0].fillAmount = 1;
                hearts[1].fillAmount = 0;
                hearts[2].fillAmount = 0;
                break;
            case 0:
                hearts[0].fillAmount = 0;
                hearts[1].fillAmount = 0;
                hearts[2].fillAmount = 0;
                DeathConditions();
                break;
        }
    }

    private void UpdateEnergyBar()
    {
        energyBar.value = energyBarLvl;
    }

    private  void GetRailsPosition()
    {
        if (railTop != null) railsList.Add(railTop.transform);
        if (railMid != null) railsList.Add(railMid.transform);
        if (railBottom != null) railsList.Add(railBottom.transform);
    }

    public void CalculateBackgroundSpeed()
    {
        if (isCharging) backgroundSpeed = shootingPhaseSpeed;


        else backgroundSpeed = energyBarLvl / 60f;
    }

    private void UpgradeScore()
    {
        scoreText.text = score.ToString();
    }

    private void UpdateDistace()
    {
        float _panelDist = 148f;

        float _distance = (_panelDist * distance) / 100f;
        

        distanceText.text = _distance.ToString("F2");
    }

}
