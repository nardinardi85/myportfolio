﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSecondaryMovement : MonoBehaviour
{
    public Transform topTarget;
    public Transform bottomTarget;
    public float smoothTime = 0.3f;
    private float yVelocity = 0.0f;

    private bool isGoingUp = true;

    private CatMovement cm;

    private void Start()
    {
        cm = GetComponentInParent<CatMovement>();
    }

    void FixedUpdate()
    {
        if(!cm.isMoving)
        {
            if (isGoingUp)
            {
                float newPosition = Mathf.SmoothDamp(transform.position.y, topTarget.position.y, ref yVelocity, smoothTime);
                transform.position = new Vector3(transform.position.x, newPosition, transform.position.z);

                if (Mathf.Abs(topTarget.position.y - transform.position.y) < 0.1f)
                {
                    isGoingUp = false;
                }
            }
            else
            {
                float newPosition = Mathf.SmoothDamp(transform.position.y, bottomTarget.position.y, ref yVelocity, smoothTime);
                transform.position = new Vector3(transform.position.x, newPosition, transform.position.z);

                if (Mathf.Abs(bottomTarget.position.y - transform.position.y) < 0.1f)
                {
                    isGoingUp = true;
                }
            }
        }
    }
}
