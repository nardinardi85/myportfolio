﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject PausePanel;
    public GameObject button;

    //private ScenesManager sm;

    private void Start()
    {
        //PausePanel.SetActive(true);
        //sm = FindObjectOfType<ScenesManager>().GetComponent<ScenesManager>();
        //PausePanel.SetActive(false);
    }


    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void BackToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void PausePanelActivation(bool active)
    {
        if (!PausePanel.activeSelf)
        {
            //Debug.Log("AproPannelloPausa");
            Time.timeScale = 0;
            PausePanel.SetActive(true);
            button.SetActive(false);
        }
        else
        {
            //Debug.Log("ChiudoPannelloPausa");
            Time.timeScale = 1;
            PausePanel.SetActive(false);
            button.SetActive(true);
        }
    }
}
