﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class HealthController : MonoBehaviour
//{
//    public int maxHearts;
//    public int currentHearts;
//    public Image[] hearts;
//    GameManager gameManager;
//    float filling;

//    public float fillingLerpValue = 0.2f;

//    private void Start()
//    {
//        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
//    }

//    private void Update()
//    {
//        filling -= fillingLerpValue * Time.fixedUnscaledDeltaTime;

//        if (currentHearts > maxHearts)
//        {
//            currentHearts = maxHearts;
//        }

//        for (int i = 0; i < hearts.Length; i++)
//        {
//            if (i < maxHearts)
//            {
//                hearts[i].fillAmount = Mathf.Clamp01(filling);
//            }
//            else
//            {
//                hearts[i].fillAmount = Mathf.Clamp01(-filling);
//            }
//        }

//    }
//}
