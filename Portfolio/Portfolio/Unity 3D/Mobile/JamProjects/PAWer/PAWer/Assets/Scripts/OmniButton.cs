﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OmniButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public float timer;
    public bool isPressed = false;

    public GameObject bulletPhaseOne;
    public GameObject bulletPhaseTwo;
    public GameObject bulletPhaseTree;
    public GameObject bulletPhaseFour;

    private GameManager gm;
    private ButtonBehaviour bb;
    private CatMovement cm;
    private float countdown;
    private bool isHolding = false;
    private Transform bulletStartPos;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        bb = transform.GetComponent<ButtonBehaviour>();
        cm = FindObjectOfType<CatMovement>().GetComponent<CatMovement>();
        bulletStartPos = cm.transform.GetChild(0).GetChild(0).GetComponent<Transform>();
        countdown = timer;
    }


    private void FixedUpdate()
    {
        if(isPressed)
        {
            countdown -= Time.deltaTime;
        }
        else if (isPressed && !Input.GetMouseButton(0))
        {
            isPressed = false;
            countdown = timer;
        }

        if(countdown < 0)
        {
            isHolding = true;
        }

        if(isHolding && gm.isCharging)
        {
            bb.OnButtonHold();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(gm.isCharging)
        {
            bb.OnButtonHold();

            if (gm.energyBarLvl > 750)
            {
                GameObject bullet = Instantiate(bulletPhaseFour, bulletStartPos.position, Quaternion.identity);
                gm.energyBarLvl = 0;
                isPressed = false;
                isHolding = false;
                countdown = timer;
                return;
            }
            else if (gm.energyBarLvl > 500)
            {
                GameObject bullet = Instantiate(bulletPhaseTree, bulletStartPos.position, Quaternion.identity);
                gm.energyBarLvl = 0;
                isPressed = false;
                isHolding = false;
                countdown = timer;
                return;
            }
            else if (gm.energyBarLvl > 250)
            {
                GameObject bullet = Instantiate(bulletPhaseTwo, bulletStartPos.position, Quaternion.identity);
                gm.energyBarLvl = 0;
                isPressed = false;
                isHolding = false;
                countdown = timer;
                return;
            }
            else if (gm.energyBarLvl >= 0)
            {
                GameObject bullet = Instantiate(bulletPhaseOne, bulletStartPos.position, Quaternion.identity);
                gm.energyBarLvl = 0;
                isPressed = false;
                isHolding = false;
                countdown = timer;
                return;
            }
        }

        if(!isHolding)
        {
            bb.OnButtonPressed();
        }
        else
        {
            bb.OnButtonHold();
        }

        isPressed = false;
        isHolding = false;
        countdown = timer;
    }
}
