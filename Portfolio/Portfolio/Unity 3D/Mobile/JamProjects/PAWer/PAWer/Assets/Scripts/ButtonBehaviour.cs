﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehaviour : MonoBehaviour
{
    public GameObject tutorialButt;

    private GameManager gm;

    private bool isCharging;
    private bool started;

    private float energyBarLvl;
    private float clickValue;
    private float holdValue;
    

    void Start()
    {
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        GetValues();
        started = gm.started;
    }


    public void OnButtonPressed()
    {
        if(!started)
        {
            energyBarLvl += clickValue;
            energyBarLvl = Mathf.Clamp(energyBarLvl, 0.0f, 1000.0f);
            gm.energyBarLvl = energyBarLvl;
            gm.started = started = true;
            tutorialButt.SetActive(false);
        }
        else
        {
            GetValues();

            energyBarLvl += clickValue;
            energyBarLvl = Mathf.Clamp(energyBarLvl, 0.0f, 1000.0f);
            gm.energyBarLvl = energyBarLvl;
        }
    }


    public void OnButtonHold()
    {
        if (!started)
        {
            energyBarLvl = Mathf.Clamp(energyBarLvl, 0.0f, 1000.0f);
            gm.energyBarLvl = energyBarLvl;
            gm.started = started = true;
            tutorialButt.SetActive(false);
        }
        else
        {
            GetValues();

            energyBarLvl += holdValue;
            energyBarLvl = Mathf.Clamp(energyBarLvl, 0.0f, 1000.0f);
            gm.energyBarLvl = energyBarLvl;
        }
    }
    
    public void GetValues()
    {
        isCharging = gm.isCharging;
        energyBarLvl = gm.energyBarLvl;
        clickValue = gm.clickValue;
        holdValue = gm.holdvalue;
    }
}
