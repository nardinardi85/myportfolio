﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    public int score;

    public float speed;
    public float speedMod = 200f;

    private Rigidbody2D rb;

    private GameManager gm;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        rb = GetComponent<Rigidbody2D>();

        float _speed = (gm.energyBarLvl / speedMod);

        speed += _speed;


        rb.velocity = transform.right * -speed;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<CatBehaviour>() != null)
        {
            gm.score += score;
            Destroy(gameObject);
        }

       
    }
}
