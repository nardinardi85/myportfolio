﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatMovement : MonoBehaviour
{
    public bool detectSwipeOnlyAfterRelease = false;
    public float swipeThreshold = 10f;
    [HideInInspector]
    public bool isMoving = false;

    [Space]
    public float smoothTime = 0.3f;

    public Position pos;
    public enum Position
    {
        Top,
        Mid,
        Bottom
    }

    private GameManager gm;
    private List<Transform> railList = new List<Transform>();
    private Vector2 fingerDown;
    private Vector2 fingerUp;

    private float yVelocity = 0.0f;

    private bool swipeUp = false;
    private bool swipeDown = false;

    void Start()
    {
        pos = Position.Mid;
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();

        foreach (var t in gm.railsList)
        {
            railList.Add(t);
        }
    }
    
    void Update()
    {
        if (gm.started)
        {
            foreach (Touch touch in Input.touches)
            {
                if(touch.position.x < 1200 && touch.position.y < 1200)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        fingerUp = touch.position;
                        fingerDown = touch.position;
                    }

                    //Detects swipe after finger is released
                    if (touch.phase == TouchPhase.Ended)
                    {
                        fingerDown = touch.position;
                        checkSwipe();
                    }
                }                
            }
        }
    }

    private void FixedUpdate()
    {
        if (swipeUp) OnSwipeUp();
        else if (swipeDown) OnSwipeDown();
    }

    void checkSwipe()
    {
        //Check if Vertical swipe
        if (verticalMove() > swipeThreshold)
        {
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                swipeUp = true;
                swipeDown = false;
                isMoving = true;
            }
            else if (fingerDown.y - fingerUp.y < 0)//Down swipe
            {
                swipeDown = true;
                swipeUp = false;
                isMoving = true;
            }
            fingerUp = fingerDown;
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }
    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }


    void OnSwipeUp()
    {
        Debug.Log("Swipe UP");

        if(pos == Position.Top)
        {
            isMoving = false;
            return;
        }
        else if (pos == Position.Mid)
        {
            MoveTheCat(railList[0], Position.Top, true);
        }
        else if (pos == Position.Bottom)
        {
            MoveTheCat(railList[1], Position.Mid, true);
        }
    }

    void OnSwipeDown()
    {
        Debug.Log("Swipe Down");
        if (pos == Position.Top)
        {
            MoveTheCat(railList[1], Position.Mid, false);
        }
        else if (pos == Position.Mid)
        {
            MoveTheCat(railList[2], Position.Bottom, false);
        }
        else if (pos == Position.Bottom)
        {
            isMoving = false;
            return;
        }
    }

    
    private void MoveTheCat(Transform target, Position p, bool isUp)
    {
        float newPosition = Mathf.SmoothDamp(transform.position.y, target.position.y, ref yVelocity, smoothTime);
        transform.position = new Vector3(transform.position.x, newPosition, transform.position.z);

        if (Mathf.Abs(target.position.y - transform.position.y) < 0.1f)
        {
            pos = p;

            if (isUp) swipeUp = false;
            if (!isUp) swipeDown = false;
            isMoving = false;
        }
    }
}
