﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //[SerializeField] int numTracks;


    public static AudioManager instance = null;
    public List<AudioClip> trackList = new List<AudioClip>();
    public List<string> trackName = new List<string>();

    public AudioSource audioSourceSingle;
    public AudioSource audioSourceLoop;

    public float loopVolume;
    public float sfxVolume;


    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        loopVolume = audioSourceLoop.volume;
        sfxVolume = audioSourceSingle.volume;


        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void PlaySingleSound(string track)
    {
        for (int i = 0; i < trackList.Count; i++)
        {
            if (audioSourceSingle != null)
            {
                if (track == trackName[i])
                {
                    Debug.Log("Sto suonando " + (trackName[i]));
                    audioSourceSingle.enabled = true;
                    instance.audioSourceSingle.PlayOneShot(trackList[i], sfxVolume);
                }
            }
        }
    }

    public void PlayLoopSound(string track)
    {
        for (int i = 0; i < trackList.Count; i++)
        {
            instance.audioSourceLoop.loop = true;
            if (track == trackName[i])
            {
                Debug.Log("Sto suonando " + (trackName[i]));
                instance.audioSourceLoop.enabled = true;
                instance.audioSourceLoop.clip = trackList[i];
                instance.audioSourceLoop.Play();
            }
        }
    }

    public void StopSound()
    {
        audioSourceLoop.Stop();
        audioSourceSingle.Stop();
    }
}
