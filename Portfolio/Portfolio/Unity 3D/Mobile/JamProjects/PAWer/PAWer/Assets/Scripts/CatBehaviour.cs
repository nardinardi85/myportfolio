﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatBehaviour : MonoBehaviour
{
    public int HP;


    GameManager gameManager;
    float filling;

    float fillingLerpValue;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<EnemiesBehaviour>() != null)
        {
            TakeDmg(collision.GetComponent<EnemiesBehaviour>().damage);

            Destroy(collision.gameObject);
            
        }
        if(collision.GetComponent<BulletBehaviour>()!=null)
        {
            TakeDmg(collision.GetComponent<BulletBehaviour>().damage);
            collision.GetComponent<BulletBehaviour>().HitEffect();
            Destroy(collision.gameObject);
        }
    }
    public void TakeDmg(int dmg)
    {
        if(gameManager.isCharging)
        {
            gameManager.currentHearts -= dmg;
            gameManager.UpdateHearts();
        }
        else
        {
            gameManager.energyBarLvl -= dmg;
        }
    }
}
