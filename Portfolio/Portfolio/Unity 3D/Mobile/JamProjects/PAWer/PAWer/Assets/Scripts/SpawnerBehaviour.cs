﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBehaviour : MonoBehaviour
{
    public float minTimer;
    public float maxTimer;
    
    private float timer;

    public GameObject[] asteroidPrefabs;
    public GameObject[] enemiesPrefabs;

    public enum ObstaclesSpawnChance
    {
        Asteroid,
        Comet,
        Satellite    
    }

    public enum EnemiesSpawnChance
    {
        Dog,
        Mouse,
        Sheep
    }
    
    private GameManager gm;

    private void Start()
    {
        timer = Random.Range(minTimer, maxTimer);
        gm = FindObjectOfType<GameManager>().GetComponent<GameManager>();
    }

    private void Update()
    {
        if(gm.started)
        {
            timer -= Time.deltaTime;

            if (!gm.isCharging)
            {
                if (timer < 0)
                {
                    Instantiate(asteroidPrefabs[Random.Range(0, asteroidPrefabs.Length)], transform.position, Quaternion.identity, transform);
                    timer = Random.Range(minTimer, maxTimer) + Random.Range(minTimer, maxTimer)/2;
                }
            }
            else
            {
                if (timer < 0)
                {
                    Instantiate(enemiesPrefabs[Random.Range(0, enemiesPrefabs.Length)], transform.position, Quaternion.identity, transform);
                    timer = Random.Range(minTimer, maxTimer) + Random.Range(minTimer, maxTimer)/2;
                }
            }
        }
    }
}
