﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public float speed = 20f;
    public int damage;
    private Rigidbody2D rb;
    public bool isEnemyBullet;
    public float moveLocktimer;

    public bool isCat;
    public bool isSheep;




    void Start()
    {
       
        if (!isEnemyBullet)
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = transform.right * speed;
            Destroy(gameObject, 2f);
        }
        else
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = transform.right * -speed;
            Destroy(gameObject, 2f);
        }
    }

    public void HitEffect()
    {
        if(isSheep)
        {
            moveLocktimer -= Time.deltaTime;
            if(moveLocktimer > 0)
            {
                FindObjectOfType<CatMovement>().GetComponent<CatMovement>().isMoving = false;
            }
            else
            {
                FindObjectOfType<CatMovement>().GetComponent<CatMovement>().isMoving = true;
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCat)
        {
            if (collision.GetComponent<EnemiesBehaviour>() != null)
            {
                EnemiesBehaviour enemy = collision.GetComponent<EnemiesBehaviour>();

                enemy.hp -= damage;
            }
        }
    }
}
