﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    [SerializeField]
    private GameObject PausePanel;

    [SerializeField]
    private TimeController TC;

    public bool canPause = true;

    private void LateUpdate()
    {
        if (!canPause)
        {
            canPause = true;
            return;
        }

        if (Input.GetButtonDown("Pause") && Time.timeScale != 0)
        {
            canPause = false;

            //Debug.Log("Pausing");
            if (PausePanel != null)
            {
                PausePanelActivation(!PausePanel.activeSelf);
            }
            
        }

        //else if (Input.GetButtonDown("Pause") && Time.timeScale <= 0)
        //{
        //    if (PausePanel != null)
        //        PausePanelActivation(!PausePanel.activeSelf);
        //}
    }

    public void PausePanelActivation(bool active)
    {        
        if(active)
        {
            TC.Chronos(0);
            PausePanel.SetActive(true);
            Button[] PausePanelButtons = PausePanel.GetComponentsInChildren<Button>();
            PausePanelButtons[0].Select();
        }
        else
        {
            //TC.Chronos(1);
            PausePanel.SetActive(false);
        }
    }
}
