﻿using UnityEngine;

[RequireComponent(typeof(Buttons))]
public class SFXSlider : MonoBehaviour
{
    private Buttons buttons;

    private void Start()
    {
        buttons = GetComponent<Buttons>();
        buttons.OnChoiceUpdate += UpdateSFXVolume;

        buttons.selectedIndex = PlayerPrefs.GetInt("sfx_volume", 10);
        buttons.UpdateChoice();
    }

    private void UpdateSFXVolume()
    {
        PlayerPrefs.SetInt("sfx_volume", buttons.selectedIndex);

        SetVolume();
        PlayerPrefs.Save();
    }

    public void SetVolume()
    {
        AudioManager.instance.SFXBus.setVolume(PlayerPrefs.GetInt("sfx_volume", 10));
    }
}
