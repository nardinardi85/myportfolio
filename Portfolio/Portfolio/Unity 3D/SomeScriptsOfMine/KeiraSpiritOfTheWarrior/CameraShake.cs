﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CameraShake : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Shake(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }
}
