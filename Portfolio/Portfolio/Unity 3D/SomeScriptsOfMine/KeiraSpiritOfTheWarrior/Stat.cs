﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public interface ISubStatEditable
{
    float EditableValue { get; set; }
}

public class SubStat
{
    private float initValue;

    public bool isFloat = false;
    public string statName;
    public float value;
    public float multiplier;
    public ISubStatEditable editable;
    public List<Stat> linkedStats;

    public event Action<bool> OnValueChange = delegate { };

    public SubStat(List<Stat> linkedStats, string statName, float value, float multiplier, ISubStatEditable editable, bool isFloat = false)
    {
        this.linkedStats = linkedStats;
        this.isFloat = isFloat;
        this.statName = statName;
        this.value = value;
        this.multiplier = multiplier;
        this.editable = editable;

        foreach (var stat in this.linkedStats)
        {
            stat.OnStatChange += Calculate;
        }
        initValue = this.value;
    }

    public void Calculate()
    {
        Clear();

        //Debug.Log("[" + statName + "] Linked stats count " + linkedStats.Count);

        foreach (var stat in linkedStats)
        {
            for (int i = 0; i < stat.StatValue; i++)
            {
                value = value * multiplier;
            }
        }

       // Debug.Log("Calculating... value is " + value);

        OnValueChange(value > editable.EditableValue);
    }
    
    public float PreviewNextValue()
    {
        return value * multiplier;
    }

    public void Apply()
    {
        //initValue = value;
        editable.EditableValue = value;
    }

    public void Clear()
    {
        value = initValue;
        OnValueChange(value > editable.EditableValue);
    }
}

public class Stat : Buttons
{
    #region Fields

    [SerializeField]
    private List<RectTransform> statPointUI;

    [SerializeField]
    Sprite PurchasedSprite;

    [SerializeField]
    Sprite PrePurchaseSprite;

    [SerializeField]
    Sprite DefaultSprite;

    [SerializeField]
    private SubStatUI[] subStatsUI;

    public event Action OnStatChange = delegate { };

    private int prePurchasedStatValue;
    private StatsManager statsManager;

    #endregion

    #region Properties

    public bool IsDirty { get; set; }

    //public StatType StatType => statType;
    //public float StatMultiplier => statMultiplier;
    public int StatValue { get; private set; }

    //public static event Action<StatType, float, bool> OnStatValueChange = delegate

    #endregion

    #region Methods

    public void Init(int initStatValue, StatsManager statsManager)
    {
        this.statsManager = statsManager;
        StatValue = initStatValue;
        OnStatChange();

        prePurchasedStatValue = StatValue;

        RefreshUI();
    }

    public void IncreaseStatValue(int amount = 1)
    {
        StatValue += amount;

        if (StatValue > statPointUI.Count)
        {
            StatValue = statPointUI.Count;
        }
        else
        {
            OnStatChange();
        }

        IsDirty = StatValue != prePurchasedStatValue;

        RefreshUI();
    }

    public void DecreaseStatValue(int amount = 1)
    {
        StatValue -= amount;

        if (StatValue < prePurchasedStatValue)
        {
            StatValue = prePurchasedStatValue;
        }
        else
        {
            OnStatChange();
        }

        IsDirty = StatValue != prePurchasedStatValue;

        RefreshUI();

    }

    public void RefreshUI()
    {
        for (int i = 0; i < statPointUI.Count; i++)
        {
            statPointUI[i].localScale = Vector3.one;
            statPointUI[i].GetComponent<Image>().sprite = DefaultSprite;
        }

        foreach (var item in statPointUI)
        {
            item.GetComponent<Image>().color = Color.white;
        }

        for (int i = 0; i < StatValue; i++)
        {
            statPointUI[i].localScale = new Vector3(resizeValue, resizeValue, resizeValue);

            
            statPointUI[i].GetComponent<Image>().sprite = i > prePurchasedStatValue - 1 ? PrePurchaseSprite : PurchasedSprite;
        }

        for (int i = prePurchasedStatValue; i < StatValue; i++)
        {
            statPointUI[i].GetComponent<Image>().color = statsManager.AfterSaleExp >= 0 ? Color.white : new Color(1, 0.0990566f, 0.3357096f, 1);
        }
    }

    public void Confirm()
    {
        IsDirty = false;
        prePurchasedStatValue = StatValue;

        RefreshUI();
    }

    public void Clear()
    {
        IsDirty = false;
        StatValue = prePurchasedStatValue;

        RefreshUI();
    }

    #endregion

}


