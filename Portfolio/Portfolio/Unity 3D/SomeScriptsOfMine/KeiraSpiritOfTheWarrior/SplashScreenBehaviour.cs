﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject MainMenuPanel;


    private void Update()
    {
        if(Input.GetButton("Submit"))
        {
            AudioManager.instance.UISelectionButtonSound();
            MainMenuPanelActivation();
        }
    }
    private void MainMenuPanelActivation()
    {
        MainMenuPanel.SetActive(true);
        gameObject.SetActive(false);
    }
}
