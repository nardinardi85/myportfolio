﻿using TMPro;
using UnityEngine;

public class StatsManagerUI : MonoBehaviour
{
    public TMP_Text currentExp;
    public TMP_Text requiredExp;
    public TMP_Text afterSaleExp;
    public TMP_Text level;

    public StatsManager statsManager;

    private void Awake()
    {
        statsManager.OnLevelOrExpChange += UpdateUI;
    }

    public void UpdateUI()
    {
        currentExp.text = statsManager.CurrentExp.ToString();
        requiredExp.text = statsManager.RequiredExp.ToString();
        afterSaleExp.text = statsManager.AfterSaleExp.ToString();
        level.text = statsManager.CurrentLevel.ToString();
    }

}
