﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class B_ButtonBehaviour : MonoBehaviour
{
    GameObject child;
    Sprite myChildSprite;
    [SerializeField]
    Sprite EscSprite;
    [SerializeField]
    Sprite BSprite;

    bool isUsingKyeboard = false;

    private void Start()
    {
        child = gameObject.transform.GetChild(0).gameObject;
        myChildSprite = transform.GetChild(0).gameObject.GetComponent<Image>().sprite;
    }

    private void Update()
    {
        myChildSprite = EscSprite;

        if (Input.anyKey)
        {
            //Debug.Log("Pigio Anykey");
            isUsingKyeboard = true;
            myChildSprite = EscSprite;
            SpriteChange(child);
        }
    }

    public void SpriteChange(GameObject spriteToChange)
    {
        if (isUsingKyeboard)
            spriteToChange.GetComponent<Image>().sprite = EscSprite;
        else
            spriteToChange.GetComponent<Image>().sprite = BSprite;
    }

    public void LateUpdate()
    {
        if (Input.GetButton("MenuNavHorJoystick") || Input.GetButton("MenuNavVertJoystick") || Input.GetButton("JoystickAnyButton")  || (Input.GetAxis("MenuNavHorJoystick") >= 0.1f || Input.GetAxis("MenuNavHorJoystick") <= -0.1f) || (Input.GetAxis("MenuNavVertJoystick") >= 0.1f || Input.GetAxis("MenuNavVertJoystick") <= -0.1f) || (Input.GetAxis("JoystickAnyButton") >= 0.1f || Input.GetAxis("JoystickAnyButton") <= -0.1f))
        {
            //Debug.Log("Pigio Joystick");
            isUsingKyeboard = false;
            myChildSprite = BSprite;
            SpriteChange(child);
        }
    }
}


//if(EventSystem.current.currentSelectedGameObject.GetComponent<Buttons>().isLocked)
//{
//    GetComponentInChildren<Image>().enabled = false;
//    GetComponentInChildren<Text>().enabled = false;
//}
//else
//{
//    GetComponentInChildren<Image>().enabled = true;
//    GetComponentInChildren<Text>().enabled = true;
//}