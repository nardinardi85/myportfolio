﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class PanelClosureBehaviour : MonoBehaviour
{
    public TimeController TC;
    public GameObject PanelToDeactivate;
    public Button[] Buttons;
    public int timeScale;

    private void Update()
    {   
        if(PanelToDeactivate.activeSelf && Input.GetButtonDown("Back"))
        {
            PanelClosure();
        }
    }

    public void PanelClosure()
    {
        foreach (Button b in Buttons)
        {
            if (EventSystem.current.currentSelectedGameObject == b.gameObject)
            {
                PanelToDeactivate.SetActive(false);
                TC.Chronos(timeScale);
            }
        }
    }
}
