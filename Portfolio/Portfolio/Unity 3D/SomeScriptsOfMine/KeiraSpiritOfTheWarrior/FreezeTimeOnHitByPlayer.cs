﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(HealthController))]
public class FreezeTimeOnHitByPlayer : MonoBehaviour
{
    [SerializeField]
    private float freezeIntensity = 0.2f;

    [SerializeField]
    private float freezeTimeCooldown = 1f;

    private float lastTimeFreeze;

    private HealthController hc;

    private void Awake()
    {
        hc = GetComponent<HealthController>();

        hc.OnTakeHit += FreezeTime;
    }

    private void FreezeTime(GameObject hitter)
    {
        if (!CanFreeze() && hitter.GetComponentInParent<PlayerInput>() == null)
            return;

        lastTimeFreeze = Time.time;

        StopAllCoroutines();

        StartCoroutine(CoFreezeTime());
    }

    private bool CanFreeze()
    {
        return Time.time > lastTimeFreeze + freezeTimeCooldown;
    }

    private void OnDestroy()
    {
        if(GetComponent<SlowTimeOnDeath>() == null)
            Time.timeScale = 1;

        StopAllCoroutines();
    }

    private IEnumerator CoFreezeTime()
    {
        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(freezeIntensity);

        Time.timeScale = 1;
    }
}

