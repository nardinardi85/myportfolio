﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Talent : Buttons
{
    public IgnoreNullClickRaycast IgnoreNavigationInput;
    public Sprite PurchasedSprite;
    public Sprite PrePurchaseSprite;
    public Sprite DefaultSprite;
    public bool isPrePurchased;
    public bool isPurchased = false;
    [SerializeField]
    private bool isVitalityTalent;
    [SerializeField]
    private bool isEnduranceTalent;
    [SerializeField]
    private bool isStrengthTalent;
    [SerializeField]
    private bool isDefenseTalent;

    public ExpController EC;

    public Action OnTalentSelected = delegate { };

    private new void Awake()
    {
        base.Awake();        
    }

    /*
    private new void Update()
    {
        base.Update();

        if (isSelected)
        {
            button.Select();
        }

        timer -= Time.unscaledDeltaTime;

        //if(selectedIndex >= VT.PurchaseableVitality.Count - 1)
        //{
        //    selectedIndex = VT.PurchaseableVitality.Count - 1;
        //}
        //if (selectedIndex <= 0)
        //{
        //    selectedIndex = 0;
        //}
        if (isVitalityTalent)
        {
            if (Input.GetAxisRaw("MenuNavHor") > 0 && timer <= 0)
            {
                for (int t = 0; t > VT.Talents.Count; t++)
                {
                    if (EC.Exp >= EC.XpNeeded(EC.CurrentLevel))
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        VT.Talents[t + 1].isSelected = true;
                        VT.Talents[t + 1].button.Select();
                        isPrePurchased = true;
                        VT.Talents[t].GetComponent<Image>().sprite = PrePurchaseSprite;
                        EC.CurrentLevel++;
                        OnTalentSelected();
                        Debug.Log("Mi sono mosso");
                        t++;
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        IgnoreNavigationInput.GetLastGameObjectSelected();
                        //EC.CurrentLevel--;
                        Debug.Log("NON mi sono mosso");
                    }

                }
                timer = originalTimer;
            }
            else if (Input.GetAxisRaw("MenuNavHor") < 0 && timer <= 0 && VT.Talents.IndexOf(this) >= 0)
            {
                for (int t = 0; t > VT.Talents.Count; t++)
                {
                    isPrePurchased = false;
                    EC.CurrentLevel--;
                    VT.Talents[t].GetComponent<Image>().sprite = DefaultSprite;
                    timer = originalTimer;
                    //OnTalentSelected();
                    t++;
                }
            }

        }
        /*else if((Input.GetAxisRaw("MenuNavHor") < 0 && VT.Talents.IndexOf(this) < 0) ||(Input.GetAxisRaw("MenuNavHor") > 0 && VT.Talents.IndexOf(this) > VT.Talents.Count))
        {
            EventSystem.current.SetSelectedGameObject(null);
            IgnoreNavigationInput.GetLastGameObjectSelected();
        }*/

    
}


