﻿using UnityEngine;
using TMPro;

public class SubStatUI : MonoBehaviour
{
    [SerializeField]
    private TMP_Text statNameText;

    [SerializeField]
    private TMP_Text currentValueText;

    [SerializeField]
    private TMP_Text nextValueText;

    private SubStat target;

    public void Init(SubStat target)
    {
        this.target = target;
        target.OnValueChange += UpdateUI;
    }

    public void UpdateUI(bool edited)
    {
        if (target.isFloat)
        {
            currentValueText.text = (target.value).ToString("0.00");
            nextValueText.text = (target.PreviewNextValue()).ToString("0.00");
        }
        else
        {
            currentValueText.text = ((int)target.value).ToString();
            nextValueText.text = ((int)target.PreviewNextValue()).ToString();
            //nextValueText.color = edited ? Color.green : Color.white;
        }
        statNameText.text = target.statName;
    }

}


