﻿using UnityEngine;

[RequireComponent(typeof(KeresMovement))]
public class KeresFeedbacks : MonoBehaviour
{
    public Material HeadMaterialDefault;
    public Material HeadMaterialPOI;

    public Material TrailMaterialDefault;
    public Material TrailMaterialPOI;

    public GameObject FlameParticle;
    public GameObject[] HeadParts;
    public GameObject[] TrailParts;
    
    private KeresMovement keresMovement;

    private void Awake()
    {
        keresMovement = GetComponent<KeresMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Mi è sembrato di vedere un POI \n Poi ci penserò");
        //foreach (GameObject headPart in HeadParts)
        //{
        //    FlameParticle.GetComponent<ParticleSystemRenderer>().material = HeadMaterialPOI;
        //    //headPart.GetComponent<MeshRenderer>().material = HeadMaterialPOI;
        //}
        //foreach (GameObject trailPart in TrailParts)
        //{
        //    trailPart.GetComponent<ParticleSystemRenderer>().trailMaterial = TrailMaterialPOI;
        //    trailPart.GetComponent<ParticleSystemRenderer>().material = TrailMaterialPOI;
        //}

        keresMovement.SwitchTarget(other.GetComponent<Rigidbody>(), false);
    }

    private void OnTriggerExit(Collider other)
    {
        //foreach (GameObject headPart in HeadParts)
        //{
        //    FlameParticle.GetComponent<ParticleSystemRenderer>().material = HeadMaterialDefault;
        //    //headPart.GetComponent<MeshRenderer>().material = HeadMaterialDefault;
        //}
        //foreach (GameObject trailPart in TrailParts)
        //{
        //    trailPart.GetComponent<ParticleSystemRenderer>().trailMaterial = TrailMaterialDefault;
        //    trailPart.GetComponent<ParticleSystemRenderer>().material = TrailMaterialDefault;
        //}

    }
}
