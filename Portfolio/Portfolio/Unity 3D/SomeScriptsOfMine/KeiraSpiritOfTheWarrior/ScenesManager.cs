﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Cinemachine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.EventSystems;

public class ScenesManager : MonoBehaviour
{
    public static ScenesManager instance;

    private float loadingScreenMinDuration = 2;

    public GameObject Player;
    public GameObject GameHUDPanel;
    public GameObject DarkBackground;
    public GameObject KeresLoadingImage;
    public GameObject LoadingScreenPanel;
    [HideInInspector]
    public Image loadingScreenImage;
    [HideInInspector]
    public Camera mainCamera;

    private bool changingScene;

    public static ScenesManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {       
        DontDestroyOnLoad(gameObject);
        mainCamera = Camera.main;

        if(LoadingScreenPanel.transform.childCount > 0)
            loadingScreenImage = LoadingScreenPanel.transform.GetChild(0).GetComponent<Image>();
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
    }

    public void SceneChange(int index)
    {
        if(EventSystem.current != null)
            EventSystem.current.enabled = false;

        StartCoroutine(CoLoadSceneAsyncAfterSeconds(0, loadingScreenMinDuration, index));
    }

    public void SceneChangeAfterFadeIn(int index)
    {
        if (EventSystem.current != null)
            EventSystem.current.enabled = false;

        FindObjectOfType<AutomaticFadeInOut>().FadeIn(() => SceneChange(index), 0);
    }

    public void SceneChangeAfterFadeIn(int index, float delay)
    {
        if (EventSystem.current != null)
            EventSystem.current.enabled = false;

        FindObjectOfType<AutomaticFadeInOut>().FadeIn( () => SceneChange(index), delay );
    }

    public void SceneChange(int index, float delay)
    {
        StartCoroutine(CoLoadSceneAsyncAfterSeconds(delay, loadingScreenMinDuration, index));
    }

    private IEnumerator CoLoadSceneAsyncAfterSeconds(float delay, float time, int index)
    {        
        yield return new WaitForSecondsRealtime(delay);
        mainCamera.GetComponent<PostProcessVolume>().profile.GetSetting<ColorGrading>().saturation.value = 0;

        LoadingScreenPanel.SetActive(true);
        if(KeresLoadingImage != null)
            KeresLoadingImage.SetActive(true);
        if (GameHUDPanel != null)
            GameHUDPanel.SetActive(false);

        if(Player != null)
            Player.GetComponent<HealthController>().Heal(Player.GetComponent<HealthController>().MaxHp);

        yield return new WaitForSecondsRealtime(0.2f);
        
        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(time);

        var asyncOperation = SceneManager.LoadSceneAsync(index);
        asyncOperation.completed += (x) => Time.timeScale = 1;

        if (SceneManager.GetActiveScene().buildIndex != 0 && asyncOperation.isDone)
        {
            if(KeresLoadingImage != null)
                KeresLoadingImage.SetActive(false);
            if (GameHUDPanel != null)
                GameHUDPanel.SetActive(true);
            LoadingScreenPanel.SetActive(false);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private IEnumerator CoTeleport(float delay)
    {
        DarkBackground.SetActive(false);

        if (GameHUDPanel != null)
            GameHUDPanel.SetActive(false);

        if(KeresLoadingImage != null)
            KeresLoadingImage.SetActive(true);
        LoadingScreenPanel.SetActive(true);
        Debug.Log(LoadingScreenPanel.activeSelf);
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(delay);
        var asyncOperation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        asyncOperation.completed += (x) => Time.timeScale = 1;

        yield return new WaitForEndOfFrame();

        if(asyncOperation.isDone)
        {
            if(KeresLoadingImage != null)
                KeresLoadingImage.SetActive(false);
            if (GameHUDPanel != null)
                GameHUDPanel.SetActive(true);
            LoadingScreenPanel.SetActive(false);
        }

        yield return new WaitForSecondsRealtime(delay);

        if(KeresLoadingImage != null)
            KeresLoadingImage.SetActive(false);
    }

    public void TeleportLoading(float delay)
    {
        StartCoroutine(CoTeleport(delay));
    }

    public void ResetData()
    {
        SaveManager.data = new SaveManager.Data();
        SaveManager.SaveData();
    }
}