﻿using System.Collections;
using UnityEngine;
using System.Linq;
public class Teleporter : InteractableObject
{
    #region Fields

    [SerializeField]
    private Teleporter destination;

    public TeleportProjectileVFX TeleportProjectileVFXPrefab;
    public float yOffsetParticleSpawn;

    public GameObject TeleporterEdge;

    private bool active = true;
    #endregion

    #region Methods

    protected override void Interact()
    {
        if (!active)
            return;
        
        base.Interact();

        Debug.Log(Interacter.GetComponentInChildren<Animator>().GetBool("HasDashed"));
        if (Interacter.GetComponentInChildren<Animator>().GetBool("HasDashed"))
            return;

        AudioManager.instance.TeleportAudio();
        AudioManager.instance.playAmbientSound2.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        StartCoroutine(CoDisableInteracterForSeconds(1));
        Interacter.transform.position = destination.transform.position - Vector3.up * 1;
        DisableForSeconds(1);
        destination.DisableForSeconds(1);

        TeleportProjectileVFX projectileVFX = Instantiate(TeleportProjectileVFXPrefab, new Vector3 (transform.position.x , transform.position.y , transform.position.z), Quaternion.identity);
        projectileVFX.Init(destination.transform.position, yOffsetParticleSpawn);

    }

    private IEnumerator CoDisableInteracterForSeconds(float seconds)
    {
        Interacter.SetActive(false);
        yield return new WaitForSeconds(seconds);
        Interacter.SetActive(true);
    }

    protected override void TriggerEnter()
    {
        base.TriggerEnter();
        if (destination.Interacter != null)
            destination.TriggerExit();

        AudioManager.instance.TeleportAudioIdle();

        Debug.Log("Audio Start");

        //TeleporterEdge.SetActive(true);
        foreach (var particle in TeleporterEdge.GetComponentsInChildren<ParticleSystem>())
        {
            particle.Play();
        }
        Debug.Log("Start");

    }

    protected override void TriggerExit()
    {
        base.TriggerExit();
        Debug.Log("Audio Stop");
        AudioManager.instance.playAmbientSound2.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        //TeleporterEdge.SetActive(false);
        foreach (var particle in TeleporterEdge.GetComponentsInChildren<ParticleSystem>())
        {
            particle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        }
        Debug.Log("Stop");

    }
    
    public void DisableForSeconds(float seconds)
    {
        //StopAllCoroutines();
        StartCoroutine(CoDisableForSeconds(seconds));
    }

    private IEnumerator CoDisableForSeconds(float seconds)
    {
        active = false;

        yield return new WaitForSeconds(seconds);

        active = true;
    }

    #endregion
}
