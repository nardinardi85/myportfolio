﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.Linq;

[System.Serializable]
public class GameObjectTooltip
{
    public GameObject gameObject;
    public string tooltip;
}

public class TooltipBehaviour : MonoBehaviour
{
    
    [SerializeField]
    TextMeshProUGUI tooltipText;

    [SerializeField]
    private GameObjectTooltip[] gameObjectTooltips;

    private void Awake()
    {
        //Tooltip1 = "Hide HUD during exploration phase";
        //Tooltip2 = "Set game sound effects volume";
        //Tooltip3 = "Set game music volume";
    }

    private void Update()
    {
        GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

        tooltipText.text = gameObjectTooltips.Where(x => x.gameObject == selectedGameObject).FirstOrDefault().tooltip;
    }

    //public void TooltipChange(string str)
    //{
    //    TooltipPanel.SetActive(true);
    //}
}
