﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class Checkpoint : InteractableObject
{
    [SerializeField]
    private GameObject fastTravelPanel;

    [SerializeField]
    private float teleportDelay = 2f;

    //public GameObject CheckpointPanel;

    public CheckpointManagerUI CheckpointManagerUI;

    public CheckpointSO CheckpointSO;

    private Text checkpointName;

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject keres;

    [SerializeField]
    private TMP_Text locationName;

    //private CheckpointManager checkpointManager;

    private Image spriteToChange;

    private Sprite checkpointImage;

    private void Start()
    {
        checkpointImage = CheckpointSO.LocationImage;

        if (fastTravelPanel != null)
        {
            checkpointName = fastTravelPanel.GetComponentInChildren<Text>();
        }
    }

    protected override void TriggerEnter()
    {
        base.TriggerEnter();

        CheckpointManagerUI.checkpointManager.SetLastVisitedCheckpoint(this);

        if (player != null)
            player.GetComponent<HealthController>().Heal(player.GetComponent<HealthController>().MaxHp);

        GetComponent<Animator>().SetBool("Interactible", true);

        if (CheckpointManagerUI.checkpointManager.DiscoveredCheckpoints[this] == false)
        {
            AudioManager.instance.CheckpointAudio();
            StartCoroutine(NewCheckpointPanelSpawn());
        }

        CheckpointManagerUI.checkpointManager.DiscoveredCheckpoints[this] = true;
        SaveManager.SaveData();

        // CheckpointManagerUI.checkpointButton.isSelected = true;
    }

    protected override void TriggerExit()
    {
        base.TriggerExit();
        GetComponent<Animator>().SetBool("Interactible", false);

        if (CheckpointManagerUI.checkpointButton != null)
            CheckpointManagerUI.checkpointButton.OnDeselect(null);

    }

    protected override void Interact()
    {
        base.Interact();

        CheckpointManagerUI.EnableAtCheckpoint(CheckpointSO.Number - 1);
        locationName.text = CheckpointSO.TitleName.ToString();
        Time.timeScale = 0;
    }

    public void Teleport() //Da usare nei bottoni
    {
        //Time.timeScale = 1;
        CheckpointManagerUI.Disable();
        fastTravelPanel.SetActive(false);
        
        Transform destination = gameObject.transform;
        player.transform.position = destination.transform.position + (new Vector3(1f, 0.3f, 0));
        keres.transform.position = destination.transform.position + (new Vector3(1f, 0.3f, 0));

        ScenesManager.instance.loadingScreenImage.sprite = destination.GetComponent<Checkpoint>().CheckpointSO.LocationImage;
        ScenesManager.instance.TeleportLoading(teleportDelay);
        SaveManager.data.lastCheckpointIndex = destination.GetComponent<Checkpoint>().CheckpointSO.Number - 1;
        SaveManager.SaveData();
        //CheckpointPanel.SetActive(false);
        //EventSystem.current.SetSelectedGameObject(null);

    }
    /* public void GoToCheckpoint(int checkpoint)
    {
        if(checkpointManager.GoToCheckpointDictionary [checkpointManager.GoToCheckpointArray [checkpoint - 1]])
        {
            player.transform.position = checkpointManager.GoToCheckpointArray [checkpoint - 1].transform.position;
        }    
    }*/

    public IEnumerator NewCheckpointPanelSpawn()
    {
        CheckpointManagerUI.discoveredCheckpointPanel.SetActive(true);
        CheckpointManagerUI.discoveredCheckpointText.text = CheckpointSO.TitleName;
        yield return null;
    }
}