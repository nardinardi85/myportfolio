﻿using System;
using UnityEngine;

[RequireComponent(typeof(Buttons))]
public class DynamicHUDButton : MonoBehaviour
{
    private Buttons buttons;
    public Action<bool> OnUpdateDynamicHUDButton = delegate { };

    public void Start()
    {
        buttons = GetComponent<Buttons>();
        buttons.OnChoiceUpdate += UpdateDynamicHUDButton;

        buttons.selectedIndex = PlayerPrefs.GetInt("dynamic_hud", 0);
        buttons.UpdateChoice();

    }

    private void UpdateDynamicHUDButton()
    {
        PlayerPrefs.SetInt("dynamic_hud", buttons.selectedIndex);
        PlayerPrefs.Save();

        UpdateDynamic();
    }

    public void UpdateDynamic()
    {
        OnUpdateDynamicHUDButton(PlayerPrefs.GetInt("dynamic_hud", 0) == 0 ? false : true);
    }
}
