﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//KERES IS A FOLLOWING PET

public class KeresMovement : MonoBehaviour
{
    private Rigidbody initTargetRigidbody;
    public Rigidbody targetRigidbody;

    [HideInInspector]
    public Transform targetTransform; //Keira's Transform
    private Transform keresTransform; //Keres' Transform
    private Rigidbody keresBody; //Keres' Rigidbody

    [HideInInspector]
    public Vector3 targetPosition; 
    private float keresAltitude; //Actual keres y value

    [SerializeField]
    private float floatModifier;
    [SerializeField]
    private float floatAmplitudeModifier;
    
    public Transform KeresTail; //Position of the Taail Particle effect

    public float keresAltitudeModifier; //Keres altitude compared to the player
    public float ZOffset = 0.9f; //Offset from the player's shoulder
    public float KeresSpeed; //Keres speed MULTIPLIER;
    public float MaxDistanceToPlayer; //Max distance allowed before sprinting

    private float distanceToPlayer; //Distance between Keres and the Player

    [SerializeField]
    private float keresAcceleration = 3f; //Keres Acceleration Value
    [SerializeField]
    private float rotSpeed = 1f; //Keres Rotation speed MULTIPLIER

    [SerializeField]
    private float maxDistanceFromInitTarget = 5f;

    private Quaternion keresRotation; //Current Keres' rotation

    private bool targetIsInitTarget = true;

    void Start()
    {
        initTargetRigidbody = targetRigidbody;
        transform.position = targetRigidbody.position;
        targetTransform = targetRigidbody.transform;
        keresTransform = this.transform;
        keresBody = GetComponent<Rigidbody>();

        keresRotation = transform.rotation;

    }


    Quaternion targetRotation;

    private void Update()
    {
        if (!targetIsInitTarget)
        {
            if (Vector3.Distance(transform.position, initTargetRigidbody.position) > maxDistanceFromInitTarget)
            {
                SwitchTarget(initTargetRigidbody, true);
            }
        }
    }

    private void FixedUpdate()
    {
        if (targetPosition == null || targetTransform == null)
            return;

        keresAltitude = Mathf.Sin(Time.time * floatModifier)*floatAmplitudeModifier + keresAltitudeModifier;

        if (targetTransform.position.z > gameObject.transform.position.z)
        {
            targetRotation = Quaternion.identity;
        }
        else
        {
            targetRotation = Quaternion.Euler(0, 180, 0);
        }

        keresBody.rotation = Quaternion.Slerp(keresBody.rotation, targetRotation, rotSpeed * Time.fixedDeltaTime);

        targetPosition = new Vector3(targetRigidbody.position.x, targetRigidbody.position.y + keresAltitude, targetRigidbody.position.z - ZOffset);

        if (targetRigidbody.rotation.y != 0)
        {
            targetPosition = new Vector3(targetRigidbody.position.x, targetRigidbody.position.y + keresAltitude, targetRigidbody.position.z + ZOffset);
            keresBody.velocity = keresBody.velocity * 2;
        }


        KeresFollow();
    }

    private Vector3 keresVelocity;

    public void SwitchTarget(Rigidbody newTargetRB, bool isInitTarget)
    {
        if (newTargetRB == null)
            return;

        targetIsInitTarget = isInitTarget;
        targetRigidbody = newTargetRB;
        targetTransform = newTargetRB.transform;
    }

    public void KeresFollow()
    {
        distanceToPlayer = Vector3.Distance(keresTransform.position, targetTransform.position);
        keresBody.position = Vector3.SmoothDamp(keresBody.position, targetPosition, ref keresVelocity, 0.3f);
    }

}
