﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsImageSwap : MonoBehaviour
{
    public Sprite SpriteToSwap;
    public Image DestinationImage;

    Buttons button;

    private void Start()
    {
        button = GetComponent<Buttons>();
    }

    private void Update()
    {
        if(button.isSelected)
        {
            DestinationImage.sprite = SpriteToSwap;
        }
    }
}
