﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class GUIManager : MonoBehaviour
{
    //GUI Elements
    public Image HealthBar;
    public Image StaminaBar;
    //public Image ExpBar;
    public Text HealthTxt;
    public Text StaminaTxt;
    public TMP_Text LevelTxt;
    public TMP_Text ExpTxt;
    // public Text LvlTxt;
    //private Image bButton;

    public Image xButton;
    public Image xImage;

    public Image yButton;
    public Image yImage;

    public Image LSRSImage;

    public Image MaxHpSparkle;
    public Image MaxStaminaSparkle;

    //Controllers
    public HealthController HC;
    public StaminaController SC;
    public RageController RC;
    public ExpController EC;

    private void Awake()
    {
        HC.OnHealthChange += UpdateGUI;
        EC.OnGainExp += UpdateGUI;
        SC.OnStaminaChange += UpdateGUI;
    }

    //Updates all GUI elements
    public void UpdateGUI()
    {
        StaminaBar.fillAmount = (float)SC.stamina / SC.MaxStamina;
       // StaminaTxt.text = ("STAMINA: " + ((int)SC.stamina).ToString() + " / " + SC.MaxStamina.ToString());
        HealthBar.fillAmount = (float)HC.Hp / HC.MaxHp;
       // HealthTxt.text = ("HP: " + HC.Hp.ToString() + " / " + HC.MaxHp.ToString());
        //LevelTxt.text = ("Level: " + EC.CurrentLevel.ToString());
        //ExpBar.fillAmount = (float)EC.Exp / EC.XpCap;
        if (ExpTxt != null)
        {
            ExpTxt.text = (EC.Exp.ToString());
        }
       
        //LvlTxt.text = ("LEVEL: " + EC.CurrentLevel.ToString());
    }

    private void Update()
    {
        if (RC != null)
        {
            if (RC.Rage == RC.MaxRage)
            {
                LSRSImage.enabled = true;
            }
            else
            {
                LSRSImage.enabled = false;
            }
        }
        if (HC != null)
        {
            if (HC.Hp == HC.MaxHp)
            {
                MaxHpSparkle.enabled = true;
            }
            else
            {
                MaxHpSparkle.enabled = false;
            }
        }

        if (SC != null)
        {
            if (SC.stamina == SC.MaxStamina)
            {
                MaxStaminaSparkle.enabled = true;
            }
            else
            {
                MaxStaminaSparkle.enabled = false;
            }
        }

        //if(Input.GetButtonDown("AbilitySwitch"))
        //{
        //    bButton.text = "";
        //    xButton.text = "";
        //    yButton.text = "";
        //    if (UnlockedWallbrakerAbility)
        //        xButton.text = "WallBreaker";
        //    if (UnlockedLightAbility)
        //        bButton.text = "Light";
        //    if (UnlockedShockWaveability)
        //        yButton.text = "Shockwave";
        //}
        //if(Input.GetButtonUp("AbilitySwitch"))
        //{
        //    bButton.text = "Dash";
        //    xButton.text = "Attack";
        //    yButton.text = "Counter";
        //}        
    }
}
