﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CheckpointManager : MonoBehaviour
{
    [SerializeField]
    private Transform player;

    public Dictionary<Checkpoint, bool> DiscoveredCheckpoints = new Dictionary<Checkpoint, bool>();

    public Checkpoint[] ThisStageCheckpoints;
    public Checkpoint lastVisitedCheckpoint;
    public Checkpoint lastDiscoveredCheckpoint;

    private void Start()
    {

        lastVisitedCheckpoint = ThisStageCheckpoints[SaveManager.data.lastCheckpointIndex];
        lastDiscoveredCheckpoint = ThisStageCheckpoints[SaveManager.data.lastDiscoveredCheckpoint];

        ThisStageCheckpoints = GetComponentsInChildren<Checkpoint>();

        foreach (Checkpoint checkpoint in ThisStageCheckpoints)
        {
            bool visited = ThisStageCheckpoints.ToList().IndexOf(checkpoint) <= ThisStageCheckpoints.ToList().IndexOf(lastDiscoveredCheckpoint);

           //Debug.Log(checkpoint.CheckpointSO.Name +" " + visited);

            DiscoveredCheckpoints.Add(checkpoint, visited);
        }     

        if (lastVisitedCheckpoint != null)
        {         
            player.transform.position = lastVisitedCheckpoint.transform.position + Vector3.right * 0.9f;
        }
  
    }

    public void SetLastVisitedCheckpoint(Checkpoint checkpoint)
    {
        lastVisitedCheckpoint = checkpoint;

        if (DiscoveredCheckpoints.Count > 0)
        {
            if (!DiscoveredCheckpoints[checkpoint])
            {
                lastDiscoveredCheckpoint = checkpoint;
                Debug.Log("New discovered checkpoint " + checkpoint.name);
            }
        }

        SaveManager.data.lastCheckpointIndex = ThisStageCheckpoints.ToList().IndexOf(lastVisitedCheckpoint);
        SaveManager.data.lastDiscoveredCheckpoint = ThisStageCheckpoints.ToList().IndexOf(lastDiscoveredCheckpoint);
        SaveManager.SaveData();
    }
    
}