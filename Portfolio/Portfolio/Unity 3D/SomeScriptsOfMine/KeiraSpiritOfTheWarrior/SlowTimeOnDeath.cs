﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(HealthController))]
public class SlowTimeOnDeath : MonoBehaviour
{
    [SerializeField]
    private float slowTimeAmount = 0.5f;

    [SerializeField]
    private float slowTimeDuration = 1.5f;

    private HealthController hc;

    private void Awake()
    {
        hc = GetComponent<HealthController>();

        hc.OnDeath += () =>
        {
            FindObjectOfType<TimeController>().SlowTimeForSeconds(slowTimeAmount, slowTimeDuration);
            GameObject tmpGO = new GameObject();
            tmpGO.transform.position = transform.position;
            Destroy(tmpGO, 5);

            FindObjectOfType<CameraController>().ChangeTargetForSeconds(tmpGO.transform, slowTimeDuration);
        };


    }
}


