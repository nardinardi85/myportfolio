﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatsManager : MonoBehaviour
{
    #region Fields

    [SerializeField]
    private ExpController EC;

    [SerializeField]
    private StaminaController SC;

    [SerializeField]
    private HealthController HC;

    [SerializeField]
    private RageController RC;

    [SerializeField]
    private Stat vitality;

    [SerializeField]
    private Stat endurance;

    [SerializeField]
    private Stat strength;

    [SerializeField]
    private Stat defense;

    [SerializeField]
    private SubStatUI subStatUIPrefab;

    [SerializeField]
    private RectTransform subStatUIContainer;

    [SerializeField]
    private GameObject confirmPanel;

    [SerializeField]
    private GameObject no_ConfirmPanel;

    [SerializeField]
    private float healthMultiplier = 1.07f;

    [SerializeField]
    private float rageConsumptionMultiplier = 0.985f;

    [SerializeField]
    private float staminaMultiplier = 1.05f;

    [SerializeField]
    private float damageLightAttackMultiplier = 1.1f;

    [SerializeField]
    private float damageHeavyAttackMultiplier = 1.08f;

    [SerializeField]
    private float attackStaminaConsumptionMultiplier = 0.98f;

    [SerializeField]
    private float dashGuardStaminaConsumptionMultiplier = 0.96f;

    [SerializeField]
    private AttackState playerAttackState;

    private List<Stat> stats = new List<Stat>();

    private Stat currentSelectedStat;

    private float lastTimeInput;
    private float timerInput = 0.25f;
    private List<SubStat> subStats = new List<SubStat>();
    private int prePurchasedLevel = 0;

    public Action OnLevelOrExpChange = delegate { };

    #endregion

    #region Properties

    public int CurrentLevel
    {
        get
        {
            int value = 0;
            foreach (var stat in stats)
            {
                value += stat.StatValue;
            }
            return value;
        }
    }

    public int CurrentExp { get; set; }
    public int RequiredExp
    {
        get
        {
            int totalXPNeeded = 0;
            for (int i = prePurchasedLevel; i < CurrentLevel; i++)
            {
                totalXPNeeded += XpNeeded(i + 1);
            }

            return totalXPNeeded;
        }
    }
    public int AfterSaleExp { get { return CurrentExp - RequiredExp; } }
    #endregion

    #region Methods

    private IEnumerator Start()
    {
        yield return null;

        CurrentExp = EC.Exp;

        stats.Add(vitality);
        stats.Add(endurance);
        stats.Add(strength);
        stats.Add(defense);

        SubStat health = new SubStat(new List<Stat> { vitality }, "Health", HC.MaxHp, healthMultiplier, HC);
        SubStat stamina = new SubStat(new List<Stat> { endurance }, "Stamina", SC.MaxStamina, staminaMultiplier, SC);
        SubStat rageConsumption = new SubStat(new List<Stat> { vitality, endurance, strength, defense }, "Rage Consumption", RC.RageLeak, rageConsumptionMultiplier, RC, true);

        SubStat damageLightAttack = new SubStat(new List<Stat> { strength }, "Damage Light Attack",
            playerAttackState.lightAttackDamage, damageLightAttackMultiplier, playerAttackState.DamageMultiplierGroupTable[DamageMultiplierGroup.Group1], true);


        SubStat damageHeavyAttack = new SubStat(new List<Stat> { strength }, "Damage Heavy Attack",
            playerAttackState.heavyAttackDamage, damageHeavyAttackMultiplier, playerAttackState.DamageMultiplierGroupTable[DamageMultiplierGroup.Group2], true);

        SubStat attackStaminaConsumption = new SubStat(new List<Stat> { strength }, "Attack Stamina Usage", playerAttackState.attackStaminaUsage, attackStaminaConsumptionMultiplier, playerAttackState, true);
        SubStat dashGuardConsumption = new SubStat(new List<Stat> { defense }, "Move Stamina Usage", RC.RageLeak, dashGuardStaminaConsumptionMultiplier, RC, true);

        subStats.Add(health);
        subStats.Add(stamina);
        subStats.Add(rageConsumption);
        subStats.Add(damageLightAttack);
        subStats.Add(damageHeavyAttack);
        subStats.Add(attackStaminaConsumption);
        subStats.Add(dashGuardConsumption);

        vitality.Init(SaveManager.data.vitalityValue, this);
        endurance.Init(SaveManager.data.enduranceValue, this);
        strength.Init(SaveManager.data.strengthValue, this);
        defense.Init(SaveManager.data.defenseValue, this);

        foreach (Transform child in subStatUIContainer)
        {
            Destroy(child.gameObject);
        }

        foreach (var item in subStats)
        {
            SubStatUI subStatUI = Instantiate(subStatUIPrefab, subStatUIContainer);
            subStatUI.Init(item);
        }

        UpdateSubStatsUI();

        prePurchasedLevel = CurrentLevel;

        OnLevelOrExpChange();

        SelectStat(stats[0]);

        ConfirmAll();
    }

    private void Update()
    {
        if (CanInput() && confirmPanel.activeSelf == false)
        {
            SelectStat(stats.Where(x => x.isSelected == true).FirstOrDefault());

            if (currentSelectedStat != null)
            {
                if (Input.GetAxisRaw("MenuNavHor") > 0)
                {

                    if (CurrentExp >= RequiredExp)
                    {
                        currentSelectedStat.IncreaseStatValue();
                        OnLevelOrExpChange();
                    }

                    lastTimeInput = Time.unscaledTime;

                }
                else if (Input.GetAxisRaw("MenuNavHor") < 0)
                {
                    currentSelectedStat.DecreaseStatValue();
                    OnLevelOrExpChange();

                    lastTimeInput = Time.unscaledTime;
                }

                if (Input.GetButtonDown("Submit"))
                {
                    if (RequiredExp > 0 && AfterSaleExp > 0)
                    {
                        ToggleConfirmPanel(true);
                    }
                    //else
                    //{
                    //    AudioManager.instance.UIBackSound();
                    //}
                }

                if (Input.GetButtonDown("Back"))
                {
                    ClearStats();
                    ToggleConfirmPanel(false);
                }

                foreach (var item in stats)
                {
                    item.RefreshUI();
                }

            }

        }

    }

    public void RefreshExp()
    {
        CurrentExp = EC.Exp;
        OnLevelOrExpChange();
        UpdateSubStatsUI();
        prePurchasedLevel = CurrentLevel;
    }

    //private void OnEnable()
    //{
    //    CurrentExp = EC.Exp;
    //    OnLevelOrExpChange();
    //    UpdateSubStatsUI();
    //    prePurchasedLevel = CurrentLevel;
    //}

    //private void OnDisable()
    //{
    //    ClearStats();
    //    ToggleConfirmPanel(false);

    //}

    private void UpdateSubStatsUI()
    {
        foreach (var item in subStats)
        {
            item.Calculate();
        }
    }


    public void ClearStats()
    {
        foreach (var stat in stats)
        {
            stat.Clear();
        }

        foreach (var item in subStats)
        {
            item.Clear();
        }
    }

    public void ToggleConfirmPanel(bool enable)
    {
        StartCoroutine(CoToggleConfirmPanel(enable));
    }

    private IEnumerator CoToggleConfirmPanel(bool enable)
    {
        yield return null;
        if(enable)
            confirmPanel.SetActive(enable);

        if (enable)
        {
            currentSelectedStat = null;
            EventSystem.current.SetSelectedGameObject(no_ConfirmPanel);
        }
        else
        {
            //EventSystem.current.SetSelectedGameObject(stats[0].gameObject);
            //SelectStat(stats[0]);
        }
    }

    public void ConfirmAll()
    {
        if (AfterSaleExp > 0)
        {
            foreach (var stat in stats)
            {
                if (stat.IsDirty)
                {
                    stat.Confirm();
                }
            }

            foreach (var item in subStats)
            {
                item.Apply();
            }

            EC.Exp = AfterSaleExp;
            CurrentExp = EC.Exp;
            prePurchasedLevel = CurrentLevel;
            OnLevelOrExpChange();

            SaveManager.data.currentExp = EC.Exp;
            SaveManager.SaveData();
        }

        SaveManager.data.vitalityValue = vitality.StatValue;
        SaveManager.data.enduranceValue = endurance.StatValue;
        SaveManager.data.strengthValue = strength.StatValue;
        SaveManager.data.defenseValue = defense.StatValue;

        SaveManager.SaveData();

       // ToggleConfirmPanel(false);
    }

    private void SelectStat(Stat stat)
    {
        currentSelectedStat = stat;
    }

    private bool CanInput()
    {
        return Time.unscaledTime >= lastTimeInput + timerInput;
    }

    public static int XpNeeded(int currentLevel)
    {

        int xpneeded;
        if (currentLevel <= 11)
        {
            xpneeded = Mathf.FloorToInt(0.0068f * Mathf.Pow(currentLevel, 3) - (0.06f * Mathf.Pow(currentLevel, 2)) + (17.1f * currentLevel) + 639);
        }
        else
        {
            xpneeded = Mathf.FloorToInt(0.02f * Mathf.Pow(currentLevel, 3) + (3.06f * Mathf.Pow(currentLevel, 2)) + (105.6f * currentLevel) - 895);
        }

        return xpneeded;
    }

    #endregion
}
