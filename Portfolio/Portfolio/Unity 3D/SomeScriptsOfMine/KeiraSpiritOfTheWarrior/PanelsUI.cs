﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelsUI : MonoBehaviour
{    
    PanelsInputController PIC;
    public GameObject DarkBackground;
    public Button destinationButton;
    public GameObject ExitButton;

    public void OnEnable()
    {
        if(!PIC.Panels.Contains(this))
            PIC.Panels.Push(this);
        if (ExitButton != null)
            ExitButton.SetActive(true);
        /*if (PIC.Panels.Peek().gameObject.name != "PauseMenu" && PIC.Panels.Peek().gameObject.name != "CheckpointMenu")
          {
          }*/
        if (PIC.Panels.Count == 1 && DarkBackground != null)
            DarkBackground.SetActive(true);


        PIC.AlphaFadeIn();

        Debug.Log("Ho " + PIC.Panels.Count + " elementi nella stack ed il mio PEEK è " + PIC.Panels.Peek().gameObject.name);
    }

    private void OnDisable()
    {
        //if (PIC.Panels.Peek().gameObject.name == "PauseMenu" && PIC.Panels.Peek().gameObject.name == "CheckpointMenu")
        //{
        //     PIC.Panels.Pop();
        //}
        if (ExitButton != null)
            ExitButton.SetActive(true);
        if (PIC.Panels.Count > 1)
        {
            if (GetComponentInChildren<CheckpointButtonUI>() != null)
            {
                //Debug.Log("CALLING CLEAR BY " + name);
                PIC.Panels.Clear();
            }
        }
        else
        {
            if(DarkBackground != null)
                DarkBackground.SetActive(false);
        }
    }

    private void Awake()
    {
        PIC = GetComponentInParent<PanelsInputController>();
    }

    public void Back()
    {        
        if(PIC.Panels.Peek().gameObject.name == "MainMenuPanel")
        {
            return;
        }

        AudioManager.instance.UIBackSound();
        if (PIC.Panels.Count > 1) // && !PIC.Panels.Peek().gameObject.activeSelf
        {
            Debug.Log("Il PEEK è " + PIC.Panels.Peek().gameObject.name);

            PIC.Panels.Pop();
            gameObject.SetActive(false);

            PIC.Panels.Peek().gameObject.SetActive(true);

            //PIC.Panels.Pop();
        }
        else if (PIC.Panels.Count == 1)
        {
            PIC.Panels.Peek().gameObject.SetActive(false);
            if (PIC.Panels.Peek().gameObject.name == "PauseMenu" || PIC.Panels.Peek().gameObject.name == "CheckpointMenu")
            {
                transform.root.GetComponent<PauseManager>().canPause = false;
            }
            else if(PIC.Panels.Peek().gameObject.name == "MainMenuPanel")
            {
                PIC.Panels.Push(this);
                PIC.Panels.Peek().gameObject.SetActive(true);
            }
            PIC.Panels.Pop();
        }
        ///Debug.Log("Ho Poppato ed ho " + PIC.Panels.Count + " elementi nella stack");

        if (destinationButton != null)
        destinationButton.Select();

        if (PIC.Panels.Count < 1)
            Time.timeScale = 1;
    }


}
