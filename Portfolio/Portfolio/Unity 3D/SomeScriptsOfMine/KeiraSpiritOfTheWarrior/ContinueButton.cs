﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Buttons))]
[RequireComponent(typeof(Button))]
public class ContinueButton : MonoBehaviour
{
    private Buttons buttons;
    private Button button;
    
    private void Start()
    {
        buttons = GetComponent<Buttons>();
        button = GetComponent<Button>();
        Debug.Log("Continueu start");

        if (SaveManager.data.timePlayed > 5)
        {
            EventSystem.current.SetSelectedGameObject(gameObject);

            buttons.isLocked = false;

            button.onClick.AddListener(() => ScenesManager.instance.SceneChangeAfterFadeIn(1));
            button.onClick.AddListener(() => button.interactable = false);
        }
    }
}
