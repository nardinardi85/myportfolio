﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Checkpoint", menuName = "Checkpoints")]
public class CheckpointSO : ScriptableObject
{
    public string Name;
    public byte Level;
    public byte Number;
    public Sprite LocationImage;
    public string TitleName; 
}
