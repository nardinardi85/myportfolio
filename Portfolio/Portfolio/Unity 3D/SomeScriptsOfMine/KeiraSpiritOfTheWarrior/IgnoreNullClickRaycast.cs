﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventSystem))]
public class IgnoreNullClickRaycast : MonoBehaviour
{

    private GameObject lastSelectedGameObject;
    private GameObject currentSelectedGameObject_Recent;
    private EventSystem eventSystem;

    private void Awake()
    {
        eventSystem = GetComponent<EventSystem>();
        lastSelectedGameObject = eventSystem.firstSelectedGameObject;
    }

    void Update()
    {
        GetLastGameObjectSelected();
    }
    public void GetLastGameObjectSelected()
    {
        if (eventSystem.currentSelectedGameObject == null && lastSelectedGameObject != null)
        {
            eventSystem.SetSelectedGameObject(lastSelectedGameObject);
            var buttons = lastSelectedGameObject.GetComponent<Buttons>();
            if (buttons != null && buttons.selectionImage != null)
                buttons.selectionImage.fillAmount = 1;
        }
        else
        {
            lastSelectedGameObject = eventSystem.currentSelectedGameObject;
        }
    }
}
