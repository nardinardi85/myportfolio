﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System;
using System.Threading;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SaveManager : MonoBehaviour
{

    /* Aggiungere al SaveManager o allo script ove contenuti i dati da salvare */
    public static Data data;

    public static Thread saveThread;

    private static string persistentDataPath;
    private const string dataFileName = "KeiraData.data";

    [SerializeField]
    private GameObject savingGameObject;

    private Coroutine savingImageCoroutine;

    [System.Serializable]
    public struct Data
    {
        public int lastCheckpointIndex;
        public int lastDiscoveredCheckpoint;
        public int currentExp;
        public bool shockwaveUnlocked;
        public bool wallbreakerUnlocked;
        public int vitalityValue;
        public int enduranceValue;
        public int defenseValue;
        public int strengthValue;

        public float timePlayed;
        
    }

    private void Awake()
    {
        persistentDataPath = Application.persistentDataPath;

        if (File.Exists(Application.persistentDataPath + "/"+ dataFileName))
        {
            data = LoadData().Last();
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            SaveData();
        }

        if (saveThread != null && saveThread.IsAlive)
        {
            if(savingImageCoroutine != null)
                StopCoroutine(savingImageCoroutine);

            savingImageCoroutine = StartCoroutine(CoShowSavingImage());

        }

    }

    private IEnumerator CoShowSavingImage()
    {
        savingGameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(2f);
        savingGameObject.SetActive(false);

    }

    public static void SaveData()
    {
        if (saveThread == null)
        {
            saveThread = new Thread(SaveDataJob);
            saveThread.Start();
        }
        
    }

    public static void SaveDataJob()
    {
        Debug.Log("SaveDataJob");
        List<Data> dataList = LoadData();
        dataList.Add(data);
       
        BinaryFormatter formatter = new BinaryFormatter();
        string path = persistentDataPath + "/" + dataFileName;

        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, dataList);
        stream.Close();

        saveThread = null;
    }

    public static List<Data> LoadData()
    {
        string path = persistentDataPath + "/" + dataFileName;

        if (File.Exists(path))
        {
            //Debug.Log("Save file found in " + path);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            List<Data> data = formatter.Deserialize(stream) as List<Data>;
            stream.Close();


            return data;
        }
        else
        {
            Debug.Log("Save file NOT found in " + path);
            List<Data> data = new List<Data>();
            return data;
        }
    }

    public static bool FileExists()
    {
        string path = persistentDataPath + "/" + dataFileName;
        if (File.Exists(path))
        {
            return true;
        }
        else return false;
    }

#if UNITY_EDITOR
    [MenuItem("Data/Reset Data")]
#endif
    public static void ResetData()
    {
        data = new Data();

        SaveData();
    }

#if UNITY_EDITOR
    [MenuItem("Data/Reset Prefs")]
#endif
    public static void ResetPrefsEditor()
    {
        PlayerPrefs.DeleteAll();
    }

    public void ResetPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

#if UNITY_EDITOR
    [MenuItem("Data/Display Time Played")]
#endif
    public static void DisplayTimePlayed()
    {
        Debug.Log(data.timePlayed);
        TimeSpan t = TimeSpan.FromSeconds(data.timePlayed);

        string answer = string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
                        t.Hours,
                        t.Minutes,
                        t.Seconds);

        Debug.Log(answer);
    }

}


