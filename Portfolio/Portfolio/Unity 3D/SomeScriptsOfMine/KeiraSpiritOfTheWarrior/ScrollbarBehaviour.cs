﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ScrollbarBehaviour : MonoBehaviour
{

    public float scrollSpeed = 10f;

    //ScrollRect m_ScrollRect;
    //RectTransform m_ContentRectTransform;

    [SerializeField]
    CheckpointManagerUI checkpointManagerUI;

    //[SerializeField]
    //Scrollbar scrollbar;

    [SerializeField]
    GameObject upperArrow;
    Image upperArrowImage;

    [SerializeField]
    GameObject lowerArrow;
    Image lowerArrowImage;

    void Awake()
    {
       // m_ScrollRect = GetComponent<ScrollRect>();
       // m_ContentRectTransform = m_ScrollRect.content;
        upperArrowImage = upperArrow.GetComponent<Image>();
        lowerArrowImage = lowerArrow.GetComponent<Image>();
    }

    private void OnEnable()
    {
        if(checkpointManagerUI.checkpointButtons.Count == 1)
        {
            upperArrowImage.enabled = false;
            lowerArrowImage.enabled = false;
        }
        else if(checkpointManagerUI.checkpointButtons.Count != 1 && checkpointManagerUI.checkpointButtons[0].isSelected)
        {
            upperArrowImage.enabled = false;
            lowerArrowImage.enabled = true;
        }
        else if (checkpointManagerUI.checkpointButtons.Count != 1 && checkpointManagerUI.checkpointButtons[checkpointManagerUI.checkpointButtons.Count - 1].isSelected)
        {
            upperArrowImage.enabled = true;
            lowerArrowImage.enabled = false;
        }
    }

    void Update()
    {
        if(Input.GetButtonDown("MenuNavVert"))
        {
            upperArrowImage.enabled = true;
            lowerArrowImage.enabled = true;
           // ScrollToChildOfContent(EventSystem.current.currentSelectedGameObject.GetComponent<RectTransform>());
            if (checkpointManagerUI != null && checkpointManagerUI.checkpointButtons[0].isSelected)
            {
                //scrollbar.value = 1;
                upperArrowImage.enabled = false;
                //lowerArrowImage.enabled = true;
            }
            if (checkpointManagerUI != null && checkpointManagerUI.checkpointButtons[checkpointManagerUI.checkpointButtons.Count-1].isSelected)
            {
                //scrollbar.value = 0;
                lowerArrowImage.enabled = false;
            } 
        }       

    }

    //private void ScrollToChildOfContent(RectTransform rectToScrollTo)
    //{
    //    float scrollViewHeight = m_ScrollRect.viewport.sizeDelta.y;
    //    float scrollViewMid = -scrollViewHeight / 2f;

    //    if (rectToScrollTo.anchoredPosition.y > scrollViewMid)
    //    {
    //        //Debug.Log("the rect is in the first half of the scroll view: no need to scroll");

    //        return;
    //    }

    //    float contentEnd = -m_ScrollRect.content.sizeDelta.y;

    //    if (rectToScrollTo.anchoredPosition.y < contentEnd - scrollViewMid)
    //    {
    //        //Debug.Log("the rect is at the very bottom of the content, so just scroll there");

    //        m_ScrollRect.verticalNormalizedPosition = 0f;

    //        return;
    //    }

    //    float normPosTopToBottom = Mathf.InverseLerp(scrollViewMid, contentEnd - scrollViewMid, rectToScrollTo.anchoredPosition.y);
    //    float normPosBottomToTop = 1f - normPosTopToBottom;

    //    m_ScrollRect.verticalNormalizedPosition = normPosBottomToTop;
    //}

}

 