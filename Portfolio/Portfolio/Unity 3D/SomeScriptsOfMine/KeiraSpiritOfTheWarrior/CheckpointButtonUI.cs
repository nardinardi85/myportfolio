﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[RequireComponent(typeof(Button))]
public class CheckpointButtonUI : Buttons
{
    [SerializeField]
    private TMP_Text buttonText;

    public Checkpoint Target { get; private set; }

    public void Init(Checkpoint target)
    {
        Target = target;
        button.onClick.RemoveAllListeners();
        buttonText.text = target.CheckpointSO.Name;
        button.onClick.AddListener(() => target.Teleport());
    }
        
    
    //private void Update()
    //{
    //    if(!isSelected)
    //    {
    //        selectionImage.color = defaultColor;
    //        selectionImageFill = 0;
    //    }
    //}

}
