﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using TMPro;

public class CheckpointManagerUI : MonoBehaviour
{
    public CheckpointManager checkpointManager;

    [SerializeField]
    private GameObject buttonPrefab;

    [SerializeField]
    private Transform buttonsContainer;

    [SerializeField]
    private Image checkpointImage;

    [SerializeField]
    private GameObject checkpointPanel;

    public GameObject discoveredCheckpointPanel;

    public TMP_Text discoveredCheckpointText;

    public List<CheckpointButtonUI> checkpointButtons = new List<CheckpointButtonUI>();

    [HideInInspector]
    public CheckpointButtonUI checkpointButton;

    //Checkpoint checkpoint;

    public void Disable()
    {
        checkpointPanel.SetActive(false);
    }

    public void EnableAtCheckpoint(int number)
    {
        checkpointPanel.SetActive(true);

        foreach(var discoveredCheckpoint in checkpointManager.DiscoveredCheckpoints)
        {
            if (discoveredCheckpoint.Value == true)
            {
                if (checkpointButtons.Count < checkpointManager.ThisStageCheckpoints.Length && checkpointButtons.Where(x=>x.Target == discoveredCheckpoint.Key).Count() == 0)
                {    
                    checkpointButton = Instantiate(buttonPrefab, buttonsContainer).GetComponent<CheckpointButtonUI>();
                    checkpointButton.name = "Checkpoint " + discoveredCheckpoint.Key.CheckpointSO.Number;
                    checkpointButtons.Add(checkpointButton);

                    checkpointButton.Awake();
                    checkpointButton.Init(discoveredCheckpoint.Key);
                    checkpointButton.OnButtonSelected += () => checkpointImage.sprite = discoveredCheckpoint.Key.CheckpointSO.LocationImage;
                }                             
            }
        }

        //for (int i = 0; i < checkpointManager.ThisStageCheckpoints.Length; i++)
        //{
        //    if (checkpointButtons.Count < checkpointManager.ThisStageCheckpoints.Length)
        //    {
        //        var checkpoint = checkpointManager.ThisStageCheckpoints[i];

        //        checkpointButton = Instantiate(buttonPrefab, buttonsContainer).GetComponent<CheckpointButtonUI>();
        //        checkpointButton.name = "Checkpoint " + i;
        //        checkpointButtons.Add(checkpointButton);


        //        checkpointButton.Awake();
        //        checkpointButton.Init(checkpoint);
        //        checkpointButton.OnButtonSelected += () => checkpointImage.sprite = checkpoint.CheckpointSO.LocationImage;
        //    }
        //}

        SelectButton(number);
    }

    private void OnDisable()
    {
        //checkpointButtons.Clear();
    }

    private void SelectButton(int j)
    {
        if (checkpointButtons.Count <= 0)
            return;
        //EventSystem.current.SetSelectedGameObject(null);
        checkpointButtons.Where(x=>x.Target.CheckpointSO.Number - 1 == j).FirstOrDefault().isSelected = true;
        //checkpointButtons[j].button.Select();
    }

    private void LateUpdate()
    {
        if(checkpointButtons[checkpointButtons.Count - 1].isSelected)
        {
            if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxis("MenuNavVertJoystick") < 0)
            {
                EventSystem.current.SetSelectedGameObject(checkpointButtons[checkpointButtons.Count - 1].gameObject);
                checkpointButtons[checkpointButtons.Count - 1].GetComponent<Button>().Select();
                checkpointButtons[checkpointButtons.Count - 1].selectionImage.fillAmount = 1;
            }            
        }
    }
}
