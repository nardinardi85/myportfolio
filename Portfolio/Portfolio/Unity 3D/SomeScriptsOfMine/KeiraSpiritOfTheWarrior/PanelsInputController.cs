﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelsInputController : MonoBehaviour
{    
    public Stack<PanelsUI> Panels = new Stack<PanelsUI>();
    public float fillingLerpValue = 0.1f;
    public float alphaFade;
    public float alphaStartingValue;
    bool alphaFadeStarted = false;
    public List<TextMeshProUGUI> Texts = new List<TextMeshProUGUI>();
    public List<Image> Images = new List<Image>();
    public GameObject Bars, Powers, Essence;

    [SerializeField]
    Image targetImage;

    private void Update()
    {
        if (Panels.Count != 0)
        {
            if(Bars != null)
                Bars.SetActive(false);
            if (Powers != null)
                Powers.SetActive(false);
            if (Essence != null)
                Essence.SetActive(false);
        }
        else
        {
            if (Bars != null)
                Bars.SetActive(true);
            if (Powers != null)
                Powers.SetActive(true);
            if (Essence != null)
                Essence.SetActive(true);
        }

        if (alphaFadeStarted)
        {
            alphaFade += fillingLerpValue * Time.fixedUnscaledDeltaTime;
            foreach (Image image in Images)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Clamp01(alphaFade));

                if (image.name == "DarkBackground")
                {
                    if (image.color.a >= 0.7f)
                    {
                        image.color = new Color(image.color.r, image.color.g, image.color.b, 0.8f);
                    }
                }
                else
                {
                    if (image.color.a >= 0.9f)
                    {
                        image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
                    }
                }
                
            }

            foreach(TextMeshProUGUI text in Texts)
            {
                text.color = new Color(1, 1, 1, Mathf.Clamp01(alphaFade));
                if (text.color.a >= 0.9f)
                {
                    text.color = Color.white;
                }
            }

            if (Images.Count > 0 && Texts.Count > 0)
            {
                if (Images[Images.Count - 1].color == Color.white && Texts[Texts.Count - 1].color == Color.white)
                {
                   // Debug.Log("Clearo Liste");
                    alphaFadeStarted = false;
                    alphaFade = 0;
                    Images.Clear();
                    Texts.Clear();
                }
            }
        }

        if (Input.GetButtonDown("Back") && Panels.Count > 0)
        {
            Panels.Peek().Back();

            if (Panels.Count <= 0)
            {
                Time.timeScale = 1;

            }
            else
            {
                Time.timeScale = 0;
            }
        }
    }

    public void StackClear()
    {
        Panels.Clear();
    }

    public void AlphaFadeIn()
    {
        alphaFade = 0;
        Images.AddRange(Panels.Peek().GetComponentsInChildren<Image>());
        Texts.AddRange(Panels.Peek().GetComponentsInChildren<TextMeshProUGUI>());

        foreach (Image image in Images)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
        }
        foreach (TextMeshProUGUI text in Texts)
        {
            text.color = new Color(1, 1, 1, 0);
        }

        alphaFadeStarted = true;
    }

}
