﻿using UnityEngine;

[RequireComponent(typeof(HealthController))]
public class DropGlobesOnDeath : MonoBehaviour, IComponentInjection<GlobeSpawner>
{
    [SerializeField]
    private GlobeSpawnData[] drop;

    private HealthController hc;
    public GlobeSpawner InjectedObject { get; set; }

    private void Awake()
    {
        hc = GetComponent<HealthController>();

        hc.OnDeath += () => InjectedObject.SpawnRandomGlobe(drop, transform.position);
    }

}

