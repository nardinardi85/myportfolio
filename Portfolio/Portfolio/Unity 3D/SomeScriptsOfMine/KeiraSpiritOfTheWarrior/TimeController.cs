﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeController : MonoBehaviour
{
    [SerializeField]
    float timeMult=0.2f;


    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.O))
        //{
        //    Chronos(timeMult);
        //}
    }

    public void Chronos(float timeMult)
    {
        Time.timeScale = timeMult;
    }

    public void SlowTimeForSeconds(float amount, float seconds)
    {
        StopAllCoroutines();

        StartCoroutine(CoSlowTime(amount, seconds));
    }

    public IEnumerator CoSlowTime(float amount, float seconds)
    {
        Time.timeScale = amount;

        yield return new WaitForSecondsRealtime(seconds);

        Time.timeScale = 1;

    }
}
