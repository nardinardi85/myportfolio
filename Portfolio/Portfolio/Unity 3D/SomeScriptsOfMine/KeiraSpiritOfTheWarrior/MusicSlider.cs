﻿using UnityEngine;

[RequireComponent(typeof(Buttons))]
public class MusicSlider : MonoBehaviour
{
    private Buttons buttons;

    private void Start()
    {
        buttons = GetComponent<Buttons>();
        buttons.OnChoiceUpdate += UpdateMusicVolume;

        buttons.selectedIndex = PlayerPrefs.GetInt("music_volume", 10);
        buttons.UpdateChoice();

    }

    private void UpdateMusicVolume()
    {
        PlayerPrefs.SetInt("music_volume", buttons.selectedIndex);

        SetVolume();

        PlayerPrefs.Save();
    }

    public void SetVolume()
    {
        AudioManager.instance.MusicBus.setVolume(PlayerPrefs.GetInt("music_volume", 10));
    }
}