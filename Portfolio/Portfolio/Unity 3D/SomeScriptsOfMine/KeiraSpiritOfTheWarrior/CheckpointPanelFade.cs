﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CheckpointPanelFade : MonoBehaviour
{
    public float fillingLerpValue = 0.2f;
    public float alphaFadeIn;
    public float alphaFadeOut;

    bool alphaFadeStarted = false;
    bool alphaFadeOutStarted = false;

    public List<TextMeshProUGUI> Texts = new List<TextMeshProUGUI>();
    public List<Image> Images = new List<Image>();

    private void OnEnable()
    {
        AlphaFadeIn();
    }

    private void Update()
    {
        if (alphaFadeStarted)
        {
            alphaFadeIn += fillingLerpValue * Time.fixedUnscaledDeltaTime;

            foreach (Image image in Images)
            {
                image.color = new Color(1, 1, 1, Mathf.Clamp01(alphaFadeIn));
                if (image.color.a >= 0.9f)
                {
                    image.color = Color.white;
                }
            }

            foreach (TextMeshProUGUI text in Texts)
            {
                text.color = new Color(1, 1, 1, Mathf.Clamp01(alphaFadeIn));
                if (text.color.a >= 0.9f)
                {
                    text.color = Color.white;
                }
            }

            if (Images.Count > 0 && Texts.Count > 0)
            {
                if (Images[Images.Count - 1].color == Color.white && Texts[Texts.Count - 1].color == Color.white)
                {
                    alphaFadeStarted = false;
                    alphaFadeIn = 0;
                    StartCoroutine(FadeOutDelay());
                 
                }
            }
        }

        if (alphaFadeOutStarted)
        {
            alphaFadeOut += fillingLerpValue * Time.fixedUnscaledDeltaTime;
            
            foreach (Image image in Images)
            {
                image.color = new Color(1, 1, 1, Mathf.Clamp01(1 - alphaFadeOut));
                if (image.color.a <= 0.1f)
                {
                    image.color = new Color(1, 1, 1, 0);
                }
            }

            foreach (TextMeshProUGUI text in Texts)
            {
                text.color = new Color(1, 1, 1, Mathf.Clamp01(1 - alphaFadeOut));
                if (text.color.a <= 0.1f)
                {
                    text.color = new Color(1, 1, 1, 0);
                }
            }

            if (Images.Count > 0 && Texts.Count > 0)
            {
                if (Images[Images.Count - 1].color == new Color(1, 1, 1, 0) && Texts[Texts.Count - 1].color == new Color(1, 1, 1, 0))
                {
                    alphaFadeOutStarted = false;
                    alphaFadeOut = 0;
                    alphaFadeIn = 0;
                    Images.Clear();
                    Texts.Clear();
                    gameObject.SetActive(false);
                }
            }
        }
    }

    public void AlphaFadeIn()
    {
        Images.AddRange(GetComponentsInChildren<Image>());
        Texts.AddRange(GetComponentsInChildren<TextMeshProUGUI>());
        //foreach (Image image in Images)
        //{
        //    if (image.gameObject.name == "Pennellata")
        //    {
        //        Images.Remove(image);
        //        break;
        //    }
        //}
        alphaFadeStarted = true;
    }

    public void AlphaFadeOut()
    {
        alphaFadeOut = 0;
        alphaFadeOutStarted = true;        
    }

    public IEnumerator FadeOutDelay()
    {
        yield return new WaitForSecondsRealtime(2);
        AlphaFadeOut();
    }
}
