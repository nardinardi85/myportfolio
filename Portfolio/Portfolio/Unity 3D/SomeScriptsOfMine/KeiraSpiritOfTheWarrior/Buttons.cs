﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Buttons : MonoBehaviour, ISelectHandler, IDeselectHandler, ISubmitHandler
{
    [SerializeField, Tooltip("Only for long buttons with horizontal navigation")]
    protected float originalTimer = 0.25f;
    protected float timer;
    public int selectedIndex;
    int changedIndex;

    public bool isDefaultButton;

    public bool isLocked;

    [SerializeField]
    private bool isLongButton;

    [SerializeField]
    private bool isSfxButton;

    [SerializeField]
    private bool isMusicButton;

    [SerializeField]
    private bool isDynamicHUDButton;

    [SerializeField]
    private bool isDifficultyButton;

    public bool isSelected;

    [HideInInspector]
    public RectTransform myRectTransform;
    [HideInInspector]
    public Button button;
    public Image selectionImage;
    private TMP_Text buttonTxt;
    [HideInInspector]
    public float selectionImageFill;

    [SerializeField]
    private float fillingLerpValue = 0.2f;

    public float resizeValue;

    private Vector3 scale;
    private Vector3 defaultScale;

    [SerializeField]
    private Color color;
    [HideInInspector]
    public Color defaultColor = Color.white;

    public Action OnButtonSelected = delegate { };

    List<TMP_Text> Choices = new List<TMP_Text>();
    List<Image> Arrows = new List<Image>();
    public Action OnChoiceUpdate = delegate { };

    [SerializeField]
    private TextMeshProUGUI tooltipText;

    public void OnDisable()
    {
        selectionImage.fillAmount = 0;
        selectionImage.color = defaultColor;
        selectionImageFill = 0;
    }

    public void Awake()
    {        
        button = GetComponent<Button>();
        if (isLocked)
            button.onClick.RemoveAllListeners();

        selectionImage = GetComponent<Image>();
        buttonTxt = GetComponentInChildren<TMP_Text>();
        myRectTransform = GetComponent<RectTransform>();
        selectionImageFill = 0;
        defaultScale = myRectTransform.localScale;
        scale = defaultScale * resizeValue;

        if (isDefaultButton)
        {
            isSelected = true;                    
        }

        if(isLongButton)
        {
            //selectionImage.color = Color.cyan;            
            Choices.AddRange(GetComponentsInChildren<TMP_Text>());
            Arrows.AddRange(GetComponentsInChildren<Image>());
            //LeftAndRightNavigation();
            Arrows.Remove(Arrows[0]);
            Choices.Remove(Choices[0]);

            for (int i = 0; i < Choices.Count; i++)
            {
               /* if (Choices[i].gameObject.name == "Left_Text")
                {
                    Choices.Remove(Choices[i]);
                }*/

                Choices[i].gameObject.SetActive(false);

                //if (Choices[i] != Choices[0])
                //{
                //    Choices[i].gameObject.SetActive(false);
                //}

                //selectedIndex = i;
            }

            Choices[selectedIndex].gameObject.SetActive(true);
        }
        //else
        //{
            
        //    gameObject.GetComponent<RectTransform>().rect.width = 350;
        //    gameObject.GetComponent<RectTransform>().rect.height = 100;
        //}

        if (isSfxButton)
        {
            PlayerPrefs.GetInt("SFX Volume", changedIndex);
            selectedIndex = changedIndex;
        }
        else if (isMusicButton)
        {
            PlayerPrefs.GetInt("Music Volume", changedIndex);
            selectedIndex = changedIndex;
        }
        else if (isDynamicHUDButton)
        {
            PlayerPrefs.GetInt("Dynamic HUD", changedIndex);
            selectedIndex = changedIndex;
        }
        else if (isDifficultyButton)
        {
            PlayerPrefs.GetInt("Difficulty", changedIndex);
            selectedIndex = changedIndex;
        }
    }

    private void Start()
    {
        if (isLocked)
        {
            Debug.Log(name + " is Locked");
            buttonTxt.color = Color.red;
            selectionImage.color = Color.black;
        }
        else
        {
            buttonTxt.color = Color.black;
            selectionImage.color = defaultColor;
        }
    }

    protected void Update()
    {
        if(isSelected)
        {
            button.Select();
            if (buttonTxt != null)
            {
                buttonTxt.color = Color.yellow;
            }
            if (selectionImage != null && selectionImage.fillAmount < 1)
            {
                selectionImageFill += fillingLerpValue * Time.fixedUnscaledDeltaTime;
                selectionImage.fillAmount = Mathf.Clamp01(selectionImageFill);
            }
            if (isLocked)
            {
                buttonTxt.color = Color.red;
                selectionImage.color = Color.black;
            }

            myRectTransform.localScale = scale;
        }

        if(isLongButton)
        {
            LeftAndRightNavigation();
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (tooltipText != null)
        {
            if (isDynamicHUDButton)
                tooltipText.text = "Hide HUD during exploration phase";
            else if (isMusicButton)
            {
                tooltipText.text = "Set game music volume";
            }
            else if (isSfxButton)
            {
                tooltipText.text = "Set game sound effects volume";
            }
        }

        if(tooltipText != null)
            tooltipText.gameObject.SetActive(true);

        //Debug.Log(gameObject.name + " was selected");
        if (!isLocked)
        {
            selectionImageFill += fillingLerpValue * Time.fixedUnscaledDeltaTime;
            selectionImage.fillAmount = Mathf.Clamp01(selectionImageFill);            
            myRectTransform.localScale = scale;
        }
        else
        {
            buttonTxt.color = Color.red;
            selectionImage.color = Color.black;
        }        
        isSelected = true;
        AudioManager.instance.UISelectedButtonSound();
        //Debug.Log("Selected " + gameObject.name);
        
        OnButtonSelected();
    }


    public void OnDeselect(BaseEventData eventData)
    {
        if (tooltipText != null)
        {
            tooltipText.text = "";
            tooltipText.gameObject.SetActive(false);
        }


        if (buttonTxt != null)
        {
            buttonTxt.color = defaultColor;
        } 
        if(isLocked)
        {
            buttonTxt.color = Color.red;
        }
        myRectTransform.localScale = defaultScale;
        selectionImage.fillAmount = 0;
        selectionImageFill = 0;
        isSelected = false;
    }

    public void DisableGameObject(GameObject go)
    {
        go.SetActive(false);
    }

    public void EnableGameObject(GameObject go)
    {
        go.SetActive(true);
    }

    public void Toggle(GameObject  go)
    {
        bool active = go.activeSelf;
        if (!active)
        {
            go.SetActive(true);
        }
        else
        {
            go.SetActive(false);
        }
    }

    public void LeftAndRightNavigation()
    {
        if (selectedIndex == 0)
        {
            Arrows[0].gameObject.SetActive(false);
            Arrows[1].gameObject.SetActive(true);
        }
        else if (selectedIndex == Choices.Count - 1)
        {
            Arrows[1].gameObject.SetActive(false);
            Arrows[0].gameObject.SetActive(true);
        }
        else
        {
            Arrows[0].gameObject.SetActive(true);
            Arrows[1].gameObject.SetActive(true);
        }

        if (isSelected)
        {
            timer -= Time.unscaledDeltaTime; //Needed by the Pause Menu, which changes the time scale
            
            if (Input.GetAxisRaw("MenuNavHor") > 0 && timer <= 0)
            {
               
                if (selectedIndex < Choices.Count-1)
                {
                    AudioManager.instance.UISliderSound();
                    selectedIndex += 1;

                    UpdateChoice();
                }
                timer = originalTimer;
            }
            if (Input.GetAxisRaw("MenuNavHor") < 0 && timer <= 0)
            {
                
                if (selectedIndex > 0)
                {
                    AudioManager.instance.UISliderSound();
                    selectedIndex -= 1;

                    UpdateChoice();
                }
                timer = originalTimer;
            }
        }
        if (isSfxButton)
        {
            changedIndex = selectedIndex;
            PlayerPrefs.SetInt("SFX Volume", changedIndex);
        }
        else if (isMusicButton)
        {

            changedIndex = selectedIndex;
            PlayerPrefs.SetInt("Music Volume", changedIndex);
        }
        else if (isDynamicHUDButton)
        {
            changedIndex = selectedIndex;
            PlayerPrefs.SetInt("Dynamic HUD", changedIndex);
        }
        else if (isDifficultyButton)
        {
            changedIndex = selectedIndex;
            PlayerPrefs.SetInt("Difficulty", changedIndex);
        }
       // Debug.Log(changedIndex);
    }

    public void UpdateChoice()
    {
        foreach (var item in Choices)
        {
            item.gameObject.SetActive(false);
        }

        Choices[selectedIndex].gameObject.SetActive(true);

        OnChoiceUpdate();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        AudioManager.instance.UISelectionButtonSound();
    }
}
