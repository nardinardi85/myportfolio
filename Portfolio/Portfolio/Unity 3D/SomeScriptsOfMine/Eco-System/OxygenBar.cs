﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OxygenBar : MonoBehaviour
{
    public Slider oxygenBar;

    [Range(0.0001f, 1f), Tooltip("Percentage where the bar become red")]
    public float criticValue;

    public Color safeColor;

    public Color dangerColor;

    public Color barFullColor;

    [Tooltip("Image of the area to fill")]
    public Image fillImage;

    [Tooltip("Max scale it has to reach in dangerous zone")]
    public float maxScale;

    float startingScale;

    float currentScale;

    bool increasingScale;

    bool decreasingScale;


    void Start()
    {
        fillImage = GetComponent<Image>();
        startingScale = oxygenBar.transform.localScale.x;
        currentScale = startingScale;
    }

    void FixedUpdate()
    {

       
        // behaviour when in critical zone
        if (oxygenBar.value < criticValue)
        {
            fillImage.color = dangerColor;
            AudioManager.instance.PlaySingleSound("LowOxygen");

            if ( currentScale <= startingScale )
            {
                increasingScale = true;
                decreasingScale = false;
            }
            if ( currentScale >= maxScale )
            {
                increasingScale = false;
                decreasingScale = true;
            }
            


            if(increasingScale)
            {
                currentScale += 2f*Time.deltaTime;
                oxygenBar.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            }
            if (decreasingScale)
            {
                currentScale -= 2f*Time.deltaTime;
                oxygenBar.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            }


        }
        // behaviour in safe zone
        if (oxygenBar.value > criticValue && oxygenBar.value <1) 
        {
            fillImage.color = safeColor;
            oxygenBar.transform.localScale = new Vector3(startingScale, startingScale, startingScale);
            currentScale = startingScale;

        }
        // when full
        if(oxygenBar.value == 1)
        {
            fillImage.color = barFullColor;
        }
    }

}
