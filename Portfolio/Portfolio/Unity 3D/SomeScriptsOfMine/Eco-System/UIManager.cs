﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("UI SPRITE REFERENCES"), Tooltip("Run Button Instance")]
    public GameObject runBtn;

    [Tooltip("Teleport Button Instance")]
    public GameObject teleportBtn;

    [Tooltip("Pause Button Instance")]
    public GameObject[] pauseBtns;

    [Tooltip("Continue Button Instance in Pause Menu panel")]
    public GameObject continueBtn;

    [Tooltip("Sprite of the button while toggled")]
    public Sprite runBtnSpriteRunning;

    [Tooltip("Sprite of the button while the player is idle")]
    public Sprite runBtnSpriteDefault;

    [Tooltip("Sprite of the button while in outer space")]
    public Sprite teleportBtnSpriteSpace;

    [Tooltip("Sprite of the button while the player is on a planet")]
    public Sprite teleportBtnSpriteDefault;

    [Tooltip("Sprite of the button while in outer space")]
    public Sprite pauseBtnSpritePaused;

    [Tooltip("Sprite of the button while the player is on a planet")]
    public Sprite pauseBtnSpriteDefault;

    [Space (5), Header("IN GAME REFERENCES"), Tooltip("Reference to the PLAYER")]
    public PlayerManager player;

    [Tooltip("Rerefence to the GALAXY CLUSTER - the father object of all the planets used to count the number of planets")]
    public GalaxyManager galaxyCluster;

    [Tooltip("Reference to the PLANET UI section of the canvas that will be shown shile on a planet")]
    public GameObject planetUI;

    [Tooltip("Reference to the SPACE UI section of the canvas that will be shown while in space")]
    public GameObject spaceUI;

    [Tooltip("Reference to the QUEST CONTAINER panel in the CANVAS - PLANET UI hierarchy")]
    public GameObject questContainer; //panel in the canvas of the HUD

    [Tooltip("Reference to the QUEST CONTAINER panel in the CANVAS - SPACE UI hierarchy")]
    public GameObject spaceQuestContainer; //panel in the canvas of the HUD

    [Tooltip("Reference to the QUEST PANEL prefab")]
    public GameObject questLinePanel; // the panel contains a single type of seed - a quest is made of multiple panels

    [Tooltip(" Reference to the basic line segment image")]
    public GameObject lineSegment; //seeds to be planted black icon

    [Tooltip("To manage the passage to the last scene")]
    public ScenesManager sceneManager;

    [Tooltip("Array of the texts in the CLEANED PLANET element in both space and planet UIs")]
    public Text[] cleanedPlanetsTexts;

    [Tooltip("Speed in seconds of the color filling in the quest UI when a correct tree is planted")]
    public float fillspeed = 1f;

    [Tooltip("Delay in seconds of the color filling in the quest UI when a correct tree is planted")]
    public float delay = 1f;

    //color of the "dead planet image"
    SpriteRenderer planetBlend;

    PlanetManager currentPlanet;

    GameObject currentQuestContainer;

    Quest currentQuest;

    bool questComplete = false;

    int planetNumber;

    int alivePlanetNumber;

    float totalQuestTrees = 0; // total number of trees for the quest 

    float dampVelocity = 0f;

    readonly float dampDistance = 0.01f;


    // AWAKE - event subscription
    private void Awake()
    {
        TreeDisplayer.UpdateQuestStats += UpdateGUI; // quando un albero lancia il suo "update quest stats" - lancio un UpdateGUI
    }

    private void OnDisable()
    {
        TreeDisplayer.UpdateQuestStats -= UpdateGUI;
    }

    // START - initialization of the quest and UI planet count
    private void Start()
    {
        planetBlend = player.currentPlanet.deadPlanetRend;

        planetNumber = galaxyCluster.gameObject.transform.childCount;

        foreach (Text iconText in cleanedPlanetsTexts)
        {
            iconText.text = "0 / " + planetNumber.ToString();
        }

        InitializeQuest(gameObject);
    }

   

    #region ENEABLE / DISEABLE UIs
    // DISABLE UI - when in space, PLANET UI must be disabled
    public void DisablePlanetUI()

    {
        if (!player.isMovingRight && !player.isPlanting && player.nearShip)
        {
            planetUI.SetActive(false);
        }
    }

    public void EneablePlanetUI()
    {
        planetUI.SetActive(true);
    }

    // DISABLE SPACE UI - when landing on a planet, SPACE UI must be disabled
    public void DisableSpaceUI()
    {
        spaceUI.SetActive(false);
    }

    // ENEABLE SPACE UI - when in space, SPACE UI must be eneabled
    public void EneableSpaceUI()
    {
        spaceUI.SetActive(true);
    }

    #endregion


    #region  QUEST MANAGE 

    // INITIALIZE QUEST GRAPHICS - Quest UI is created from zero
    public void InitializeQuest(GameObject targetPlanet)
    {
        questComplete = false;
        if (targetPlanet.GetComponent<PlanetManager>() == null) // we already are on a planet - autofill
        {
            currentPlanet = player.currentPlanet;
            currentQuestContainer = questContainer;
        }
        else // planet is specified by the hover mouse position when pointing at it from the space
        {
            currentPlanet = targetPlanet.GetComponent<PlanetManager>();
            currentQuestContainer = spaceQuestContainer;
        }

        foreach (Transform questLine in currentQuestContainer.transform)
        {
            Destroy(questLine.gameObject);
        }

        currentQuest = currentPlanet.planetQuest;

        for (int j = 0; j < currentQuest.questLines.Length; j++) //cycle the number of quests
        {
            GameObject newQuestLinePanel = Instantiate(questLinePanel, currentQuestContainer.transform); //instantiate a prefab panel for each type of tree required by the quest
            newQuestLinePanel.GetComponent<Image>().sprite = currentQuest.questLines[j].treeType.lineBackground;

            for (int segmentsNumber = 0; segmentsNumber < currentQuest.questLines[j].treeQuantity; ++segmentsNumber)
            {
                GameObject newSegment = Instantiate(lineSegment, transform);
                newSegment.transform.SetParent(newQuestLinePanel.transform.GetChild(0).transform); // child 0 of the quest line panel is the segment container               
            }
        }
       
        StartCoroutine(FrameWait());
    }

    // FRAME WAIT FOR ICONS INSTANTIATION
    IEnumerator FrameWait()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        UpdateGUI();
    }

    // UPDATE GUI - quest UI is updated according to the current planet situation
    public void UpdateGUI()
    {
        totalQuestTrees = 0;

        if (player.currentPlanet != null)
        {
            if (!currentPlanet.backgroundGenerated)
            {
                currentPlanet.GenerateBackground();
                questComplete = false;
                currentPlanet.oxygenBar.value = currentPlanet.oxygenCurrentValue;
            }
        }

        Quest planetQuest = currentPlanet.planetQuest;
 
        int[] treesForQuest = new int[planetQuest.questLines.Length]; // trees variety

        for (int currentLine = 0; currentLine < treesForQuest.Length; ++currentLine) // cycle each line of the quest
        {
            Transform linePanel = currentQuestContainer.transform.GetChild(currentLine).transform.GetChild(0).transform; // the transform of the SEGMENT CONTAINER in the LINE PANEL of the QUEST CONTAINER

            Quest.QuestLine questLine = planetQuest.questLines[currentLine]; // the line of the planet quest i am updating

            int usefulTrees = 0;

            foreach (TreeDisplayer tree in currentPlanet.fruitTreesList) // cycle each tree on the planet
            {
                if (questLine.treeType == tree.treeType)
                {
                    ++treesForQuest[currentLine]; 
                    ++usefulTrees;                //at the end of the cycle, i will have the number of useful trees for the quest line
                    ++totalQuestTrees;              //every GUIUpdate calculates how many valid trees we have in the whole quest
                }
            }

            // image update
            int totalIcons = planetQuest.questLines[currentLine].treeQuantity;
            for (int iconToColor = 0; iconToColor < totalIcons; ++iconToColor)
            {
                if (linePanel.GetChild(iconToColor) != null)
                {
                    Transform currentIcon = linePanel.GetChild(iconToColor);
                    if (usefulTrees > 0) // there is a useful tree that can update the quest line
                    {
                        --usefulTrees;
                        currentIcon.GetComponent<Image>().sprite = planetQuest.questLines[currentLine].treeType.coloredSegment; // sprite is filled with the fill bar of the tree

                        if (AudioManager.instance != null)
                            AudioManager.instance.PlaySingleSound("TaskCompleted");

                    }
                    else // all the other segments of the quest are empty
                    {
                        currentIcon.GetComponent<Image>().sprite = planetQuest.questLines[currentLine].treeType.emptySegment; // sprite is filled with the empty bar image
                    }
                }

            }

            currentPlanet.UpdateOxygenBar();
        }

        planetBlend = currentPlanet.deadPlanetRend;

       
        #region QUEST COMPLETE CHECK

        //checks if we have all the trees we need
        for (int currentLine = 0; currentLine < treesForQuest.Length; ++currentLine)
        {
            if (treesForQuest[currentLine] >= planetQuest.questLines[currentLine].treeQuantity)
            {
                questComplete = true;

                if (AudioManager.instance != null)
                    AudioManager.instance.PlaySingleSound("AllQuestsCompleted");
            }
            else
            {
                questComplete = false;
                break;
            }
        }
           
        currentPlanet.SetQuestStatus(questComplete);

        #endregion
    }

    #endregion


    // COUNT ALIVE PLANETS - for the saved planet icon
    public void CountAlivePlanets()
    {
        alivePlanetNumber = 0;

        foreach (PlanetManager planet in galaxyCluster.planets)
        {
            if (planet.currentState == PlanetManager.PlanetState.Alive)
            {
                ++alivePlanetNumber; // the number of planets that are currently alive
            }
        }

        foreach (Text iconText in cleanedPlanetsTexts)
        {
            iconText.text = alivePlanetNumber.ToString() + " / " + planetNumber.ToString();
        }

        // checks the win condition
        if (galaxyCluster.planets[galaxyCluster.planets.Length - 1].currentState == PlanetManager.PlanetState.Alive)
        {
            sceneManager.SceneChange(2);
        }

    }

    #region BUTTONS IMAGES
    public void SpriteChange()
    {
        if (player.isMovingRight)
        {
            runBtn.GetComponent<Image>().sprite = runBtnSpriteRunning;
        }
        else if (player.isPlanting)
        {
            runBtn.GetComponent<Image>().sprite = runBtnSpriteDefault;
        }
        else
        {
            runBtn.GetComponent<Image>().sprite = runBtnSpriteDefault;
        }
        

        if (player.nearShip)
        {
            teleportBtn.GetComponent<Image>().sprite = teleportBtnSpriteSpace;
        }
        else
        {
            teleportBtn.GetComponent<Image>().sprite = teleportBtnSpriteDefault;
        }
        

        if(Time.timeScale == 0)
        {
            foreach(GameObject button in pauseBtns)
            {
                button.GetComponent<Image>().sprite = pauseBtnSpritePaused;
            }
        }

        else if (Time.timeScale != 0)
        {
            foreach (GameObject button in pauseBtns)
            {
                button.GetComponent<Image>().sprite = pauseBtnSpriteDefault;
            }
        }
        
    }
    #endregion

    // UPDATE - check on buttons
    private void Update()
    {
        float currentAlpha = Mathf.SmoothDamp(planetBlend.color.a, 1f - (totalQuestTrees / currentPlanet.questTotalFruits), ref dampVelocity, 1f);

        planetBlend.color = new Color(planetBlend.color.r, planetBlend.color.g, planetBlend.color.b, currentAlpha);

        if (player.isPlanting)
        {
            SpriteChange();
        }

        if(player.nearShip)
        {
            SpriteChange();
        }
        else
        {
            SpriteChange();
        }
    }

    // FILL BAR - fill the quest segment
    IEnumerator FillBar(Transform childIcon)
    {
        childIcon.GetComponent<BarBehaviour>().isFilling = true;
       
        while (childIcon.GetComponent<Image>().fillAmount < 1)
        {
            childIcon.GetComponent<Image>().fillAmount += fillspeed * Time.deltaTime;
            yield return new WaitForSeconds(delay);
        }
        childIcon.GetComponent<BarBehaviour>().isFilling = false;
        childIcon.GetComponent<BarBehaviour>().filled = true;       
    }
}
