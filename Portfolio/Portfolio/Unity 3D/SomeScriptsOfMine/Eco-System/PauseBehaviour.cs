﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseBehaviour : MonoBehaviour
{
    public Animator playerAnimator;

    public GameObject pauseMenu;

    [HideInInspector]
    public bool isPaused = false;

    public void Pause()
    {
        if (!isPaused)
        {
            Time.timeScale = 0;
            isPaused = true;
            playerAnimator.enabled = false;
            pauseMenu.SetActive(true);
            GameManager.instance.gamePaused = true;
        }

        else if (isPaused)
        {
            Time.timeScale = 1;
            isPaused = false;
            playerAnimator.enabled = true;
            pauseMenu.SetActive(false);
            GameManager.instance.gamePaused = false;
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Continue()
    {
        Pause();
    }
}
