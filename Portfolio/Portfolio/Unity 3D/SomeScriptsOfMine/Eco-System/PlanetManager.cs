﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlanetManager : MonoBehaviour
{
    [Tooltip("Clock Instance")]
    public GameObject clock;

    [Tooltip("The prefab of the screen that shows the day and night")]
    public GameObject dayNightPrefab;

    public Quest planetQuest;
    
    // PLANET SETTINGS
    [Tooltip("List of the trees plantable on this planet")]
    public List<TreeData> plantableTree;

    [Header("PLANET SETTINGS"), Tooltip("How fast the player moves around this planet")]
    public float movementSpeed;

    [Tooltip("Reference to the planet body in this planet hierarchy")]
    public GameObject planetBody;

    [Tooltip("Reference to the DAY TRIGGER prefab")]
    public GameObject dayTrigger;

    [Tooltip("Reference to the NIGHT TRIGGER prefab")]
    public GameObject nightTrigger;

    [Tooltip("Reference to the DEAD PLANET SPRITE RENDERER")]
    public SpriteRenderer deadPlanetRend;

    [Tooltip("How many trees this planet needs to be heathy")]
    public int requiredTrees = 10;

    [Range(0.01f, 99.99f), Tooltip("The height in the planet surfice of the player")]
    public float playerHeight = 0.01f;

    [Range(0.01f, 99.99f), Tooltip("The height in the planet surfice of the ship - generally, it should be higher that the player")]
    public float shipHeight = 0.01f;

    [Tooltip("How far away is the camera from the planet: default 1.5")]
    public float cameraDistance;

    [Header("TO UNLOCK THIS PLANET"), Tooltip("The list of planets that the player must revive to unlock the navigation to this planet - N.B. List is auto-filled with sub-required planets")]
    public List<PlanetManager> blockingPlanetsToClear;

    [Tooltip("When unlocking this planet, theese smoke areas will disappear")]
    public GameObject[] smokeAreas;

    [Tooltip("How much time (seconds) is required for the smoke to dissolve")]
    public float smokeDissolveTime;

    [Space(10), Header("SPACE UI REFERENCES"), Tooltip("The reference to the SPACE UI gameobject in this planet hierarchy")]
    public GameObject spaceUi;

    [Tooltip("The reference to the SPACE UI SPAWNER gameobject in this planet hierarchy")]
    public GameObject spaceUiSpawner;

    [Tooltip("The reference to the SPACE UI QUEST CONTAINER in the CANVAS hierarchy")]
    public GameObject spaceUiQuestContainer;

    [Tooltip("The reference to the SPACE UI QUARANTINE gameobject in this planet hierarchy")]
    public GameObject spaceUiQuarantine;

    [Tooltip("Reference to the CANVAS UI MANAGER script component to show the current planet quest by hovering with the mouse on the planet")]
    public UIManager canvasUiManager;

    [Space(10), SerializeField, Tooltip("When the player clicks on this planet to begin the landing")]
    public UnityEvent BeginLanding;

    [Space(5), SerializeField, Tooltip("When the state of the planet health state changes")]
    public UnityEvent PlanetStateChanged;

    [HideInInspector]
    public bool questCompleted = false; // QUEST completion flag

    [Tooltip("TEST: if TRUE - all the planets in the \"blocking planets to clen \" must be cleaned to unlock this planet - if FALSE - only one of the planets of the list is enough to unlock the planet")]
    public bool TEST_AllPlanetsCleaned;

    [Space(10), Header("OXYGEN"), Tooltip("Reference to the oxygen bar")]
    public Slider oxygenBar;

    [Tooltip("Decrease of the oxygen bar per frame")]
    public float oxygenDecreasPerFrame;

    [Range(0.0001f, 1f), Tooltip("Start level of the oxygen")]
    public float oxygenStartValue;

    [Range(0.0001f, 1f), Tooltip("Current level of the oxygen"), HideInInspector]
    public float oxygenCurrentValue;

    // STATUS
    [HideInInspector]
    public float planetHour; // (real seconds) 24 hours after which the time is reset to 0:00

    [HideInInspector]
    public PlayerManager player; // when the player is on this planet, this variable is not null

    [HideInInspector]
    public List<TreeDisplayer> treeList; // the list of all the trees on the planet

    [HideInInspector]
    public List<TreeDisplayer> fruitTreesList; //list of the trees ready to drop fruit

    [HideInInspector]
    public enum PlanetState { Alive, Dying } // planet possible states

    [HideInInspector]
    public enum PlanetStage { Day, Night } // planet light condition

    [HideInInspector]
    public PlanetState currentState = PlanetState.Dying; // state of the planet

    [HideInInspector]
    public PlanetStage dailyStage = PlanetStage.Day;

    // MISC
    [HideInInspector]
    public Vector3 cameraPlanetPosition;

    [HideInInspector]
    public GameObject landingSpot;

    [HideInInspector]
    public Vector3 playerLandingSpot;

    [HideInInspector]
    public Vector3 shipLandingSpot;

    [HideInInspector]
    public float questTotalFruits;

    [HideInInspector]
    public bool isClickable;

    [HideInInspector]
    public bool backgroundGenerated = false;

    [HideInInspector]
    public bool isResettingOxygen;

    [HideInInspector]
    public bool isUnlocked; // condition determined by the planets that are present in the "blocking planets to clear" list 

    public float currentAngle;

    List<QuestStatus> myQuestStatus = new List<QuestStatus>();

    GameObject currentUi;

    bool smokeDissolved;

    bool smokeDissolveMemory;
    
    float planetRadius;

    public struct QuestStatus
    {
        public TreeData questTree;
        public int requiredValue;
        public int myValue;      
    }

    

    

    // PLANET INITIAL SETUP
    private void Awake()
    {
        planetRadius = transform.localScale.x / 2;
        cameraPlanetPosition = (transform.position + Vector3.back * (planetRadius * 2 + cameraDistance));

        // day & night triggers creation
        GameObject newDayTrigger = Instantiate(dayTrigger, planetBody.transform); // day trigger
        newDayTrigger.transform.localPosition = new Vector3(0f, 0.5f, 0f);

        GameObject newNightTrigger = Instantiate(nightTrigger, planetBody.transform); // night trigger
        newNightTrigger.transform.localPosition = new Vector3(0f, -0.5f, 0f);

        // ship position calculation
        float newShipHeight = (planetRadius * shipHeight) / 100;
        shipLandingSpot = transform.position + Vector3.up * newShipHeight + Vector3.back * 0.05f;

        // player position calculation
        float newPlayerHeight = (planetRadius * playerHeight) / 100;
        playerLandingSpot = transform.position + Vector3.up * newPlayerHeight + Vector3.back * 0.15f;

        // total quest fruits calcualtion
        for (int i = 0; i < planetQuest.questLines.Length; ++i)
        {
            questTotalFruits += planetQuest.questLines[i].treeQuantity;
        }
    }

    

    // FIRST UNLOCK CHECK
    private void Start()
    {
        currentAngle = 0;
        oxygenCurrentValue = oxygenStartValue;
        oxygenBar.value = oxygenCurrentValue;
        

        if (CheckIfIsUnlocked())
        {
            isUnlocked = true;
        }
        else
        {
            isUnlocked = false;
        }
    }

    private void Update()
    {
        if (GameManager.instance.player.currentPlanet == this && !isResettingOxygen)
        {
            if (oxygenBar.value == 0)
            {

                GameManager.instance.player.IsDying();  //sets the bool for the animation
                GameManager.instance.player.MoveButton();  // stops the player from moving
                isResettingOxygen = true;
                GameManager.instance.questFailed = true;
            }
        }       
    }

    // MOVE RIGHT
    public void MoveRight()
    {
        planetBody.transform.RotateAround(transform.position, Vector3.forward, movementSpeed * Time.deltaTime);
        PassTime(1);
        clock.transform.RotateAround(clock.transform.position, Vector3.forward, movementSpeed * Time.deltaTime);
        PassTime(1);

        currentAngle = planetBody.transform.eulerAngles.z;
    }

    // PASS TIME
    public void PassTime(float timeScaler)
    {
        planetHour += Time.deltaTime * timeScaler;
        if (!questCompleted && !GameManager.instance.gamePaused)
        {
            oxygenBar.value -= oxygenDecreasPerFrame*Time.deltaTime;
            oxygenCurrentValue = oxygenBar.value;
        }
        if(questCompleted)
        {
            oxygenCurrentValue = 1f;
            oxygenBar.value = 1f;
        }
        if (planetHour >= 24f)
        {
            planetHour = 0f;
        }

        foreach (TreeDisplayer tree in treeList)
        {
            
            tree.PassTime(Time.deltaTime * timeScaler);
            
        }
    }

    //CAN BE CLICKED
    public void CanBeClicked()
    {
        isClickable = true;
    }

    //CANNOT BE CLICKED
    public void CannotBeClicked()
    {
        isClickable = false;
    }

    // PLANET STATUS CHECK
    public void SetQuestStatus(bool questValue)
    {
        if (questValue == false) // quest not completed yet
        {
            questCompleted = false;
            if(currentState != PlanetState.Dying)
            {
                currentState = PlanetState.Dying;

                PlanetStateChanged.Invoke();
                AudioManager.instance.PlaySingleSound("PlanetDead");
            }
            

            if (!smokeDissolved)
            {
                smokeDissolveMemory = false;
            }
        }
        else if (questValue == true) // Quest is now complete
        {
            questCompleted = true;

            if(currentState != PlanetState.Alive)
            {
                currentState = PlanetState.Alive;

                if (GetComponent<DialogueTrigger>() != null && player != null) // spawns the "planet clean" dialogue
                {
                    player.SpawnDialogue(GetComponent<DialogueTrigger>());
                    Destroy(GetComponent<DialogueTrigger>()); // destroys the component in order to not spawn the dialogue ever again
                }


                PlanetStateChanged.Invoke();
                AudioManager.instance.PlaySingleSound("PlanetHealed");
            }

            if (!smokeDissolved)
            {
                smokeDissolveMemory = true;
            }
        }
    }

    // CHECKS IF IT IS NAVIGABLE - recursive
    public bool CheckIfIsUnlocked()
    {
        if (blockingPlanetsToClear[0] != null)
        {
            if (TEST_AllPlanetsCleaned) // all planets must be cleaned to unlock this planet
            {
                foreach (PlanetManager planet in blockingPlanetsToClear)
                {
                    if (planet.currentState != PlanetState.Alive)
                    {
                        return false;
                    }
                }
                return true;
            }
            else // only one of the blocking planets is enough to unlock this planet
            {
                foreach (PlanetManager planet in blockingPlanetsToClear)
                {
                    if (planet.currentState == PlanetState.Alive)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        else // list contains nothing -> planet is unlocked
        {
            return true;
        }
    }

    // LANDING - clicking on a planet will hide its space UI and will invoke the landing protocol
    public void OnMouseDown()
    {
        if (isClickable && isUnlocked)
        {
            if (currentUi != null)
            {
                currentUi.SetActive(false);
            }

            spaceUiQuestContainer.gameObject.SetActive(false);

            BeginLanding.Invoke();
        }
    }

    #region SPACE UI MOUSE HOVERING

    // SPACE UI - hovering on a planet will show its space UI with the current status of its quest
    public void OnMouseEnter()
    {
        if (isClickable)
        {
            if (isUnlocked)
            {
          
                currentUi = spaceUi;
                spaceUiQuestContainer.transform.position = Camera.main.WorldToScreenPoint(spaceUiSpawner.transform.position);

                spaceUiQuestContainer.gameObject.SetActive(true);

                canvasUiManager.InitializeQuest(gameObject); // commands to create the quest into the spaceUIQuestContainer
               
            }
            else
            {
                currentUi = spaceUiQuarantine;
            }

            currentUi.SetActive(true);
        }
    }

    public void OnMouseExit()
    {
        if (isClickable)
        {
            if (currentUi != null)
            {
                currentUi.SetActive(false);
            }

            spaceUiQuestContainer.gameObject.SetActive(false);
        }
    }
    #endregion


    // START UNSMOKE - command from the player to the planet when the 
    public void StartUnsmoke()
    {

        if (smokeDissolveMemory)
        {
            StartCoroutine(SmokeDissolveDelay());
        }
    }

    // SMOKE DISSOLVE DELAY
    public IEnumerator SmokeDissolveDelay()
    {
        yield return new WaitForSeconds(smokeDissolveTime);

        foreach (GameObject cloud in smokeAreas) // smode is dissolved
        {
            cloud.GetComponent<ParticleSystem>().Stop();
        }

        smokeDissolved = true;
    }

    // QUEST STATUS UPDATE
    public void QuestStatusUpdate(int[] treesWeHave)
    {
        for (int i = 0; i < planetQuest.questLines.Length; i++)
        {
            QuestStatus inspectedTree = new QuestStatus();

            inspectedTree.questTree = planetQuest.questLines[i].treeType;
            inspectedTree.requiredValue = planetQuest.questLines[i].treeQuantity;
            inspectedTree.myValue = treesWeHave[i];
            myQuestStatus.Add(inspectedTree);
        }
    }

    //reduce the oxygen 
    public void UpdateOxygenBar()
    {
        if (GameManager.instance.player.isInSpace)
        {
            oxygenBar.value = oxygenCurrentValue;
        }
    }

    // when we run out of oxygen resets the planet rotation
    public void ResetPlanetPosition()
    {        
        foreach (TreeDisplayer tree in treeList)
        {
            tree.hasToDie = true;
        }
        transform.GetChild(0).transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        currentAngle = 0;
        clock.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }

    // generates the background of the planet that shows day and night
    public void GenerateBackground()
    {        
        backgroundGenerated = true;
        Instantiate(dayNightPrefab, transform);
    }
}
