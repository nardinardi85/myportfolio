﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

  //  [HideInInspector]
    public static GameManager instance = null;

    GameObject self;

    // GENERAL REFERENCES
    [Space(10), Header("GENERAL REFERENCES"), Tooltip("Reference to the PLAYER object")]
    public PlayerManager player;

    [Tooltip("The black panel that fades when the quets if failed")]
    public MissionFailed questFailedPanel;

    public bool gamePaused;

    public bool questFailed;


    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (questFailed)
        {
           
           // player.MoveButton();            
            questFailedPanel.currentPlanet = player.currentPlanet;
            questFailedPanel.gameObject.SetActive(true);
            questFailed = false;
        }
    }
}
