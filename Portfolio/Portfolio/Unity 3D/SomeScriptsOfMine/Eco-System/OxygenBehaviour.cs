﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OxygenBehaviour : MonoBehaviour
{

    [Tooltip("The speed of the oxygen")]
    public float upSpeed;

    [Tooltip("How much oxygen to add")]
    public float oxygenToAdd;

    [Tooltip("After how long it destroys itself")]
    public float selfDestroyTime;

    [HideInInspector]
    public PlanetManager currentPlanet;

    [Tooltip("The layer the raycast has to ignore")]
    public LayerMask layerMask;

    new Animator animation;

    void Start()
    {
        StartCoroutine(SelfDestroy());
        animation = GetComponent<Animator>();
    }

    //checks if we click on the oxygen
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
            {
                if (hit.transform.gameObject.tag == "Oxygen")
                {
                    AudioManager.instance.PlaySingleSound("OxygenExplodes");
                    AudioManager.instance.PlaySingleSound("OxygenIncreases");
                    hit.transform.GetComponent<OxygenBehaviour>().animation.SetTrigger("Exploding");               
                }
            }
        }
        transform.position = new Vector3(transform.position.x, transform.position.y + upSpeed*Time.deltaTime, transform.position.z);
    }

    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(selfDestroyTime);
        Destroy(gameObject);         
    }

    // called at the end of the animation
    public void InstantDestroy()
    {
        StopAllCoroutines();
        currentPlanet.oxygenBar.value += oxygenToAdd;
        currentPlanet.oxygenCurrentValue = currentPlanet.oxygenBar.value;
        Destroy(gameObject);
    }
}
