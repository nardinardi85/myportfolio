﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{

    public void SceneChange(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    // general method to open/close tab/panels
    public void PanelManager(GameObject panel)
    {
        panel.SetActive(!panel.activeSelf);
    }
}



