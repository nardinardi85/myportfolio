﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour {
    [SerializeField]float maxTime;
    float timer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(timer>=maxTime)
        {
            SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
        }
        timer += Time.deltaTime;
	}
}
