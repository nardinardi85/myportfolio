﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMessages
{
    public static string Message(string key)
    {
        switch(key)
        {
            case "draw1a": return "This is the battlefield, where you\'ll face your opponents.";
            case "draw1b": return "As you can see you have six familiar cards right now.";
            case "deploy1a": return "Now you must position your cards onto the battlefield";
            case "deploy1b": return "Drag the highlighted card from your hand to the highlighted position on the battlefield, you can also tap on a card and then on the desired position.";
            case "deploy1c": return "Well done, now repeat what you just did for your five remaining cards.";
            case "pick1a": return "Now that your formation is set you must tap into one of your familiars power in order to cast a spell sphere.";
            case "pick1b": return "You can achieve that by either tapping on your familiar or his corresponding card, the choice is UP TO YOU!";
            case "throw1a": return "You can adjust the ball\'s horizontal position by sliding it.";
            case "throw1b": return "Swipe upward to cast the sphere, but be careful, fellow wizards have BROKEN their ARMS while doing this.";
            case "throw1c": return "You\'re pretty good!";
            case "throw1d": return "Try again! Quick, don\'t keep me waiting!";
            case "throw2a": return "If you want to throw a curve sphere add a curve to your swipe.";
            case "throw2b": return "Nicely done!";
            case "throw2c": return "You\'re gonna lose if you can\'t throw curve spheres, c\'mon try again!";
            case "pick3a": return "As you may have noticed your familiars have different elements.";
            case "pick3b": return "These elements will be absorbed by your sphere so hitting a weak familiar will make you earn more points, by doing the opposite you\'ll earn fewer points";
            case "pick3c": return "If you hit a weak familiar you\'ll also create a malus on the opponent\'s battlefield.";
            case "pick3d": return "Positioning a familiar on a malus spot can have different outcomes.";
            case "pick3e": return "If the familiar is weaker against the malus\' element the opponent won\'t be able to use that familiar\'s special ability, nor use his element";
            //case "pick3f": return "If the familiar has the same element of the malus the opponent won\'t be able to use that familiar\'s special ability, but he will be able to use his element";
            //case "pick3g": return "If the familiar is stronger against that malus\' element, the malus will be removed.";
            case "pick4a": return "An Elemental interaction will happen even when 2 spheres collide, useless to say that the weaker sphere will be destroyed.";
            case "pick4b": return "An interesting outcome can be seen when 2 spheres of the same element collide, they will bounce back, so avoid that collision at any cost!";
            case "next": return "Confirm your choice";
        }
        return string.Empty;
    }
}
