﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitButton : MonoBehaviour// InputController
{
    [SerializeField] GameObject uiLevelLoader;
    public void OnClick()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        GameController.Instance.ResumeGame();
        transform.parent.parent.gameObject.SetActive(false);
        LevelLoader.sceneToLoad = "MainMenuScene";
        uiLevelLoader.GetComponent<LevelLoader>().LoadLevel();
    }
    /*
    protected override void OnSwipe(Gesture gesture)
    {
        if(gesture.Ended)
        {
            GameObject startObject = UIMain.Instance.GetUIObject(Gesture.ToScreenCoords(gesture.start));
            GameObject endObject = UIMain.Instance.GetUIObject(Gesture.ToScreenCoords(gesture.start));
            if (startObject == gameObject && endObject == gameObject)
            {

            }
        }
    }
    protected override void OnTap(Gesture gesture)
    {
        GameObject tapObject = UIMain.Instance.GetUIObject(Gesture.ToScreenCoords(gesture.start));
        if (tapObject == gameObject)
        {
            GameController.Instance.ResumeGame();
            transform.parent.parent.gameObject.SetActive(false);
            LevelLoader.sceneToLoad = "MainMenuScene";
            uiLevelLoader.GetComponent<LevelLoader>().LoadLevel();
        }
    }
    protected override void OnPressure(Gesture gesture)
    {

    }*/
}