﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour
{
    [SerializeField] GameObject menuPopUp;
    public void OnClick()
    {
        if (Input.touchCount <= 1)
        {
            SoundController.Instance.PlaySound("ButtonSelection");
            GameController.Instance.PauseGame();
            menuPopUp.gameObject.SetActive(true);
        }
    }
}
