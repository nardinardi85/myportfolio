﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CampaignMenuController : MonoBehaviour
{
    public static CampaignMenuController instance;

    public Text textLv;
    public TextMeshProUGUI textBattle;
    public string stage1Title;
    public string stage1Desctext;
    public string tutorialTitle;
    public string tutorialDescText;
    public TextMeshProUGUI textDescription;
    public GameObject panel;
    public GameObject backButton;
    [SerializeField] [Range(0, 100)] int level;
    //[SerializeField] GameObject dummy;
    //[SerializeField] GameObject description;
    [SerializeField] GameObject levelLoader;

    public static CampaignMenuController Instance 
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
    }


    public void ClickTutorialBtn()
    {
        MainMenuController.startTutorial = true;
        LevelLoader.sceneToLoad = "GameScene";
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("TUTORIAL: BOTTONE PREMUTO");
        panel.SetActive(true);
        backButton.SetActive(false);
        textBattle.text = tutorialTitle;
        textDescription.text = tutorialDescText.ToUpper();
        //SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void ClickStage1Btn()
    {
        MainMenuController.startTutorial = false;
        LevelLoader.sceneToLoad = "GameScene";
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("STAGE 1: BOTTONE PREMUTO");
        panel.SetActive(true);
        backButton.SetActive(false);
        textBattle.text = stage1Title;
        textDescription.text = stage1Desctext.ToUpper();
        
    }

    public void ClickPlayBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        levelLoader.GetComponent<LevelLoader>().LoadLevel();
    }

    public void ClickMinionsBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("MINIONS: BOTTONE PREMUTO");
    }

    //Campaign Menu
    public void ClickBackBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
    }
    public void ClickBackPanelBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");

        panel.SetActive(false);
        backButton.SetActive(true);
    }
    public void ClickBackPanelNoSoundBtn()
    {
        panel.SetActive(false);
        backButton.SetActive(true);
    }

    public void ClickStandardCurrencyBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("STANDARD CURRENCY: BOTTONE PREMUTO");
    }

    public void ClickPremiumCurrencyBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("PREMIUM CURRENCY: BOTTONE PREMUTO");
    }

    private void Start()
    {
        panel.SetActive(false);
        backButton.SetActive(true);
        SoundController.Instance.PlaySoundTrack("MenuSoundtrack");
        //textLv.text = "LV." + level.ToString();
    }
}
