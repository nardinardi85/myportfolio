﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    //[SerializeField] int numTracks;


    private static SoundController instance = null;
    [SerializeField]
    public List<AudioClip> trackList = new List<AudioClip>();
    [SerializeField]
    public List<string> trackName = new List<string>();
    AudioSource[] audioSources;
    [SerializeField]
    public List<float> volume = new List<float>();
    public static SoundController Instance
    {
        get
        {
            return instance;
        }
    }

    public float sfxVolume
    {
        get
        {
            return audioSources[0].volume;
        }
    }

    public float musicVolume
    {
        get
        {
            return audioSources[1].volume;
        }
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        audioSources = GetComponents<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }
    public void PlaySingleSound(string track)
    {
        int i = 0;

        foreach (string name in trackName)
        {

            if (track == name)
            {
                if (audioSources[0].isPlaying)
                {
                    audioSources[0].Stop();
                    audioSources[0].clip = trackList[i];
                    audioSources[0].volume = volume[i];
                    audioSources[0].Play();
                }
                else
                {
                    audioSources[0].clip = trackList[i];
                    audioSources[0].volume = volume[i];
                    audioSources[0].Play();
                }
               
            }
            i++;
        }
        
    }

    public void PlaySound(string track)
    {

        int i = 0;

        foreach (string name in trackName)
        {
            if (track == name)
            {
                audioSources[0].PlayOneShot(trackList[i], volume[i]);
            }
            i++;
        }

    }
    public void PlaySoundTrack(string track)
    {

        int i = 0;

        foreach (string name in trackName)
        {
            if (track == name)
            {
                if (audioSources[1].isPlaying)
                {
                    if (audioSources[1].clip != trackList[i])
                    {
                        audioSources[1].Stop();
                        audioSources[1].volume = volume[i];
                        audioSources[1].clip = trackList[i];
                        audioSources[1].Play();
                        Debug.Log("Started new soundtrack");
                    }
                }
                else
                {
                    audioSources[1].clip = trackList[i];
                    audioSources[1].volume = volume[i];
                    audioSources[1].Play();
                    Debug.Log("Started soundtrack");
                }
                break;
            }
            i++;
        }

    }

    public void StopSoundtrack()
    {
        if (audioSources[1].isPlaying)
        {
            audioSources[1].Stop();
        }
    }

    public void SetSFXVolume()
    {
        if (audioSources[0].volume > 0f)
            audioSources[0].volume = 0f;
        else 
            audioSources[0].volume = GetTrackVolume(audioSources[0].clip);
    }

    public void SetMusicVolume()
    {
        if (audioSources[1].volume > 0f)
            audioSources[1].volume = 0f;
        else
            audioSources[1].volume = GetTrackVolume(audioSources[1].clip);
    }

    public float GetTrackVolume(AudioClip track)
    {
        int i = 0;
        foreach (AudioClip a in trackList)
        {
            if (track == trackList[i])
            {
                return volume[i];
            }
            i++;
        }
        return 1f;
    }
    // Use this for initialization
    void Start ()
    {
    }

}
