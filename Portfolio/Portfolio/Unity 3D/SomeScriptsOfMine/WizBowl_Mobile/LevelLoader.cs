﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelLoader : MonoBehaviour
{

    public GameObject loadingScreen;
    public Slider slider;
    public static string sceneToLoad;

    private void Awake()
    {
        sceneToLoad = SceneManager.GetSceneByBuildIndex(0).name;
    }

    public void LoadLevel()
    {
        loadingScreen.SetActive(true);
        slider.value = 0f;
        StartCoroutine(LoadLevelAsync(sceneToLoad));
    }

    IEnumerator LoadLevelAsync(string sceneName)
    {

        AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
        while (!op.isDone)
        {
            float progress = Mathf.Clamp01(op.progress);
            if (slider.value < 0.9f)
            {
                slider.value = progress;
            }
            else
            {
                slider.value = Mathf.Lerp(slider.value, 1f, 0.4f);
            }
            yield return null;
        }
        slider.value = 1f;

    }
}
