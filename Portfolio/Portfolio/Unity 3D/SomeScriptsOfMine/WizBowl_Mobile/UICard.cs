﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICard : MonoBehaviour {

    public Card card;
    public bool locked = false;
    public bool placed = false;
    public void SetCard(Card card)
    {
        this.card = card;
        GetComponent<Image>().sprite = card.backgroundImage;
        transform.GetChild(0).GetComponent<Image>().sprite = card.cardImage;

    }
}
