﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButton : MonoBehaviour
{
    public void OnClick()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        GameController.Instance.ResumeGame();
        transform.parent.parent.gameObject.SetActive(false);
    }
}
