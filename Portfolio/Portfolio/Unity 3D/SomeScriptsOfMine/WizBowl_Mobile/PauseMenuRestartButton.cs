﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuRestartButton : MonoBehaviour
{
    [SerializeField] GameObject uiLevelLoader;
    public void OnClick()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        GameController.Instance.ResumeGame();
        LevelLoader.sceneToLoad = "GameScene";
        uiLevelLoader.GetComponent<LevelLoader>().LoadLevel();
    }
}
