﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public float scoresSpeed = 0.2f;
    public bool isPlayer;
    public int scorePoints = 0;
    // Use this for initialization
    private void Awake()
    {
        isPlayer = true;
    }
    // Update is called once per frame
    void Update ()
    {
        if (Time.timeScale == 0f) return;        
        Vector3 destination = (isPlayer ? GameController.Instance.playerScorePanel.transform.position : GameController.Instance.enemyScorePanel.transform.position);
        transform.position = Vector3.Lerp(transform.position, destination, scoresSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position, destination) < 100f)
        {
            GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.clear, scoresSpeed * Time.deltaTime); 
            if (Vector3.Distance(transform.position, destination) < 10f)
            {
                GivePoints();
            }
        }
	}
    void GivePoints()
    {
        SoundController.Instance.PlaySound("PointGain");
        if (isPlayer)
        {
            int benny = System.Convert.ToInt32(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
            GameController.Instance.AddPlayerPoints(benny);
        }
        else
        {
            int benny = System.Convert.ToInt32(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
            GameController.Instance.AddEnemyPoints(benny);
        }
        Destroy(gameObject);
    }
    public void SetScorePoints(int points)
    {
        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = points.ToString();
    }
}
