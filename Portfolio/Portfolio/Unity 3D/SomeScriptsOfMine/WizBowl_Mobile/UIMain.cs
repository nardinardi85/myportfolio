﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using TMPro;

public class UIMain : MonoBehaviour
{

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;
    [SerializeField] GameObject mainMessageObject;


    static UIMain instance;

    public static UIMain Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = GetComponent<EventSystem>();
    }
    void Start()
    {
    }

    void Update()
    {

    }

    public void SetDebugLog(string str)
    {
        //transform.Find("DebugInfo").GetComponent<TextMeshProUGUI>().text = str.ToUpper();
        mainMessageObject.GetComponent<TextMeshProUGUI>().text = str.ToUpper();
        if(str.Length <= 0)
        {
            mainMessageObject.transform.parent.GetComponent<Image>().color = Color.clear;
        }
        else
        {
            mainMessageObject.transform.parent.GetComponent<Image>().color = Color.white;
        }
    }

    public GameObject GetUIObject(Vector2 position, string layer = "UI")
    {
        //Set up the new Pointer Event
        m_PointerEventData = new PointerEventData(m_EventSystem);

        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position =  position;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);

        if(results.Count > 0)
            return results[0].gameObject;
        return null;
    }

    public void DoSomething()
    {

    }
}