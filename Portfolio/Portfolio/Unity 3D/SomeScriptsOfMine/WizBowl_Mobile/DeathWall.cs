﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : MonoBehaviour
{
    public LayerMask ballLayer;
    public LayerMask pinLayer;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == Mathf.RoundToInt(Mathf.Log(ballLayer.value, 2)))
        {
            GameController.FakeDestroy(collision.gameObject);
        }
        if (collision.gameObject.layer == Mathf.RoundToInt(Mathf.Log(pinLayer.value, 2)))
        {
            GameController.FakeDestroy(collision.gameObject);
        }
    }
}
