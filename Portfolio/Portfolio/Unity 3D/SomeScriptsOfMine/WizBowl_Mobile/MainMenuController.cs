﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController instance;
    public static bool startTutorial = false;
    public GameObject gizmo;
    public GameObject questsPanel;
    public GameObject creditsPanel;
    public Text textLv;
    public Text textMentor;
    public Text textSocial;
    public Text textEvents;
    public ScrollRect scrollRect;
    public TextMeshProUGUI musicText;
    public TextMeshProUGUI sfxText;
    [SerializeField] [Range(0, 100)] int level;

    public static MainMenuController Instance 
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
        startTutorial = false;
    }


    public void ClickShopBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("SHOP: BOTTONE PREMUTO");
	}

    public void ClickMinionsBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("MINIONS: BOTTONE PREMUTO");
    }

    public void ClickMatchBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("MATCH: BOTTONE PREMUTO");
    }

    public void ClickSocialhBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("SOCIAL: BOTTONE PREMUTO");
    }

    public void ClickEventsBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("EVENTS: BOTTONE PREMUTO");
    }

    public void ClickChestBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("CHEST: BOTTONE PREMUTO");
    }

    public void ClickCampaignBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("CAMPAIGN: BOTTONE PREMUTO");
        SceneManager.LoadScene("CampaignScene", LoadSceneMode.Single);
    }

    public void ClickOnlineBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("ONLINE: BOTTONE PREMUTO");
        SceneManager.LoadScene("OnlineMenuScene", LoadSceneMode.Single);
    }

    public void ClickOptionsGizmoBtn()
    {
        Debug.Log("OPTIONS: BOTTONE PREMUTO");
        SoundController.Instance.PlaySound("ButtonSelection");
        scrollRect.enabled = false;
        gizmo.SetActive(true);
    }

    public void ClickCreditsBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        scrollRect.enabled = false;
        creditsPanel.SetActive(true);
    }
    public void ClickStandardCurrencyBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
    }

    public void ClickPremiumCurrencyBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("PREMIUM CURRENCY: BOTTONE PREMUTO");
    }

    public void ClickStatsBtn()
    {
        //SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("STATS: BOTTONE PREMUTO");
        SceneManager.LoadScene("ProfileManagementScene", LoadSceneMode.Single);
    }

    public void ClickQuestBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("QUEST: BOTTONE PREMUTO");
        scrollRect.enabled = false;
        questsPanel.SetActive(true);
    }


    public void ClickBtnBase()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("BOTTONE PREMUTO");
    }

    public void ClickMusicBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        SoundController.Instance.SetMusicVolume();
        if (SoundController.Instance.musicVolume > 0f)
            musicText.text = "MUSIC ON";
        else
            musicText.text = "MUSIC OFF";
    }
    public void ClickSfxBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        SoundController.Instance.SetSFXVolume();
        if (SoundController.Instance.sfxVolume > 0f)
            sfxText.text = "SFX ON";
        else
            sfxText.text = "SFX OFF";


    }
    //Online Menu
    public void ClickBackBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("BACK: BOTTONE PREMUTO");
        if (SceneManager.GetActiveScene().name == "MainMenuScene")
        {
            gizmo.SetActive(false);
            questsPanel.SetActive(false);
            creditsPanel.SetActive(false);
            scrollRect.enabled = true;
        }
        else
        {
            SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
        }
    }


    public void ClickBackBtnNoSound()
    {
        gizmo.SetActive(false);
        questsPanel.SetActive(false);
        scrollRect.enabled = true;
    }

    public void ClickSingleMatchBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        Debug.Log("SINGLE MATCH: BOTTONE PREMUTO");
        startTutorial = false;
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void ClickComingSoonSocialBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        textSocial.text = "COMING SOON!";
    }

    public void ClickcomingSoonEventsBtn()
    {
        SoundController.Instance.PlaySound("ButtonSelection");
        textEvents.text = "COMING SOON!";
    }

    private void Start()
    {
        SoundController.Instance.PlaySoundTrack("MenuSoundtrack");
        if (musicText != null)
        {
            if (SoundController.Instance.musicVolume > 0f)
                musicText.text = "MUSIC ON";
            else
                musicText.text = "MUSIC OFF";
        }
        if (sfxText != null)
        {
            if (SoundController.Instance.sfxVolume > 0f)
                sfxText.text = "SFX ON";
            else
                sfxText.text = "SFX OFF";
        }
    }


}
