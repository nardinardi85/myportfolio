﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Gesture
{
    public enum GestureType
    {
        Line, Curve, Spline
    }
    // Start and end position of the gesture
    public Vector2 start;
    public Vector2 end;

    // True if the gesture has ended(for TouchPhase.Ended)
    bool ended;
    GestureType gestureType;

    // Gesture movement parameters
    public float time;     // Time for the whole gesture 
    float stationaryTime;
    float length;   // Length of the whole gesture

    float currentSpeed;

    TouchPhase phase;

    List<Vector2> positions;    // All gesture screen positions

    float swipeFactor;

    float abortedGestureAngleFactor;
    
    public void CopyPositions(Gesture from)
    {
        positions = new List<Vector2>();
        foreach(Vector2 v in from.positions)
        {
            positions.Add(v);
        }
    }

    public void Start(Vector2 position)
    {
        position = ToPercentualCoords(position);
        start = position;
        end = position;
        ResetGesture();
        positions = new List<Vector2>();
        positions.Add(start);
        phase = TouchPhase.Began;
    }

    public void Update(Vector2 position, TouchPhase touchPhase)
    {
        position = ToPercentualCoords(position);
        ended = touchPhase == TouchPhase.Ended;
        time += Time.deltaTime;
        phase = touchPhase;
        if (phase == TouchPhase.Stationary)
        {
            currentSpeed = 0f;
            stationaryTime += Time.deltaTime;
        }
        if (phase == TouchPhase.Moved)
        {
            stationaryTime = 0f;
        }
        if (end != position)
        {
            currentSpeed = Vector2.Distance(position, end) / Time.deltaTime;
            length += Vector2.Distance(position, end);
            end = position;
            positions.Add(end);
            foreach (Vector2 p in positions)
            {
                float swipeDistance = Mathf.Abs((end.y - start.y) * p.x - (end.x - start.x) * p.y + end.x * start.y - end.y * start.x) / Mathf.Sqrt(Mathf.Pow(end.y - start.y, 2) + Mathf.Pow(end.x - start.x, 2));
                if (swipeFactor < swipeDistance)
                {
                    swipeFactor = swipeDistance;
                }
            }
        }
        if (ended)
        {
            //DrawLine(positions, Color.red);
            if (positions.Count > 2)
            {
                // Left or right
                abortedGestureAngleFactor = Vector2.SignedAngle(positions[2] - positions[1], positions[1] - positions[0]);
                if (positions.Count > 3)
                {
                    for (int i = 2; i < positions.Count - 1; ++i)
                    {
                        float deltaAngle = Vector2.SignedAngle(positions[i + 1] - positions[i], positions[i] - positions[i - 1]);
                        float deltaStartAngle = Vector2.SignedAngle(positions[i + 1] - positions[i], positions[1] - positions[0]);
                        if (Mathf.Abs(deltaStartAngle) > GestureDetector.curveAngleDeadZone)
                        {
                            gestureType = GestureType.Curve;
                            if (deltaAngle != 0f)
                            {
                                if (Mathf.Sign(abortedGestureAngleFactor) != Mathf.Sign(deltaAngle))
                                {
                                    gestureType = GestureType.Spline;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < positions.Count - 1; ++i)
            {
                // Debug.DrawLine(Camera.main.ScreenToViewportPoint(positions[i]) * 5f, Camera.main.ScreenToViewportPoint(positions[i + 1]) * 5f, Color.red);
            }
        }
    }

    public float Direction
    {
        get
        {
            return -Vector2.SignedAngle(Vector2.up, end - start);
        }
    }

    public float StartDirection
    {
        get
        {
            if (positions.Count > 1)
                return -Vector2.SignedAngle(Vector2.up, positions[1] - positions[0]);
            return -Vector2.SignedAngle(Vector2.up, end - start);
        }
    }

    public float CurrentDirection
    {
        get
        {
            if (positions.Count > 1)
                return -Vector2.SignedAngle(Vector2.up, positions[positions.Count - 1] - positions[positions.Count - 2]);
            return -Vector2.SignedAngle(Vector2.up, end - start);
        }
    }

    public float Length
    {
        get
        {
            return length;
        }
    }


    public void Cancel()
    {
        Debug.Log("Aborted Gesture");
        phase = TouchPhase.Canceled;
        ResetGesture();
    }


    public override string ToString()
    {
        return "Gesture(" + start.ToString("F4") + " to " + end.ToString("F4") + ") in " + time.ToString("F2") + " sec || Direction = " + Direction.ToString("F0") + " || Length = " + Length.ToString("F1") + " || SwipeFactor = " + SwipeFactor.ToString("F1") + " || StartDir = " + StartDirection.ToString("F0") + " || " + (abortedGestureAngleFactor < 0 ? "LEFT" : "RIGHT") + " || " + gestureType + " || " + Speed.ToString();
    }

    void ResetGesture()
    {
        length = 0f;
        time = 0f;
        ended = false;
        gestureType = GestureType.Line;
        swipeFactor = 0f;
        abortedGestureAngleFactor = 0f;
        stationaryTime = 0f;
        currentSpeed = 0f;
    }

    public void RebaseGesture()
    {
        Start(ToScreenCoords(positions[positions.Count - 2]));
        Debug.Log("Rebased Gesture");
    }

    public float Speed
    {
        get
        {
            return length / (time > 0f ? time : -1f);
        }
    }
    public float CurrentSpeed
    {
        get
        {
            return currentSpeed;
        }
    }

    public float SwipeFactor
    {
        get
        {
            return swipeFactor;
        }
    }

    public GestureType Type
    {
        get
        {
            return gestureType;
        }
    }

    public bool Ended
    {
        get
        {
            return ended;
        }
    }

    public static Vector2 ToScreenCoords(Vector2 position)
    {
        return new Vector2(position.x * Screen.width, position.y * Screen.height);
    }

    public static Vector2 ToPercentualCoords(Vector2 position)
    {
        return new Vector2(position.x / Screen.width, position.y / Screen.height);
    }



    void DrawLine(List<Vector2> vertexes, Color color, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = Vector3.zero;//Camera.main.ScreenPointToRay(positions[0]).origin;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.01f;
        lr.endWidth = 0.01f;
        
        int i = 0;
        Vector3[] pos = new Vector3[vertexes.Count];
        foreach (Vector2 p in vertexes)
        {
            pos[i] = Camera.main.ScreenToWorldPoint(new Vector3(p.x, p.y, Camera.main.nearClipPlane + 0.1f));
            ++i;
        }
        lr.SetPositions(pos);
        GameObject.Destroy(myLine, duration);
    }
}
