﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LateralWall : MonoBehaviour
{
    public LayerMask ballLayer;
    public LayerMask pinLayer;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == Mathf.RoundToInt(Mathf.Log(ballLayer.value, 2)))
        {
            Debug.Log("BOING BOING, you hit a bouncy wall <3");
        }
        if (collision.gameObject.layer == Mathf.RoundToInt(Mathf.Log(pinLayer.value, 2)))
        {
            GameController.FakeDestroy(collision.gameObject);
        }
    }
}
