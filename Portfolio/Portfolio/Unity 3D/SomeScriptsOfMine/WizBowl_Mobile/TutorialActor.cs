﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActor : MonoBehaviour
{
    public int turnNumber = 1;
    public int order = 0;
    public TurnController.TurnPhases phase;
}
