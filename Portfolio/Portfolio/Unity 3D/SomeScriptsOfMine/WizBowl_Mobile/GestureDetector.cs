﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureDetector : MonoBehaviour
{
    [SerializeField] float minDistanceForSwipe = 0.01f;
    //[SerializeField] float minIgnoreSwipeOffset = 50f;
    [SerializeField] static float screenResolutionFactor = 0.5f;
    public static float curveAngleDeadZone = 10f;
    public static float swipeWidthDeadZone = 10f;

    static Gesture gesture;

    public static float screenResolutionScale
    {
        get
        {
            return screenResolutionFactor;
        }
    }

    public static event Action<Gesture> OnGesture;
    public static event Action<Gesture> OnTap;
    public static event Action<Gesture> OnPressing;

    // Use this for initialization
    private void Awake()
    {
        gesture = new Gesture();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                gesture.Start(touch.position);
            }
            else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Ended)
            {
                gesture.Update(touch.position, touch.phase);
                DetectGesture();
            }
            else
            {
                gesture.Cancel();
            }
        }
	}
    
    void DetectGesture()
    {
        if (IsGestureLongEnough())
        {
            SendGesture();
        }
        else
        {
            if (gesture.Ended)
            {
                SendTap();
            }
            else
            {
                SendPressure();
            }
        }
    }

    bool IsGestureLongEnough()
    {
        if (gesture.Length >= minDistanceForSwipe)
            return true;
        return false;
    }

    void SendGesture()
    {
        OnGesture(gesture);
    }

    void SendTap()
    {
        OnTap(gesture);
    }

    void SendPressure()
    {
        OnPressing(gesture);
    }

    public static void RebaseGesture()
    {
        gesture.RebaseGesture();
    }
}