﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Strike : MonoBehaviour
{
    [SerializeField] float strikeSpeed = 2f;
    [SerializeField] float fadeSpeed = 2f;
    [SerializeField] float fadeUpSpeed = 2f;

    private void Start()
    {
        transform.SetSiblingIndex(GameController.Instance.uiPauseMenu.transform.GetSiblingIndex() - 1);
    }
    // Update is called once per frame
    void Update ()
    {
        if (Time.timeScale == 0f) return;
		if(transform.localScale.x < 0.99f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, strikeSpeed * Time.deltaTime);
        }
        if (transform.localScale.x >=  0.99f)
        {
            transform.localScale = Vector3.one;
            transform.position += Vector3.up * (fadeUpSpeed * Time.deltaTime);
            GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.clear, fadeSpeed * Time.deltaTime);
            if(GetComponent<Image>().color.a < 0.01f)
            {
                Destroy(gameObject);
            }
        }
    }
}
