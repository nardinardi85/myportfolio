{
    "id": "8cf7a08b-1fed-47e9-bfc5-ac1c4da0e28a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_selection",
    "AntiAlias": 1,
    "TTFName": "fonts\\fnt_selection\\PressStart2P-Regular.ttf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a62070c3-416d-40a8-bd52-0df7deb89b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4fa6745e-6cfd-45ad-9029-24eec5d4a01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 80,
                "y": 79
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "52c6a169-ecbb-4de7-a78d-2af02b76ebd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 102,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a48300a5-dc5d-4b6a-9df9-f94a6761d4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "72dfde56-9b4f-4f9a-b96e-9dab089f1d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 57
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "83ec6183-73a6-4628-b7ab-588def846ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d9720728-cf4c-4590-9a23-9139db77a0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f1389277-ec88-4896-9377-12d19989e193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 3,
                "x": 86,
                "y": 79
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "55705039-7892-47eb-9eea-fe3b2f85fcef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 26,
                "y": 79
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8b324d85-6c73-488e-ab8d-69dcf76cac83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 61,
                "y": 79
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "085b3082-9b8c-4ceb-8ab3-33e26d6fc964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "402c6520-7ff4-4e5e-8b8b-1854a51326bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 90,
                "y": 57
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "60c4b552-d0e5-448e-baa6-5fa1fe7c4c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 74,
                "y": 79
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4ecf79f2-86a8-4815-8cad-4b511fd6a6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1902f733-fa21-463d-9534-48a04df52927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 3,
                "x": 106,
                "y": 79
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "febc45da-71fb-4ad8-ae5a-7bda463eb9eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 57
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3c90264e-83af-48a6-bca2-34c090536daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fca0dcd7-3baa-43f1-8249-7cffaa379dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2d7b21d2-9b12-459f-a655-27b01ec048c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dff268cf-fff7-4026-8e04-94c759bcd157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 35
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "502e6a36-88a2-4da8-8710-84dcdf8d420c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 35
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "23ddca30-692f-40ec-84b4-95a65e9274ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c1be2807-0bd2-4c0a-8ead-fe5d9937b4cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ee7f8838-c1f9-441c-ae7d-fb53d17eb100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6678748e-ea09-4c43-bc2d-26dc7e3a4725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1fcbed4f-50d1-4d99-b445-66ff0dad5472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9b05f014-df0a-4315-a58f-937647578e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 3,
                "x": 91,
                "y": 79
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dbf1ed26-f0a7-4aca-aede-3b13b34224c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 68,
                "y": 79
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "09cef6bc-08ad-4fc4-afcb-afa2087f9923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 118,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "208d9e09-58e3-45f5-a3a1-c696db0fc1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 57
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "af006a32-5d83-4df0-b77b-8a6e99568b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 110,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cce10b02-27a4-49aa-a26d-0a46a4388261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 57
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9ac0ca23-a81a-4e2b-a647-f5b0bb9179e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 57
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e5ffd6d3-5600-46f0-adce-a2cd3078eca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 57
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "13563067-74d4-4a26-9156-fe614f1f6614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 57
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "84530975-63bd-4c71-8a3a-fcc4ffdfc6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "40b4c6a4-6f90-4618-8594-9e3f3626bbef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2c091dcf-09c5-49cf-9222-0bd7adcc9abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 35
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8867c362-e2f2-4722-9b11-da0115e249ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 35
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4f48b30c-35dd-45a0-b130-10bd20cb9e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 35
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "37600729-cf27-4de4-bfe4-19366b6293da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 24
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "15bc6405-6bb7-40b8-a141-64f9561214b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3baa3327-aa54-4576-a2da-3c3090646ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 13
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e53d0e16-9eeb-457b-b6a3-33e9a6639dc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 13
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b650cd54-b7c4-442d-bda4-c6d0a4cafe71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "73ad3df3-3180-4f65-8f60-016540cbcc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 13
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "29d518f0-aa55-4936-892b-adabb0b8365b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 13
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "47e4a5d9-bfb8-468e-9f34-a4c8aaca6db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 35
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "12a8356e-6dfb-4f4a-92f8-f56aa5badaf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e3604187-7f1c-4d47-b66e-63f5d2edba5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4d542aec-47ad-4cfe-931a-f332a505fd8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "45fdd8d0-2338-413e-a9e6-2bc923cb7a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b14d243d-74ef-4b1f-936a-79bfd50dda70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fc753dec-cc61-4b98-a2fa-7b9aa776dac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "97453018-b384-4771-94a0-1cfa317cddfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8a06af2a-ed3c-4719-8b0c-8055b8f9278b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b6bc6b88-378a-438a-8031-20d209754f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bd735e8b-f70f-4692-8383-3a052ed08e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 68
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "626404f2-2fa4-4c41-989c-4b4185849485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8c75bfaa-c5cb-428c-90f1-3a37e744c78d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 33,
                "y": 79
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a642a627-6724-4b2a-a736-b199ca7ae2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1fd869ad-643e-47da-93b9-378b5bc981b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 40,
                "y": 79
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2e2b203d-a7a0-4ae2-ac77-d18cd2f8d6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 18,
                "y": 79
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1b7f2dbd-a30d-4f5c-9daf-c8b1fa201a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 13
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f3fd0948-1a5d-48b8-97f1-0bc00cfa5f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 96,
                "y": 79
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a8b0e011-ad9f-449c-a538-1b80e0231e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 13
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ecf17180-1937-42ce-8e05-03e7728ca5fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 13
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "233a347b-5b40-4fb6-9808-c9d76bcd3c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 13
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9b76d33d-04c3-4861-90ac-56cce8e6b05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 35
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "aa8a36ca-2d09-4937-a342-bdc79f8524b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 35
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1a24df5a-2f7b-4d9c-85d3-9713a0d588db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 68
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c92ff804-0044-4e58-94d3-5ee6f298de24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f0fb54c8-c680-4501-8710-f64a3893deb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "389f4be7-ce85-46fb-86df-b3d14823cbeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a398347d-e658-4112-9b73-0c9b3e8235e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 10,
                "y": 79
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5220352b-9f21-4fbe-be0d-c69619d72d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "078828fb-c14d-4044-ba60-810652230051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 110,
                "y": 57
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7fff4763-737e-4ca8-9f81-a94ed4eb80b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5e092fa2-ae38-45d0-b70b-81d6ac7e898d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1cbc65e7-4e35-4317-8069-a91f23e45ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 35
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2a9be925-e29d-4153-9293-dc29fb47f771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fdcbcd43-ab72-4954-81ba-3b3609035b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "09f8b5d3-f8e3-49aa-bd5f-31f141a6383c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "12fbdc58-04d7-479d-a7dc-e26f0fd20f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "47948ca4-7df8-4e78-9565-aff7df7eb8ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 57
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ace98021-8b58-4eaf-9605-0416bb645c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "af82ce0d-00c0-4059-8d16-0a98e46a5a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b106eb9a-e4c8-4592-9373-3247345402b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a68ece54-77da-408b-b344-45d3ed773f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "feb5c050-d2b2-4265-b621-2926bddaf608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d3264e0d-2e49-4303-99ad-deb3307df5f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "20a1b218-4432-444c-bedc-03b3ba5f46c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 47,
                "y": 79
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8d45b787-9dcc-469f-81d8-b5e644b37927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 101,
                "y": 79
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dba0867d-c6ef-40bf-b3a7-6d75bfdf04dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 54,
                "y": 79
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ac0e7608-de7a-41d1-aa3b-6cc8b17fa565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "24d9645c-01eb-4aa7-a6d6-257fe8d6fdb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 2,
                "y": 79
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 7,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}