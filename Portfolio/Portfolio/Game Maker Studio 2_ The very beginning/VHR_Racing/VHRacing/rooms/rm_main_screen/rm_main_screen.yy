
{
    "name": "rm_main_screen",
    "id": "896a380a-6a7b-4fc7-8c6e-c5ad0349cf27",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "a9b24dab-0fb0-4da0-b18b-2bded221513c",
        "7def30b7-3501-4d3b-b2e7-78249072f9d9",
        "b8d55b62-f03a-47cd-9d49-93f5cda95feb",
        "a127e22b-cc1c-4c11-9db0-0986e00828b0"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "System",
            "id": "ec636317-e478-41c8-93d0-d20912306a3b",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_game_rules","id": "a9b24dab-0fb0-4da0-b18b-2bded221513c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_game_rules","objId": "0daf92c2-392b-40f2-9590-b59e407cbd38","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -32,"y": 0}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Buttons",
            "id": "d885ba20-0f10-429e-ae0c-742603e64598",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_button_free_race","id": "7def30b7-3501-4d3b-b2e7-78249072f9d9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_button_free_race","objId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","properties": [{"id": "07c4d134-ed04-4840-addd-7cd8520878b7","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "a7b0ab43-60ae-42cf-a16a-57369cb9a66d","mvc": "1.0","value": "0"},{"id": "638141e4-c417-4324-b782-b5a8e20f9235","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "3ac0eea4-0b1f-4bd4-b53a-951f7d5ea75e","mvc": "1.0","value": "changeRoom"},{"id": "67d33148-7242-434a-8e60-711df816bde7","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "fb1e481e-bbb7-4554-8bc9-98488b3536e1","mvc": "1.0","value": "rm_car_selection"},{"id": "0e64943c-9ffb-4b9e-8aea-e4aab3407da3","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "575bc36d-c649-4176-bce4-0161fabc019e","mvc": "1.0","value": "\"Game Mode\""},{"id": "eba6d62d-c884-44a0-97da-efe4f805efa0","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "07509d3a-b1cc-4d8a-895a-4684b2c4eabc","mvc": "1.0","value": "game_mode.free_race"},{"id": "6bf06919-99d1-4bdf-a190-10bf6847c409","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "7e1b7b15-78f0-4dd2-9d54-fe32da0c9ad3","mvc": "1.0","value": "Free Race"},{"id": "48b74db2-c143-4ce8-829c-e24d3c8c44f2","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "0064367f-7d28-47dc-8679-cf8619fae775","mvc": "1.0","value": "spr_button_default"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 128,"y": 306},
{"name": "inst_button_quit","id": "b8d55b62-f03a-47cd-9d49-93f5cda95feb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_button_quit","objId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","properties": [{"id": "6f8b8dd5-63c5-46aa-bfbb-76a57a2d50f0","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "a7b0ab43-60ae-42cf-a16a-57369cb9a66d","mvc": "1.0","value": "1"},{"id": "702daed4-c926-4066-bf01-4a9ba4bcd738","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "3ac0eea4-0b1f-4bd4-b53a-951f7d5ea75e","mvc": "1.0","value": "exitGame"},{"id": "22973e4c-4b68-4917-aa4a-c301622abe0a","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "0064367f-7d28-47dc-8679-cf8619fae775","mvc": "1.0","value": "spr_button_default"},{"id": "f139dbb9-2a54-4550-8a4b-f738b134bb9a","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "7e1b7b15-78f0-4dd2-9d54-fe32da0c9ad3","mvc": "1.0","value": "Quit"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 342}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Header",
            "id": "a4e6bbc1-a269-4465-beb6-d85c85f2400e",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_1DBF7820","id": "a127e22b-cc1c-4c11-9db0-0986e00828b0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1DBF7820","objId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","properties": [{"id": "229d64ad-60e0-45a9-a81a-6d62220267e4","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "0064367f-7d28-47dc-8679-cf8619fae775","mvc": "1.0","value": "spr_title"}],"rotation": 0,"scaleX": 0.72,"scaleY": 0.72,"mvc": "1.0","x": 428,"y": 194}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "ab342f33-085b-4b9f-9d64-561ba7f48f07",
            "animationFPS": 10,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "f48200d2-4f5b-4ef6-957e-7807670ad0d7",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 0,
        "PhysicsWorldPixToMeters": 0.01,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "0b31cdb9-13bb-4a44-9259-b9d0bd50eaf1",
        "Height": 360,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 640
    },
    "mvc": "1.0",
    "views": [
{"id": "34c8f600-163a-4a30-8d5d-d6609fade5d4","hborder": 32,"hport": 1080,"hspeed": -1,"hview": 360,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1920,"wview": 640,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d9f26efa-913d-4c94-9c05-251d243013f1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "17005afe-2b04-4fd9-91ef-37d557164446","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ddfd25ab-6a72-42d2-bb46-a73220732744","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "8e49a8f2-f881-4c0d-ae8f-e6e0fa5c6787","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "3396594c-17ea-4856-bc60-c940ff881322","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fb81a28f-664e-457a-a3fb-5bb002d36e41","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "766dbc0a-fe1b-4297-b8d7-e8834ded833f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "ae0b7070-f36c-42a4-b467-5b14d507c0dc",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}