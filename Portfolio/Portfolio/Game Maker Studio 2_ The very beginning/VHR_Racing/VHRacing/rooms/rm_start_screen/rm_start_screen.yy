
{
    "name": "rm_start_screen",
    "id": "e8734c46-e3d7-4d89-b2ad-c481a8c6e0f5",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "6a75441a-ebad-47c2-826a-b45f0ddf276d",
        "35fe06c3-5938-4785-ae81-23dd97bdc021",
        "39e93aa1-cb2f-4468-8aa5-ea267f1a6578",
        "14f3ccd5-8085-4e1b-8068-f285bc11e49e",
        "6ce84570-042d-437c-a631-d019def6e365",
        "f0445ae0-26bc-45a7-8d94-c283fd27f872"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "System",
            "id": "b59f7b74-e299-4ca8-9aa2-4ecd7bdf2c96",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_system","id": "6a75441a-ebad-47c2-826a-b45f0ddf276d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_system","objId": "4588d543-8c4d-4de7-9086-616bd3d04f12","properties": [{"id": "d11758b6-abba-4c76-bfaf-0ff90ca0630b","modelName": "GMOverriddenProperty","objectId": "4588d543-8c4d-4de7-9086-616bd3d04f12","propertyId": "241079aa-ab12-455d-9219-c3afd113dcab","mvc": "1.0","value": "True"},{"id": "ef3ed17d-8874-4d94-b357-28bc64e636a9","modelName": "GMOverriddenProperty","objectId": "4588d543-8c4d-4de7-9086-616bd3d04f12","propertyId": "99ce5cfa-686e-40b6-bbfd-4ff8337960c5","mvc": "1.0","value": "True"},{"id": "6b59320e-a994-4b9e-8fe1-444aebfa1ec9","modelName": "GMOverriddenProperty","objectId": "4588d543-8c4d-4de7-9086-616bd3d04f12","propertyId": "7b012dbf-d37c-44b0-ba82-9686d3267a93","mvc": "1.0","value": "True"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -32,"y": 0},
{"name": "inst_input","id": "35fe06c3-5938-4785-ae81-23dd97bdc021","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_input","objId": "d534a33e-8313-4067-9465-469c2de2579d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -32,"y": 32},
{"name": "inst_data","id": "39e93aa1-cb2f-4468-8aa5-ea267f1a6578","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_data","objId": "33263b01-2b1e-42a1-9f98-b0bba04fc01c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -32,"y": 64},
{"name": "inst_3F10B7E7","id": "f0445ae0-26bc-45a7-8d94-c283fd27f872","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3F10B7E7","objId": "96fd83e4-5402-484a-858f-36383f88923a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -32,"y": 96}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Buttons",
            "id": "a3465bb2-5918-452b-aa05-2a06b2e6bb3d",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_press_enter","id": "6ce84570-042d-437c-a631-d019def6e365","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_press_enter","objId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","properties": [{"id": "9c33d5f9-6541-4869-accd-23aa38f0b736","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "3ac0eea4-0b1f-4bd4-b53a-951f7d5ea75e","mvc": "1.0","value": "changeRoom"},{"id": "18cf13e3-9d9e-4081-a887-24ce46dde843","modelName": "GMOverriddenProperty","objectId": "268b8206-9667-4c8a-a3a2-26570f55e0a8","propertyId": "fb1e481e-bbb7-4554-8bc9-98488b3536e1","mvc": "1.0","value": "rm_main_screen"},{"id": "b09c0dc6-0544-4e38-b09e-17ffb9a7df97","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "7e1b7b15-78f0-4dd2-9d54-fe32da0c9ad3","mvc": "1.0","value": "Enter"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 342}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Header",
            "id": "fe623fa6-312f-47fd-83cf-67a7c899de61",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_title_start","id": "14f3ccd5-8085-4e1b-8068-f285bc11e49e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_title_start","objId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","properties": [{"id": "9e130bf1-7d7d-4d08-9d67-d7c2671a17df","modelName": "GMOverriddenProperty","objectId": "245b0ce2-fbf4-4ef3-a9b0-71a7d5245784","propertyId": "0064367f-7d28-47dc-8679-cf8619fae775","mvc": "1.0","value": "spr_title"}],"rotation": 0,"scaleX": 0.72,"scaleY": 0.72,"mvc": "1.0","x": 428,"y": 194}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Path_1",
            "id": "dafc56d4-1e3f-4bcc-b80e-66ee94af52a3",
            "colour": { "Value": 4278190335 },
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRPathLayer",
            "pathId": "7f0b3baf-e183-4a65-affe-0592f316715e",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "b5fb863e-4f21-4be2-923d-74261c37f537",
            "animationFPS": 10,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "f6c6bb45-090f-46bd-911a-afc057a86df9",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 0,
        "PhysicsWorldPixToMeters": 0.01,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "70987781-e0a0-4ab4-90d4-8847f3b1daa7",
        "Height": 360,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 640
    },
    "mvc": "1.0",
    "views": [
{"id": "ed9fe998-3b59-4e07-93f5-144b884b2846","hborder": 32,"hport": 1080,"hspeed": -1,"hview": 360,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1920,"wview": 640,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c3dda5a4-b8e0-4f75-882a-e6bceb36673d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6db65e92-d0c6-47b2-9e2f-92a88bc0126e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ad65169f-9201-4f37-a72b-d4c274edd853","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5f08a81a-df50-4367-8462-c61fdc0b2c2d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e864f1f2-0834-4703-8adb-c70c43abc427","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "399a8348-6511-440f-861e-9658cd13834f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "932756eb-7501-4491-b6e6-97e7ed80c1ae","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "299ec6c1-87a0-4c0a-8292-db9efb783b9f",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}