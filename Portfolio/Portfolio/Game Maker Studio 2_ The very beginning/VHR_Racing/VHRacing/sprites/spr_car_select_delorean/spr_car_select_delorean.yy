{
    "id": "0f1371d5-9e95-434f-b194-7465fa369729",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_select_delorean",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 48,
    "bbox_right": 351,
    "bbox_top": 88,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "630615cb-ecf1-498c-84da-de252f41ad65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f1371d5-9e95-434f-b194-7465fa369729",
            "compositeImage": {
                "id": "58e58d05-0bbe-445e-b263-bbf40794cf91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630615cb-ecf1-498c-84da-de252f41ad65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c780272-517d-4487-99af-543ae714aa50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630615cb-ecf1-498c-84da-de252f41ad65",
                    "LayerId": "32332cdb-780a-4e4a-ac74-bd1fa86b7f73"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 270,
    "layers": [
        {
            "id": "32332cdb-780a-4e4a-ac74-bd1fa86b7f73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f1371d5-9e95-434f-b194-7465fa369729",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 390,
    "xorig": 195,
    "yorig": 135
}