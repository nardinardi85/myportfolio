{
    "id": "2fb8de71-6647-48a0-a68b-ec5a544ea6e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_npc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9367394-50dd-4ec0-898e-d80d4db60494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb8de71-6647-48a0-a68b-ec5a544ea6e4",
            "compositeImage": {
                "id": "6538ddbb-b3ed-4025-a0d2-f704fb7fb105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9367394-50dd-4ec0-898e-d80d4db60494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141e3d95-cfa3-4004-abbe-45f2bcf1ab0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9367394-50dd-4ec0-898e-d80d4db60494",
                    "LayerId": "e9a6f47b-25f6-4593-9ac3-54bf0fe6c86e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e9a6f47b-25f6-4593-9ac3-54bf0fe6c86e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fb8de71-6647-48a0-a68b-ec5a544ea6e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}