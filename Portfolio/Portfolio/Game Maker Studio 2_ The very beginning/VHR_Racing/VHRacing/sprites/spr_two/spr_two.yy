{
    "id": "79384133-518d-4785-8f05-0f2b9f6fbaf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9689b91d-127b-41ff-9b37-49c7c6b6ccd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79384133-518d-4785-8f05-0f2b9f6fbaf5",
            "compositeImage": {
                "id": "ceef59e8-8b11-4b28-a742-239372536439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9689b91d-127b-41ff-9b37-49c7c6b6ccd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63ed60d-82b0-4dd9-9a29-10b12c28d952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9689b91d-127b-41ff-9b37-49c7c6b6ccd1",
                    "LayerId": "7241cb5f-d20b-4cdb-8cee-086e432862cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7241cb5f-d20b-4cdb-8cee-086e432862cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79384133-518d-4785-8f05-0f2b9f6fbaf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}