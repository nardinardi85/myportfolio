{
    "id": "4d51a73c-183e-4d82-abed-be6d2fe79862",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b43dc236-754e-4805-a777-f06e199b6604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d51a73c-183e-4d82-abed-be6d2fe79862",
            "compositeImage": {
                "id": "38f99901-eadf-4887-b68f-cd817e33b426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43dc236-754e-4805-a777-f06e199b6604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e278cd-c1b5-4ad7-b42b-69a3178bd31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43dc236-754e-4805-a777-f06e199b6604",
                    "LayerId": "d0f9cad1-9c55-4597-b05f-253a545dfd96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0f9cad1-9c55-4597-b05f-253a545dfd96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d51a73c-183e-4d82-abed-be6d2fe79862",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}