{
    "id": "f0d84ed5-fa19-4e41-a6dc-4747af3d5671",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_zero",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5a92bcb-8362-4174-ab83-bb3a16d7647f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d84ed5-fa19-4e41-a6dc-4747af3d5671",
            "compositeImage": {
                "id": "e9270d28-9e05-4d88-952e-848f5c2a895a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a92bcb-8362-4174-ab83-bb3a16d7647f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876de017-ce4b-4110-8190-50e4174d034f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a92bcb-8362-4174-ab83-bb3a16d7647f",
                    "LayerId": "ad674695-067e-4337-9185-4b3f5d37b393"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad674695-067e-4337-9185-4b3f5d37b393",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0d84ed5-fa19-4e41-a6dc-4747af3d5671",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}