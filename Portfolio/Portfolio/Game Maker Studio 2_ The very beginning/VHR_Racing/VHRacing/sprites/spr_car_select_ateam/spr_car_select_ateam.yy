{
    "id": "9b95de39-2eef-41a6-9d97-2bddad29795c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_select_ateam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 48,
    "bbox_right": 351,
    "bbox_top": 88,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a20117d1-be5e-4d82-9260-3e17035ffc4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b95de39-2eef-41a6-9d97-2bddad29795c",
            "compositeImage": {
                "id": "aa7286d4-daa3-43a7-aa12-0ea0a90f5af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a20117d1-be5e-4d82-9260-3e17035ffc4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e086ae0b-d5b9-46ad-8268-103fb025e58c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a20117d1-be5e-4d82-9260-3e17035ffc4f",
                    "LayerId": "3707cabb-6f01-4e40-a5b4-cb4c322ad42d"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 270,
    "layers": [
        {
            "id": "3707cabb-6f01-4e40-a5b4-cb4c322ad42d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b95de39-2eef-41a6-9d97-2bddad29795c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 390,
    "xorig": 195,
    "yorig": 135
}