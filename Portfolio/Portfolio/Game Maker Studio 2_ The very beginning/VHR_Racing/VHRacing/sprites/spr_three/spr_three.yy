{
    "id": "81c0e1e3-69e8-4bf9-9c93-4f5a32689b3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed547653-566c-4fc8-9501-fa7f71b1e877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c0e1e3-69e8-4bf9-9c93-4f5a32689b3d",
            "compositeImage": {
                "id": "c58de6ed-a8d4-44df-8a53-9656953b9761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed547653-566c-4fc8-9501-fa7f71b1e877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361023f6-f18c-4570-a606-73a1a04ab6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed547653-566c-4fc8-9501-fa7f71b1e877",
                    "LayerId": "6a67f2c0-ff42-4ab7-96d1-c87b865323cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6a67f2c0-ff42-4ab7-96d1-c87b865323cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81c0e1e3-69e8-4bf9-9c93-4f5a32689b3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}