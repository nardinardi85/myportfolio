{
    "id": "b3d72bdc-2600-4c13-b9ae-1b6619e0bf62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_select_track_03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 6,
    "bbox_right": 56,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bbbf27c-94db-4c32-8fa7-04ac7cba60af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3d72bdc-2600-4c13-b9ae-1b6619e0bf62",
            "compositeImage": {
                "id": "5d72f58b-1055-40ff-83de-c7dfb6629de5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bbbf27c-94db-4c32-8fa7-04ac7cba60af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3b67e3-780e-435f-a100-543bac306a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bbbf27c-94db-4c32-8fa7-04ac7cba60af",
                    "LayerId": "76ea88f3-faa0-4586-9779-c72fca5059ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "76ea88f3-faa0-4586-9779-c72fca5059ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3d72bdc-2600-4c13-b9ae-1b6619e0bf62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}