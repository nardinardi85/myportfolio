{
    "id": "62bdce2b-7ec5-47f0-8c6b-938ca2b08998",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79fa3eb6-58d5-4102-a0e7-e5881f82721f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bdce2b-7ec5-47f0-8c6b-938ca2b08998",
            "compositeImage": {
                "id": "5a5fc22a-b2fe-4c73-a98e-3069b37edeb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79fa3eb6-58d5-4102-a0e7-e5881f82721f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7e6ad5-ef48-4b19-b942-48d5ca2391cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79fa3eb6-58d5-4102-a0e7-e5881f82721f",
                    "LayerId": "6d2c37a4-7762-4464-bdcc-aa3aa1f32169"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6d2c37a4-7762-4464-bdcc-aa3aa1f32169",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62bdce2b-7ec5-47f0-8c6b-938ca2b08998",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}