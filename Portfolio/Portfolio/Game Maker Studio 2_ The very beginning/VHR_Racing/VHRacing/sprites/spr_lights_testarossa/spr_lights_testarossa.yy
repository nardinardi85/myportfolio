{
    "id": "30f816ae-119b-42a6-ba3e-a3ac0e9d65c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lights_testarossa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 8,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07676063-5501-43df-a52b-d0ee8dd5d274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30f816ae-119b-42a6-ba3e-a3ac0e9d65c2",
            "compositeImage": {
                "id": "5aa02e90-244c-45d5-b115-6dffc91ccffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07676063-5501-43df-a52b-d0ee8dd5d274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1321911-32d9-446c-8836-718961ad20c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07676063-5501-43df-a52b-d0ee8dd5d274",
                    "LayerId": "899a525b-3aa6-4162-92dd-201f9d04781a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "899a525b-3aa6-4162-92dd-201f9d04781a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30f816ae-119b-42a6-ba3e-a3ac0e9d65c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}