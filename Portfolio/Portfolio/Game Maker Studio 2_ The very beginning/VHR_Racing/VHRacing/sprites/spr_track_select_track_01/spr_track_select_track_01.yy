{
    "id": "8c9dcf8a-9366-447e-983b-3741b1a634fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_select_track_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15d9fbaf-810a-45a0-8ad2-1387fd2bf291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c9dcf8a-9366-447e-983b-3741b1a634fd",
            "compositeImage": {
                "id": "b2dd5090-8e6e-44cf-828b-b67044ed6032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15d9fbaf-810a-45a0-8ad2-1387fd2bf291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77dda188-7d1b-44ac-a287-81cb8b10eafb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15d9fbaf-810a-45a0-8ad2-1387fd2bf291",
                    "LayerId": "a08b6f72-a6cc-4c3a-9b39-b4711a276dc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a08b6f72-a6cc-4c3a-9b39-b4711a276dc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c9dcf8a-9366-447e-983b-3741b1a634fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}