{
    "id": "303f690e-bcbe-4b19-b144-41624b3b321f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_tires_armored",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 17,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d59e08c-fed9-4c82-8280-3495aaddf070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303f690e-bcbe-4b19-b144-41624b3b321f",
            "compositeImage": {
                "id": "78a69b70-221d-440e-9520-b880965c38b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d59e08c-fed9-4c82-8280-3495aaddf070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e020fc0-224e-4142-b985-d7ad442f199b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d59e08c-fed9-4c82-8280-3495aaddf070",
                    "LayerId": "137b8542-92a3-405a-ab9d-85153552dea0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "137b8542-92a3-405a-ab9d-85153552dea0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "303f690e-bcbe-4b19-b144-41624b3b321f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}