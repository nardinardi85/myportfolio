{
    "id": "b81b600a-ca85-445d-8b58-8b2344f35a28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_traffic_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0780c72-0d86-484b-ba31-b57b7e3dd312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "compositeImage": {
                "id": "f170a6c5-cb5e-4b14-8bdd-6099a1204b2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0780c72-0d86-484b-ba31-b57b7e3dd312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1265e5a9-68d9-48b8-9e8d-5e52ce6e33cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0780c72-0d86-484b-ba31-b57b7e3dd312",
                    "LayerId": "105f3eea-597b-465f-9c39-6b9b14c11a63"
                }
            ]
        },
        {
            "id": "10bc0413-4d2e-42b0-af8a-279a3504440e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "compositeImage": {
                "id": "e1e8d4a5-eb92-4aa1-8fee-42dfd2ca1ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10bc0413-4d2e-42b0-af8a-279a3504440e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f66c68-b789-4bed-9849-8fedf2c90bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10bc0413-4d2e-42b0-af8a-279a3504440e",
                    "LayerId": "105f3eea-597b-465f-9c39-6b9b14c11a63"
                }
            ]
        },
        {
            "id": "1703e150-560c-4a8c-ba75-1e2ce4f7dc48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "compositeImage": {
                "id": "adb149b8-5a1f-4584-a36f-d6226589c01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1703e150-560c-4a8c-ba75-1e2ce4f7dc48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eee5ce2-c36d-4cbb-b018-f66f058bbe76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1703e150-560c-4a8c-ba75-1e2ce4f7dc48",
                    "LayerId": "105f3eea-597b-465f-9c39-6b9b14c11a63"
                }
            ]
        },
        {
            "id": "88ed4adb-e163-4f3d-8007-7f20645398f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "compositeImage": {
                "id": "be06cff1-9ef3-4e8d-9d73-1e035c2be739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ed4adb-e163-4f3d-8007-7f20645398f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92838467-b2df-4060-a353-48634713a01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ed4adb-e163-4f3d-8007-7f20645398f9",
                    "LayerId": "105f3eea-597b-465f-9c39-6b9b14c11a63"
                }
            ]
        },
        {
            "id": "12b151cb-9bba-4c66-a7c9-5e6f7475dc34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "compositeImage": {
                "id": "bb7bd4d9-5051-4736-ac65-f7a6ac6b8a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12b151cb-9bba-4c66-a7c9-5e6f7475dc34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b649e0-b123-4a71-b976-e4a474326c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12b151cb-9bba-4c66-a7c9-5e6f7475dc34",
                    "LayerId": "105f3eea-597b-465f-9c39-6b9b14c11a63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "105f3eea-597b-465f-9c39-6b9b14c11a63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}