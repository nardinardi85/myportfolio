{
    "id": "579e5b77-217f-48dd-b6f9-81d0f4509d8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_tires_interceptor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 11,
    "bbox_right": 19,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2082121-584f-472c-9cc2-1c9fb64f6587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "579e5b77-217f-48dd-b6f9-81d0f4509d8d",
            "compositeImage": {
                "id": "ee7219bc-bc92-4869-9d42-9a959739a252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2082121-584f-472c-9cc2-1c9fb64f6587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31100945-aa0d-41ec-b32c-ccd4e4ed3625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2082121-584f-472c-9cc2-1c9fb64f6587",
                    "LayerId": "02fbf1e6-3add-4291-a9ec-53ce3467a63f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "02fbf1e6-3add-4291-a9ec-53ce3467a63f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "579e5b77-217f-48dd-b6f9-81d0f4509d8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}