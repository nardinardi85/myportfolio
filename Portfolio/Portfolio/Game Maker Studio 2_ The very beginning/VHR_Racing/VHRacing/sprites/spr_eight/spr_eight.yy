{
    "id": "c7db1b61-9e70-44f0-900e-9fc472a38871",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_eight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de9c6aa1-c068-4151-8f2c-7767aeef74d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7db1b61-9e70-44f0-900e-9fc472a38871",
            "compositeImage": {
                "id": "8289a7bd-4340-4ed0-92ee-a89cd1f5c7ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de9c6aa1-c068-4151-8f2c-7767aeef74d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b40ce7a-ab11-4f12-a268-4c61c80fcbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de9c6aa1-c068-4151-8f2c-7767aeef74d6",
                    "LayerId": "d78898e6-8803-46fe-be11-a9e21591d5d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d78898e6-8803-46fe-be11-a9e21591d5d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7db1b61-9e70-44f0-900e-9fc472a38871",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}