{
    "id": "c4589666-8968-4b76-870e-9c36de01fd13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wallino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24eae598-f983-4ccb-aa2d-663b45e0521f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4589666-8968-4b76-870e-9c36de01fd13",
            "compositeImage": {
                "id": "3b103dea-48fc-40ab-a39e-9f69205741f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24eae598-f983-4ccb-aa2d-663b45e0521f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc12c64a-121c-47ff-9fbf-4ec0d753d401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24eae598-f983-4ccb-aa2d-663b45e0521f",
                    "LayerId": "91305e62-62e5-4e78-8dab-635dfb324e33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "91305e62-62e5-4e78-8dab-635dfb324e33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4589666-8968-4b76-870e-9c36de01fd13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}