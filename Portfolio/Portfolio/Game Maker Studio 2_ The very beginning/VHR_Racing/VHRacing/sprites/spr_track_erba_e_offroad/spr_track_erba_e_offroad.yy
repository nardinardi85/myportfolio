{
    "id": "7b573700-0840-402e-a531-e7ebaf31aa34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_erba_e_offroad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1439,
    "bbox_left": 32,
    "bbox_right": 1407,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48a68661-18ff-4da4-b962-b07c86e3bf6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b573700-0840-402e-a531-e7ebaf31aa34",
            "compositeImage": {
                "id": "2da24195-0662-4548-8851-f9e2cafe5e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a68661-18ff-4da4-b962-b07c86e3bf6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686873c1-00d7-4ee4-90fa-7584cc669e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a68661-18ff-4da4-b962-b07c86e3bf6c",
                    "LayerId": "66a379fd-f5bb-42eb-84ca-5575ce043758"
                },
                {
                    "id": "07595edf-f37a-44c2-8d38-0db8303e2925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a68661-18ff-4da4-b962-b07c86e3bf6c",
                    "LayerId": "e65d51a6-e42a-480f-8018-90fbc0c29c44"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 1440,
    "layers": [
        {
            "id": "66a379fd-f5bb-42eb-84ca-5575ce043758",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b573700-0840-402e-a531-e7ebaf31aa34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e65d51a6-e42a-480f-8018-90fbc0c29c44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b573700-0840-402e-a531-e7ebaf31aa34",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1440,
    "xorig": 0,
    "yorig": 0
}