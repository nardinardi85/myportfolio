{
    "id": "7c30e0f8-d0e6-4730-90e0-65e327dfee00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1389,
    "bbox_left": 32,
    "bbox_right": 1343,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bbc3ffd-65b1-42aa-b946-90e7e7a7a6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c30e0f8-d0e6-4730-90e0-65e327dfee00",
            "compositeImage": {
                "id": "28d09e03-6e83-4ace-9627-89a53d8465fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bbc3ffd-65b1-42aa-b946-90e7e7a7a6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf7fba64-8fca-4b30-b22e-2b8cf2a8f556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bbc3ffd-65b1-42aa-b946-90e7e7a7a6ef",
                    "LayerId": "15db2c96-0e55-46ca-939b-3860f0b2c87d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1440,
    "layers": [
        {
            "id": "15db2c96-0e55-46ca-939b-3860f0b2c87d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c30e0f8-d0e6-4730-90e0-65e327dfee00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1440,
    "xorig": 0,
    "yorig": 0
}