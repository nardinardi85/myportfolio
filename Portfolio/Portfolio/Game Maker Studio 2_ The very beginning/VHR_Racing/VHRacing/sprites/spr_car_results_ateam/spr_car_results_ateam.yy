{
    "id": "f652c956-9e65-45b5-a4a6-b47f71e7bc5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car_results_ateam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dcd59a7-ce93-4f3f-aa09-0d81fa9eb4de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f652c956-9e65-45b5-a4a6-b47f71e7bc5e",
            "compositeImage": {
                "id": "c14b7ef6-b4e3-4a68-9647-396427928e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dcd59a7-ce93-4f3f-aa09-0d81fa9eb4de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e4d22d-ae0c-49a6-9fac-f9d5608e481c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dcd59a7-ce93-4f3f-aa09-0d81fa9eb4de",
                    "LayerId": "39f0037b-a273-4fa0-85f7-ce60b9b42cab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "39f0037b-a273-4fa0-85f7-ce60b9b42cab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f652c956-9e65-45b5-a4a6-b47f71e7bc5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 63,
    "xorig": 31,
    "yorig": 32
}