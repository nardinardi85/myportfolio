{
    "id": "e9115c86-fdbe-4fdd-b5e8-33001220db30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_select_track_08",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "039fbafa-0d1a-4e98-9016-0350b7ba80b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9115c86-fdbe-4fdd-b5e8-33001220db30",
            "compositeImage": {
                "id": "f698e4df-a6e3-4589-ba7a-de45072cfcc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "039fbafa-0d1a-4e98-9016-0350b7ba80b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a236c2-f47d-4fb9-8b0d-cd69001d13f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "039fbafa-0d1a-4e98-9016-0350b7ba80b7",
                    "LayerId": "cd90f680-a067-453b-8b32-9bb6921123a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cd90f680-a067-453b-8b32-9bb6921123a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9115c86-fdbe-4fdd-b5e8-33001220db30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}