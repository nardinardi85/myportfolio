{
    "id": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b8cffa5-d6c5-4af2-9ac2-4862ca472553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "9050f236-f4ff-4b54-95c9-1f808acbb748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8cffa5-d6c5-4af2-9ac2-4862ca472553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb37aa52-da69-4887-b9b3-ce35a0622f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8cffa5-d6c5-4af2-9ac2-4862ca472553",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "2968c4d1-69de-4ba6-a93a-ea2ccc66bf3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "7e06ac56-5503-482b-9464-448857164c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2968c4d1-69de-4ba6-a93a-ea2ccc66bf3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2256d750-1718-4b7c-9428-53730a3b8ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2968c4d1-69de-4ba6-a93a-ea2ccc66bf3e",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "17960d12-3241-40bb-9a51-5ad243da403f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "3a24e686-d99c-4c98-92dd-4ffaec679b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17960d12-3241-40bb-9a51-5ad243da403f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b51576f4-fcb8-4a36-8fd7-9da248a0def8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17960d12-3241-40bb-9a51-5ad243da403f",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "ad0d66e4-7565-4cdc-805b-9333cdfd45dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "1900159e-8290-4a15-af84-c9285f43b0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0d66e4-7565-4cdc-805b-9333cdfd45dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fba3bf5-546f-457b-8366-54d38f152aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0d66e4-7565-4cdc-805b-9333cdfd45dd",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "b60a3ee2-14ea-4de9-b543-753ce6b587c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "6252ff5c-59b0-4b5f-a3a0-0cf70d0506c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60a3ee2-14ea-4de9-b543-753ce6b587c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6748eac-01e3-479f-be33-e228727fe105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60a3ee2-14ea-4de9-b543-753ce6b587c2",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "0ee79397-a5ba-418a-b62e-c9911125fe0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "d9bd3f81-8d66-4d49-84d6-c4e406af87bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee79397-a5ba-418a-b62e-c9911125fe0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31fcd8b-6576-4f37-8373-92cf4ab491a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee79397-a5ba-418a-b62e-c9911125fe0f",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "4b9e9ac8-0e9d-471f-b7a3-394361565361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "894edf96-390d-402b-a254-22e5cfe4e209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9e9ac8-0e9d-471f-b7a3-394361565361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c86717c-ae1f-4137-8215-6d1d33fb6f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9e9ac8-0e9d-471f-b7a3-394361565361",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "9d909330-9761-42a2-9d6d-57bb36a17ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "03bc5158-96c5-4848-b69d-cae8e616fa92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d909330-9761-42a2-9d6d-57bb36a17ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1d31f4-41d8-492b-bd83-0ce85e0f34ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d909330-9761-42a2-9d6d-57bb36a17ce4",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "79513b08-9805-4456-9062-885f8398d9b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "a6333a7c-6166-4a9c-b69c-b1148391c180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79513b08-9805-4456-9062-885f8398d9b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6001c4-6001-44ce-9f3f-27f158fcc1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79513b08-9805-4456-9062-885f8398d9b7",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "8b549ad7-03a9-4111-9d1e-8c363b2e4ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "3d57307d-072b-419b-b543-79427ecee04b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b549ad7-03a9-4111-9d1e-8c363b2e4ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e28fbad-c500-42cf-ac16-4457ab0a8d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b549ad7-03a9-4111-9d1e-8c363b2e4ea2",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "266671ae-e78d-4d7d-bf27-e624a2c300ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "28f9b4b9-2060-47d4-9a74-2555bae9b2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266671ae-e78d-4d7d-bf27-e624a2c300ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158899e3-4a05-4a40-a7ef-9171765ee040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266671ae-e78d-4d7d-bf27-e624a2c300ff",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        },
        {
            "id": "75289dea-7b4f-4565-aad0-f2c60fd768cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "compositeImage": {
                "id": "e09e63b6-dddc-4c89-bf4b-394646d309ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75289dea-7b4f-4565-aad0-f2c60fd768cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c8b9db9-5d71-4aee-8b18-f62476ab3dde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75289dea-7b4f-4565-aad0-f2c60fd768cd",
                    "LayerId": "61908257-46e3-4056-954d-f8f40a0c36fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "61908257-46e3-4056-954d-f8f40a0c36fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d04ffea-1f55-434c-aeb5-a2d73d914ea6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}