{
    "id": "a6ba9cc6-46e7-4bd0-9ad5-be45dacafb25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_degree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18ef8946-fd8d-480b-857b-ed4368f5b73c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6ba9cc6-46e7-4bd0-9ad5-be45dacafb25",
            "compositeImage": {
                "id": "45b0bdcc-dda7-4a1c-9f30-40e249b4e657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18ef8946-fd8d-480b-857b-ed4368f5b73c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d22e4b-82ef-430e-9077-895b9d063145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18ef8946-fd8d-480b-857b-ed4368f5b73c",
                    "LayerId": "7ad337f9-7ffa-4227-b487-2a756509d37c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "7ad337f9-7ffa-4227-b487-2a756509d37c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6ba9cc6-46e7-4bd0-9ad5-be45dacafb25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 84,
    "yorig": -30
}