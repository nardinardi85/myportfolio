{
    "id": "2f00d625-cb1d-4c3d-858a-962568c34032",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91e55b39-1900-41cd-8b57-24ac06a0fbc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f00d625-cb1d-4c3d-858a-962568c34032",
            "compositeImage": {
                "id": "e0496f63-13aa-411d-838e-b9b42ed6acee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e55b39-1900-41cd-8b57-24ac06a0fbc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6801055-47af-4972-b5e9-f6a93c718baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e55b39-1900-41cd-8b57-24ac06a0fbc6",
                    "LayerId": "6766c50a-acae-408d-bc2f-12f19b83d22e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6766c50a-acae-408d-bc2f-12f19b83d22e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f00d625-cb1d-4c3d-858a-962568c34032",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}