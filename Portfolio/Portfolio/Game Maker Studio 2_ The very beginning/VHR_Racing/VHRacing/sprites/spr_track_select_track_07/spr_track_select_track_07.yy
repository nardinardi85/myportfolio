{
    "id": "5cb6fb99-fb06-4475-a538-6771515eb892",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_select_track_07",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57848024-852e-4fa1-9463-896314ef8ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cb6fb99-fb06-4475-a538-6771515eb892",
            "compositeImage": {
                "id": "62674001-f7ed-4637-85f3-dbc0635282b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57848024-852e-4fa1-9463-896314ef8ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d2e909-c5ad-4e77-9de9-7668098d6c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57848024-852e-4fa1-9463-896314ef8ab9",
                    "LayerId": "a49dabd4-0403-4372-bf2e-640c5108cd3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a49dabd4-0403-4372-bf2e-640c5108cd3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cb6fb99-fb06-4475-a538-6771515eb892",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}