/// @description

#region Input freeze

if (instance_exists(obj_traffic_light)) || (instance_exists(obj_pause))
{
	exit;
}

#endregion Input freeze

// check if race time isn't set and set it
if (instance_exists(obj_gui_time))
{
	race_time_start += 1/60;
	if (race_time_rank == 0)
	{
		race_time_rank = current_time * 0.001;
	}
}

// sort rank grid
ds_grid_sort(obj_track.track_rank_grid, 1, false);

