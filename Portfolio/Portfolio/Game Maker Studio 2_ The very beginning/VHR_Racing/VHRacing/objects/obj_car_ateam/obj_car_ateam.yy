{
    "id": "bde2e729-26e8-4473-bab8-1eecdce6afe7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_car_ateam",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "6fea3281-02b7-4191-8f4d-980e1494ffc1",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "a86649ee-7790-4e30-b57c-cde7641bd97d",
            "value": "spr_car_tires_ateam"
        },
        {
            "id": "b8c82e3c-6d5a-4e57-8de1-9b0b41aff7b6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "d4263b20-7c73-4bc2-a6a9-58f759b109a7",
            "value": "2.49"
        },
        {
            "id": "871258f4-b745-4563-a72a-096c686902d0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "6e0a5c5d-4827-4acf-abae-5cf58d27e89c",
            "value": "0.3"
        },
        {
            "id": "8813601c-39a3-4ab8-9b2a-6c898e156406",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "2854bb01-6c9b-4c0c-b8b6-7b708cd48855",
            "value": "4"
        },
        {
            "id": "b36ce28b-112e-4543-b64f-5bc9339e2f10",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "fb61daf4-c85f-4d36-8b96-1451b2d38085",
            "value": "2"
        },
        {
            "id": "cdeef80c-ad83-4e15-bf41-2837edb3cc17",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "97ffff4b-92a0-4ee1-8cf3-3a557f411b56",
            "value": "2.7"
        },
        {
            "id": "8842e700-ac20-49d4-876e-30a4b462e178",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "efb6bba2-aff5-40b3-9f7d-2d5cbc6cb623",
            "value": "2"
        },
        {
            "id": "8e00c8e7-4957-4a72-922e-1e448f05b6fa",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "606e0370-ab6e-4308-a6fc-75d4426e9f08",
            "value": "100"
        },
        {
            "id": "3fc9556c-1a94-464f-9c50-e14d9dd962ec",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "58d759a5-cf68-4f13-a316-d97117d7ad5e",
            "value": "3"
        },
        {
            "id": "1d5d0f86-1929-44a0-875f-d92228ab3d81",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "d325e257-4981-4bea-840d-08c0f300b363",
            "value": "100"
        },
        {
            "id": "2178824b-62b4-4905-993d-44058ccacd8b",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "8f9db335-a901-4684-9549-c1e4bbc28d5e",
            "value": "4"
        },
        {
            "id": "307b99a4-e251-4c53-8550-c0926aaeb73d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "8e242790-f172-453a-913f-f2d384bca232",
            "value": "100"
        },
        {
            "id": "b447b9f0-33a9-459c-9399-2f5afe0c970c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "13438652-4391-4a65-889a-758f57ea5705",
            "value": "6"
        },
        {
            "id": "55f1d2e6-4143-41e4-a59b-65eaa75ba5d4",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "cb37fc8c-5d8c-4136-bb40-20745697f6cd",
            "value": "0.7"
        },
        {
            "id": "ed802760-f1d3-4024-b78e-ad88ff0ccd29",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "73ae6aa9-d082-4571-b44d-0066d372b76a",
            "value": "spr_car_race_ateam"
        },
        {
            "id": "3ae9c24d-8594-403d-97f3-8cd9483ee264",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "608a7c44-e032-4900-a294-7c02cd42f67c",
            "value": "spr_car_race_ateam_brake"
        },
        {
            "id": "7a926ba4-f158-44fd-b3fa-0be576fbc9a7",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "c35634f1-7d99-4a63-b7dc-78cf634633e2",
            "value": "spr_car_race_ateam_reverse"
        },
        {
            "id": "d65a7d2f-6b2b-4f77-a10f-d6fcb9948723",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
            "propertyId": "7797e9ad-d05c-43e5-941e-d4ce634a317b",
            "value": "spr_lights_ateam"
        }
    ],
    "parentObjectId": "d2d59a09-8013-4fb2-acd6-e9c9b2adada4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.9,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f42340bd-f922-4ac4-9959-448b1d96067b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 16
        },
        {
            "id": "d2336443-3f6f-4cb2-8139-df2b23b7e197",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 57,
            "y": 16
        },
        {
            "id": "8f118e1d-ad10-4746-8708-ef973d24cc4b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 57,
            "y": 48
        },
        {
            "id": "5d5edb2b-a16c-4fb6-bacb-6a535233acdd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "272451f6-5f44-4327-b241-c4d9f47b178e",
    "visible": true
}