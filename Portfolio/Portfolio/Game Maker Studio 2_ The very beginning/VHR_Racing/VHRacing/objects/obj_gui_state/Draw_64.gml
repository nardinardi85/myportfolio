/// @description Draw player car state machine variables

if (debug_mode == true)
{

	draw_set_halign(fa_left);
	draw_set_valign(fa_top);

	draw_text(10, 10, string_hash_to_newline("State: " + obj_game_rules.car_selected_instance.state_name +
		"#Frames: " + string(obj_game_rules.car_selected_instance.state_frames)));
}