/// @description Destroy Instance


instance_create_depth(x, y, 101, obj_gui_speed);
instance_create_depth(x, y, 101, obj_gui_lap);
instance_create_depth(x, y, 101, obj_gui_time);
instance_create_depth(x, y, 101, obj_gui_rank);

instance_destroy();
