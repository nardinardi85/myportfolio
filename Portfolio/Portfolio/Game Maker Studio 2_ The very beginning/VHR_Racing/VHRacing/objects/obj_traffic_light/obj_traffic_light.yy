{
    "id": "86afa615-6d24-4af5-b95a-628d0d1a515a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_traffic_light",
    "eventList": [
        {
            "id": "c23c7437-c855-4380-9256-5af519678d4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "86afa615-6d24-4af5-b95a-628d0d1a515a"
        },
        {
            "id": "9cb786d4-19e2-4ef8-ab48-fd9fe4fe689b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "86afa615-6d24-4af5-b95a-628d0d1a515a"
        },
        {
            "id": "2690b201-3821-441b-afbd-4a56254fcc53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86afa615-6d24-4af5-b95a-628d0d1a515a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b81b600a-ca85-445d-8b58-8b2344f35a28",
    "visible": true
}