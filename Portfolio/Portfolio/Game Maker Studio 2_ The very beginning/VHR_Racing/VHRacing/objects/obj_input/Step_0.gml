/// @description Check keyboard input and update input masks


// check pressed input
input_pressed_up = keyboard_check_pressed(key_up);
input_pressed_down = keyboard_check_pressed(key_down);
input_pressed_left = keyboard_check_pressed(key_left);
input_pressed_right = keyboard_check_pressed(key_right);
input_pressed_confirm = keyboard_check_pressed(key_confirm);
input_pressed_cancel = keyboard_check_pressed(key_cancel);
input_pressed_debug_restart = keyboard_check_pressed(key_debug_restart);

// check down input
input_down_up = keyboard_check(key_up);
input_down_down = keyboard_check(key_down);
input_down_left = keyboard_check(key_left);
input_down_right = keyboard_check(key_right);
input_down_confirm = keyboard_check(key_confirm);
input_down_cancel = keyboard_check(key_cancel);
input_down_debug_restart = keyboard_check(key_debug_restart);

// check released input
input_released_up = keyboard_check_released(key_up);
input_released_down = keyboard_check_released(key_down);
input_released_left = keyboard_check_released(key_left);
input_released_right = keyboard_check_released(key_right);
input_released_confirm = keyboard_check_released(key_confirm);
input_released_cancel = keyboard_check_released(key_cancel);
input_released_debug_restart = keyboard_check_released(key_debug_restart);

// update input masks
event_user(0);