{
    "id": "d534a33e-8313-4067-9465-469c2de2579d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_input",
    "eventList": [
        {
            "id": "e16d5875-79b2-4814-8e71-50a2944c6a33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d534a33e-8313-4067-9465-469c2de2579d"
        },
        {
            "id": "bf462089-feda-41a9-a5fc-2461023ec904",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d534a33e-8313-4067-9465-469c2de2579d"
        },
        {
            "id": "93c6feb2-4c70-4e27-bf84-b241dcc0ba4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d534a33e-8313-4067-9465-469c2de2579d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "d7c2e83c-9bb7-4bac-af48-799df3e92356",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "ord(\"W\")",
                "vk_up"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_up",
            "varName": "key_up",
            "varType": 6
        },
        {
            "id": "f76f4c3b-c6fd-4f0f-9dec-c190468a0e89",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "ord(\"S\")",
                "vk_down"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_down",
            "varName": "key_down",
            "varType": 6
        },
        {
            "id": "fc3d361b-3afc-4e27-921e-07aa08af5841",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "ord(\"A\")",
                "vk_left"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_left",
            "varName": "key_left",
            "varType": 6
        },
        {
            "id": "0b44462b-0834-4cfd-a3fd-0408aef1d719",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "ord(\"D\")",
                "vk_right"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_right",
            "varName": "key_right",
            "varType": 6
        },
        {
            "id": "a7fab9f7-c931-4e02-85ee-9e2be38e0a2a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "vk_space",
                "vk_enter"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_enter",
            "varName": "key_confirm",
            "varType": 6
        },
        {
            "id": "cb3d23c3-0609-4976-8c59-9e4a85a7da8d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "vk_backspace",
                "vk_escape"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "vk_escape",
            "varName": "key_cancel",
            "varType": 6
        },
        {
            "id": "cd68e0d2-fd38-46f5-b5a1-5ea769f81d4b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "ord(\"R\")"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "ord(\"R\")",
            "varName": "key_debug_restart",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}