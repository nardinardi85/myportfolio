/// @description Initialize car variables and state machine states

#region ParticlesCreate
// Particle System
p_system = part_system_create_layer("AI", false);
part_system_automatic_draw(p_system, true);
part_system_automatic_update(p_system, true)
part_system_position(p_system, 0, 0);

p_emit = part_emitter_create(p_system);

// Tires Particle System
tires_p_system = part_type_create();
part_type_sprite(tires_p_system, car_particle_sprite_tires, false, false, false);
part_type_life(tires_p_system, 100, 200);
part_type_alpha2(tires_p_system, 0.03, 0);


// Smoke Particle System
smoke_p_system = part_type_create();
part_type_sprite(smoke_p_system, spr_smoke, false, false, false);
part_type_size(smoke_p_system, 0.1, 1, 0.1, 0.1);
part_type_orientation(smoke_p_system, 0, 359, 1, 1, 0);
part_type_life(smoke_p_system, 10, 100);
part_type_alpha2(smoke_p_system, 0.1, 0);

// Dirt Particle System
dirt_p_system = part_type_create();
part_type_sprite(dirt_p_system, spr_dirt, false, false, false);
part_type_size(dirt_p_system, 0.1, 2, 0.02, 0.1)
part_type_life(dirt_p_system, 5, 30);
part_type_alpha2(dirt_p_system, 0.5, 0);

// Light Particle System
light_p_system = part_type_create();
part_type_sprite(light_p_system, car_particle_sprite_lights, false, false, false)
part_type_size(light_p_system, 1, 1, 0, 0);
part_type_orientation(light_p_system, 0, 0, 1, 1, 0);
//part_type_shape(light_p_system, pt_shape_flare)

part_type_life(light_p_system, 1, 3)
//part_type_color2(light_p_system, c_red, c_white)
part_type_alpha2(light_p_system, 1, 0)


// Variables
angle_dif = 0;
#endregion

// sound emitters
s_emit = audio_emitter_create();
audio_emitter_falloff(s_emit, emitter_falloff_reference_distance, emitter_falloff_max_distance, 1.5);
audio_emitter_velocity(s_emit, phy_speed_x, phy_speed_y , 0);

// initialize variables
car_starting_line = false;
car_checkpoint_counter = 0;
car_lap_current = 0;

car_rank_current = 0.0;

if (inst_game_rules.car_selected_instance == self)
{
	car_player = true;
}