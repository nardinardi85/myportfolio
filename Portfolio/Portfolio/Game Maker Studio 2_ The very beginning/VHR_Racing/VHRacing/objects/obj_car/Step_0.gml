/// @description Execute the current state script

#region Input freeze

if (instance_exists(obj_traffic_light) || instance_exists(obj_pause))
{
	exit;
}

#endregion Input freeze

if (car_player == true)
{
	if (inst_input.input_released_cancel == true)
	{
		inst_input.input_released_cancel = false;
		instance_create_depth(cam_width/2, cam_height/2, 99, obj_pause)
	}
	audio_listener_position(x, y, 0);
	
	#region CarPlayer
	if (debug_mode && inst_input.input_released_debug_restart)
	{
		game_restart();
	}

	// execute current state
	stateExecute();
	#endregion CarPlayer
}
else
{
	#region CarAI
	if (my_target)
	{
		// set image speed
		image_speed = floor(phy_speed * 0.5);
		
		t_dir = point_direction(x, y, my_target.x, my_target.y);
	
		t_x_dir = lengthdir_x(npc_speed, t_dir);
		t_y_dir = lengthdir_y(npc_speed, t_dir);
		physics_apply_force(x, y, t_x_dir, t_y_dir);
		
		audio_play_sound_on(s_emit, snd_acceleration, false, 0)
		
		phy_rotation = -t_dir;
		
		
	}
	#endregion CarAI
}


audio_emitter_position(s_emit, x, y, 0);

// emit car sounds

if (phy_speed > car_particle_speed)
{
	part_emitter_region(p_system, p_emit, x - 1, x + 1, y - 1, y + 1, ps_shape_ellipse, ps_distr_gaussian);
	inertia_dir = darctan2(phy_speed_y, phy_speed_x);
	forward_dir = phy_rotation;
	
	angle_dif = abs(angle_difference(forward_dir, inertia_dir));
	
	if (angle_dif > 20)
	{
		audio_play_sound_on(s_emit, snd_skid, false, 0)
		
		
		part_type_orientation(tires_p_system, -phy_rotation, -phy_rotation, 0, 0, 0);
		part_emitter_burst(p_system, p_emit, tires_p_system, 20);
	
	}	
}

