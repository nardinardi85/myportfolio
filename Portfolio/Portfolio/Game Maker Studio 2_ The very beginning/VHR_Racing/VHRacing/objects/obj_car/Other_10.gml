/// @description Collision with offroad tiles


// check collision with offroad tiles
if (!scr_tile_meeting(x + phy_speed_x, y, layer_tilemap_get_id("TilesTrack_1")) && !scr_tile_meeting(x + phy_speed_x, y, layer_tilemap_get_id("TilesTrack_2")) && !scr_tile_meeting(x, y + phy_speed_y, layer_tilemap_get_id("TilesTrack_1")) && !scr_tile_meeting(x, y + phy_speed_y, layer_tilemap_get_id("TilesTrack_2")))
{
	if (car_player == true && phy_speed > 0 && audio_is_playing(snd_offroad) == false)
	{
		audio_play_sound_on(s_emit, snd_offroad, false, 2);
		offroad_sound_active = true;
	}
	
	phy_linear_damping = car_offroad_linear_damping;	
	
	inertia_dir = darctan2(phy_speed_y, phy_speed_x);
	forward_dir = phy_rotation;
	
	if (floor(phy_speed) >= 1)
	{
		part_emitter_region(p_system, p_emit, x - 1, x + 1, y - 1, y + 1, pt_shape_square, ps_distr_gaussian);
		part_type_speed(smoke_p_system, 1, 2, 0, 0);
		part_type_direction(smoke_p_system, inertia_dir, inertia_dir, 0, 0);
		part_emitter_burst(p_system, p_emit, smoke_p_system, 1);
	}
}
else if (car_player == false && alarm_get(1) == -1)
{
	phy_linear_damping = npc_accel_linear_damping
}

