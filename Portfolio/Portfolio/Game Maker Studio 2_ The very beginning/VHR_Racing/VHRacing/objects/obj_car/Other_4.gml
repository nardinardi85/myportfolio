/// @description Initialize car player or ai variables

if (instance_exists(obj_car))
{
	for (var i = 0; i < 1; ++i)
	{
		audio_play_sound(snd_car_start, 3, false)
	}
}

if (car_player == true)
{
	//Warning: Player's Car Will Also Instantiate GUI Objects!
	#region CarPlayer
	
	audio_listener_orientation(0, 1, 0, 0, 0, -1)
	
	// car player variables
	wrong_way_announcer = false;

	// initialize car variables
	car_tires_direction_current = 0; // tires direction: left (-1), straight (0), right (1)
	car_tires_direction_previous = car_tires_direction_current; // tires direction value previous frame


	#region StateMachine
	// state machine initialization
	stateMachineInitialize();

	// state creation
	stateCreate("Idle", stateIdle);
	stateCreate("Acceleration", stateAcceleration);
	stateCreate("Deceleration", stateDeceleration);
	stateCreate("Brake", stateBrake);
	stateCreate("Reverse", stateReverse);
	stateCreate("Cutscene", stateCutscene);


	// set default state
	stateInitialize("Idle");
	#endregion StateMachine

#region Instantiate debug GUI objects

	if (debug_mode == true)
	{
		instance_create_layer(0, 0, "GUI", obj_gui_state);
		instance_create_layer(0, 0, "GUI", obj_gui_checkpoint);
	}
#endregion Instantiate debug GUI objects

	#endregion CarPlayer
}
else
{
	#region CarAI
		
	my_target = instance_create_depth(phy_position_x, phy_position_y, -1, obj_npc_target);
	my_target.npc = self

	//acceleration_counter = 0;
	#endregion
}