/// @description Set idle car animation, check input and change state

// check if the state is a new state and set the new sprite and physics variables
if (state_new)
{
	// change sprite
	sprite_index = car_accel_sprite;
	
	// sprite change
	image_index = 0;
	image_speed = 0;
	
	// reset car speed
	phy_linear_velocity_x = 0;
	phy_linear_velocity_y = 0;
	
	// reset car angular velocity
	phy_angular_velocity = 0;	
}

audio_play_sound_on(s_emit, snd_idle, false, 2);

// reset car dampings
phy_linear_damping = car_default_linear_damping;
phy_angular_damping = car_default_angular_damping;

// check offroad tiles collision and change linear damping
event_user(0);


// set current tires direction based on input
carTiresDirection();


// change state based on inputs
if (inst_input.input_down_up == true || inst_input.input_down_down == true)
{
	// input up
	if (inst_input.input_down_up == true)
	{
		stateSwitch("Acceleration");		
	}

	// input down
	if (inst_input.input_down_down)
	{
		stateSwitch("Reverse");
		audio_play_sound_on(s_emit, snd_idle, false, 1);
	}
}