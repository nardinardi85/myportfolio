// draw_text_outline(x, y, text, textColor, outlineColor)

var xx = argument0;
var yy = argument1;
var text = argument2;
var txt_color = argument3;
var outl_color = argument4;


draw_set_color(outl_color);
draw_text(xx - 1, yy, text);
draw_text(xx + 1, yy, text);
draw_text(xx, yy - 1, text);
draw_text(xx, yy + 1, text);

draw_set_color(txt_color);
draw_text(xx, yy, text);

