/// @description Update the current state and related variables


// check if next state isn't equal current state and set it as current state, else increase state frames counter
if (state_next != state_current)
{
	state_current = state_next;
	state_new = true;
	state_frames = 0;
}
else
{
	state_new = false;
	state_frames += 1;
}