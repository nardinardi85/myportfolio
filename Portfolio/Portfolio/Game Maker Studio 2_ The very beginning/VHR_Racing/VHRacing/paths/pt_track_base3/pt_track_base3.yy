{
    "id": "4a2c8c70-fa98-4ba6-bf4f-fa2c3c34d5be",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_track_base3",
    "closed": true,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "b337223a-421f-4348-8439-14904f6767c1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3591,
            "y": 4811,
            "speed": 100
        },
        {
            "id": "15cc230d-18c5-4dd0-bdab-2cc446b78183",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4034,
            "y": 4803,
            "speed": 100
        },
        {
            "id": "cfdc4071-8c2c-4c97-8bad-8d97e024d523",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4224,
            "y": 4800,
            "speed": 100
        },
        {
            "id": "3402e3fc-f8cd-4a52-9d48-5845ad1c3e63",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4384.05664,
            "y": 4844.058,
            "speed": 100
        },
        {
            "id": "ff76a6ed-418c-4d60-8514-f9b3feada59d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4443,
            "y": 4903,
            "speed": 100
        },
        {
            "id": "ba444a2b-8875-4a8d-bb8a-a222441ab9a6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4432.526,
            "y": 5036.0376,
            "speed": 100
        },
        {
            "id": "7665abd2-5439-4029-be06-a4ce5205b04d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4499,
            "y": 5224,
            "speed": 100
        },
        {
            "id": "1b572f76-8187-48a1-9386-84949f43a52a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4648,
            "y": 5294,
            "speed": 100
        },
        {
            "id": "021d017d-4bc6-473a-841c-5c027ec2592c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5056,
            "y": 5312,
            "speed": 100
        },
        {
            "id": "03ad9127-ce14-4bab-80c4-87ea6cb93dc2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5254.48535,
            "y": 5288.81348,
            "speed": 100
        },
        {
            "id": "5a798a4a-295f-4a23-aae3-8600963e2d22",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5464,
            "y": 5277,
            "speed": 100
        },
        {
            "id": "bc2a4645-6e7a-4f42-a0ee-21c1715e1d7f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5553,
            "y": 5150,
            "speed": 100
        },
        {
            "id": "d5378a95-2f70-404c-bb95-2806f469de2d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5551.77246,
            "y": 5050.589,
            "speed": 100
        },
        {
            "id": "9aacd75a-c59b-47cd-b11b-a0efac6f9b42",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5568,
            "y": 4352,
            "speed": 100
        },
        {
            "id": "5c31c930-58c0-412b-b489-da0cbf0b9943",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5544,
            "y": 3396,
            "speed": 100
        },
        {
            "id": "dd974de4-94d6-4a70-9e59-14a6659ff3bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5474.959,
            "y": 3322.09814,
            "speed": 100
        },
        {
            "id": "d520107f-39bc-4c14-ad6a-da57aa58adc1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5353,
            "y": 3291,
            "speed": 100
        },
        {
            "id": "3d250d89-7e1c-4da5-ac50-82da6428e892",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4960,
            "y": 3264,
            "speed": 100
        },
        {
            "id": "e70fd2e8-880e-435a-aa8f-278bac76ea17",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3232,
            "y": 3264,
            "speed": 100
        },
        {
            "id": "00934ff5-234b-4265-b204-bed5b564aee2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2933.2627,
            "y": 3235.64746,
            "speed": 100
        },
        {
            "id": "3fdac9f3-1e07-4550-bee9-cb0bd8ca35e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2801,
            "y": 3202,
            "speed": 100
        },
        {
            "id": "c6f566d0-dca1-4a01-8b2a-d09c59d3cea6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2752.487,
            "y": 3077.09644,
            "speed": 100
        },
        {
            "id": "c4d76e9e-718c-4f9f-ab5d-6b966b2ee8e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2735,
            "y": 2937,
            "speed": 100
        },
        {
            "id": "1f242d35-2ef8-47d8-9be1-5513b010cbd3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2739.17432,
            "y": 2658.725,
            "speed": 100
        },
        {
            "id": "e433f63b-b9c8-4216-bd3f-9fea869d95b5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2776,
            "y": 2559,
            "speed": 100
        },
        {
            "id": "d293e966-e875-4bc0-89eb-914e75619fb1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2913.04419,
            "y": 2484.709,
            "speed": 100
        },
        {
            "id": "0cf02d09-9466-46f8-9f80-14f7bb005cf0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3136,
            "y": 2496,
            "speed": 100
        },
        {
            "id": "fe900041-d886-4ebe-aa75-aff17a0629d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4416,
            "y": 2496,
            "speed": 100
        },
        {
            "id": "c3f85b0b-9efb-4bd3-b630-0279ce60d16a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4510.889,
            "y": 2461.42456,
            "speed": 100
        },
        {
            "id": "0a7cd3d2-c62a-42de-8944-d19ec4bab724",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4544,
            "y": 2400,
            "speed": 100
        },
        {
            "id": "23c06bd6-dba7-4b2a-832f-76fc8e6f7e6b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4538.434,
            "y": 2318.465,
            "speed": 100
        },
        {
            "id": "cc152087-4c66-4fc5-b03f-d13d9ba58a3a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4342,
            "y": 2065,
            "speed": 100
        },
        {
            "id": "e9cbc829-d576-439a-a924-7b3daff80011",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4071.36426,
            "y": 1777.585,
            "speed": 100
        },
        {
            "id": "470a724b-dbe2-48c0-ba94-bfc6ba06604e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3680,
            "y": 1376,
            "speed": 100
        },
        {
            "id": "b68ac6b9-6e57-44f6-a184-7614a7a3dabb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3262.30469,
            "y": 974.1963,
            "speed": 100
        },
        {
            "id": "0d6a10e0-dccc-4914-8d7a-78b2cb1bfa47",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2720,
            "y": 445,
            "speed": 100
        },
        {
            "id": "9903e2e8-de7f-4311-8375-cbfe6bb09373",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2623,
            "y": 402,
            "speed": 100
        },
        {
            "id": "40534acf-654a-458f-8028-d2277c6d5369",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2510,
            "y": 398,
            "speed": 100
        },
        {
            "id": "e268f1a7-06c5-4749-a21c-51693e680fb8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2388,
            "y": 413,
            "speed": 100
        },
        {
            "id": "25cef82b-a2b6-4321-a642-2b1a5ed70a1d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2219,
            "y": 545,
            "speed": 100
        },
        {
            "id": "6d2b3d66-9258-4d16-b74b-169d59f04f28",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1955,
            "y": 819,
            "speed": 100
        },
        {
            "id": "c9786483-b5c3-42bf-a546-0da60836a461",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1472,
            "y": 1280,
            "speed": 100
        },
        {
            "id": "18303760-7188-480c-90e8-394bbdbbcfdb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 1952,
            "speed": 100
        },
        {
            "id": "a8f052a2-5f39-4182-b3d8-0c096f3d0a9a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 2080,
            "speed": 100
        },
        {
            "id": "22c8cb5f-9833-4713-ad9a-ac4ab9c31ffe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 2208,
            "speed": 100
        },
        {
            "id": "1334280d-91b2-4973-9b57-caf05a19a60e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 613,
            "y": 2488,
            "speed": 100
        },
        {
            "id": "08416c7a-2cce-4843-af8c-27cfbe9b859f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 618,
            "y": 2880,
            "speed": 100
        },
        {
            "id": "c17a8cd8-a257-42bd-8b73-06ea0637e519",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 679,
            "y": 3067,
            "speed": 100
        },
        {
            "id": "0d3d6a8a-7cec-4294-88e0-cb4852d4265c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 3264,
            "speed": 100
        },
        {
            "id": "412846d2-0ebc-4b0b-a46a-15af759ba4be",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1472,
            "y": 3904,
            "speed": 100
        },
        {
            "id": "a4210af4-eadf-45a1-a9cb-73279f66e31d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2208,
            "y": 4640,
            "speed": 100
        },
        {
            "id": "5b057749-3740-4425-a37b-af3980885b31",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2336,
            "y": 4704,
            "speed": 100
        },
        {
            "id": "4ebebcad-b427-4a39-a6f4-cfc03a6503bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2496,
            "y": 4768,
            "speed": 100
        },
        {
            "id": "6db82418-b5f6-4e9e-ac13-0e22604d0b54",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2678.55176,
            "y": 4825.214,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}