{
    "id": "b4ce31b1-ee79-4f15-9039-5b640fc8fbab",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_track_base33",
    "closed": true,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "23f3a954-52ea-4608-9d25-820fa9fea907",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2848,
            "y": 4928,
            "speed": 100
        },
        {
            "id": "98b5abf5-cd4c-4103-8c57-faaa641c0b18",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4224,
            "y": 4928,
            "speed": 100
        },
        {
            "id": "3c4eb221-14d8-4442-a2c0-a5aa0372b289",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4416,
            "y": 4896,
            "speed": 100
        },
        {
            "id": "a65148df-1a3d-43e8-b9a8-fd4fea58c49f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4512,
            "y": 4832,
            "speed": 100
        },
        {
            "id": "2b9cfc8f-d231-42d9-9c0b-97e9462c5878",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5216,
            "y": 4128,
            "speed": 100
        },
        {
            "id": "8e67e787-e12e-4676-b865-7ac8d4c1938d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 4032,
            "speed": 100
        },
        {
            "id": "932317e4-82db-465a-9ff3-4c6a253f3c9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 3936,
            "speed": 100
        },
        {
            "id": "dd615396-fe25-40a2-96a1-d08f07f5c359",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 3872,
            "speed": 100
        },
        {
            "id": "4c1f7696-3e30-4683-932a-cebb2ebbc64a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 3200,
            "speed": 100
        },
        {
            "id": "5a580432-3a0e-4405-a216-ae483db453cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5280,
            "y": 3072,
            "speed": 100
        },
        {
            "id": "9ae23379-d614-478f-b44c-3eba4ab3c787",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5216,
            "y": 3008,
            "speed": 100
        },
        {
            "id": "5c5ee3d5-dd92-4c48-92d2-91442b6d0b4a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5088,
            "y": 2976,
            "speed": 100
        },
        {
            "id": "f9c49623-86e6-466c-9da8-f1caff088f7a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4992,
            "y": 2944,
            "speed": 100
        },
        {
            "id": "30fc025a-463a-4966-9f90-860f6e547e2c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4928,
            "y": 2848,
            "speed": 100
        },
        {
            "id": "ca5f89f1-9527-44cb-bc9d-fd09391a26f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4928,
            "y": 2688,
            "speed": 100
        },
        {
            "id": "4f5fc11b-9686-499a-a4ac-418c5bb2ca84",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4928,
            "y": 2560,
            "speed": 100
        },
        {
            "id": "b98d91f3-bfe7-44c0-8d2f-c4f014b4fa34",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4992,
            "y": 2496,
            "speed": 100
        },
        {
            "id": "5a4be0d4-dd4d-4d23-8ab1-5b5efc114942",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5056,
            "y": 2464,
            "speed": 100
        },
        {
            "id": "f5f0c5c5-e4b5-4837-ba02-f5d17bf2a06a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5216,
            "y": 2400,
            "speed": 100
        },
        {
            "id": "c28f9afc-93b8-4ef0-ad7d-83e0b52a93e1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5248,
            "y": 2368,
            "speed": 100
        },
        {
            "id": "b28c27d3-c5e5-4334-b577-fd85a1116880",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 2304,
            "speed": 100
        },
        {
            "id": "0a2786c0-d346-4a77-91e6-d686fc717ec2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 2176,
            "speed": 100
        },
        {
            "id": "0f3a5f1e-a852-4320-b651-681d8595c4b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 1184,
            "speed": 100
        },
        {
            "id": "9a21f0b6-d531-4d75-b310-3afe4ec68531",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5312,
            "y": 1088,
            "speed": 100
        },
        {
            "id": "b882af65-87a1-41cd-b473-10cb990bc4e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5152,
            "y": 928,
            "speed": 100
        },
        {
            "id": "7aacd690-638d-4c2c-981c-c077119472ee",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 5024,
            "y": 832,
            "speed": 100
        },
        {
            "id": "a14b2ef1-8fa7-4d95-8de2-79b2e84f9724",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4864,
            "y": 832,
            "speed": 100
        },
        {
            "id": "5263468f-7816-416d-a64e-0ead7cfefb02",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4736,
            "y": 864,
            "speed": 100
        },
        {
            "id": "de001981-b0ac-4d60-bb9c-529adbd3d4c5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4608,
            "y": 960,
            "speed": 100
        },
        {
            "id": "dbe01417-1023-444d-9bd8-2142de78e9cd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4512,
            "y": 1088,
            "speed": 100
        },
        {
            "id": "b0b2bbf4-0a12-4095-8b2e-9ee3b746fcd6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4512,
            "y": 1184,
            "speed": 100
        },
        {
            "id": "201b0d21-f5b1-414f-8461-bc846df1f0dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4512,
            "y": 1472,
            "speed": 100
        },
        {
            "id": "26bf3158-db10-48e4-ba39-8ab9157aa36a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4512,
            "y": 1536,
            "speed": 100
        },
        {
            "id": "202a1a89-dac6-451a-9c78-291c26c046b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4480,
            "y": 1600,
            "speed": 100
        },
        {
            "id": "41568af0-0fe3-4cd8-a584-2f36d69b9bc2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4384,
            "y": 1664,
            "speed": 100
        },
        {
            "id": "112e1464-6994-40e0-bdc2-9336268dec46",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4288,
            "y": 1664,
            "speed": 100
        },
        {
            "id": "940bb0ae-be97-4959-ab3c-8580f9e51a16",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3840,
            "y": 1664,
            "speed": 100
        },
        {
            "id": "6d83ac74-25a4-4751-87ea-e7bc91b3e0d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3776,
            "y": 1632,
            "speed": 100
        },
        {
            "id": "131b9bc0-7cf8-4e69-a31e-0aa8cec596ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3648,
            "y": 1568,
            "speed": 100
        },
        {
            "id": "dbda1536-bcaa-4612-884d-8b26c507a1f5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3520,
            "y": 1568,
            "speed": 100
        },
        {
            "id": "0daaa0bd-3eab-4aa7-ba94-198d43a17e44",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3360,
            "y": 1696,
            "speed": 100
        },
        {
            "id": "7359390d-6a91-4e3f-a925-67726759f373",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3232,
            "y": 1824,
            "speed": 100
        },
        {
            "id": "7c1eb401-219e-491d-a2c5-60cbf9b175f0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3136,
            "y": 1888,
            "speed": 100
        },
        {
            "id": "19979845-4a9d-4e94-a861-f3a92ea3fd27",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2944,
            "y": 1920,
            "speed": 100
        },
        {
            "id": "b53203cd-a9e2-498a-a82e-6cc1ab59be1f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2464,
            "y": 1952,
            "speed": 100
        },
        {
            "id": "7eca72d6-e4ea-49ce-bfcd-481157992572",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2304,
            "y": 1856,
            "speed": 100
        },
        {
            "id": "5252e34f-2046-4bd7-8d2f-759dd7f2a55b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1952,
            "y": 1504,
            "speed": 100
        },
        {
            "id": "db6e6f33-7d23-4717-bbf7-ccde27cc77a9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 1408,
            "speed": 100
        },
        {
            "id": "e16d3f26-a45c-43ad-bfd9-e8b3b1324454",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 1312,
            "speed": 100
        },
        {
            "id": "23353afc-8a5c-4551-97f2-7841060e3ef2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 1056,
            "speed": 100
        },
        {
            "id": "74c392d1-afb7-491d-a066-9ed23eb3d097",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1952,
            "y": 928,
            "speed": 100
        },
        {
            "id": "3ef24278-4cd0-4e51-bff2-fe323f850fd0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1984,
            "y": 768,
            "speed": 100
        },
        {
            "id": "273a0bfc-6475-46da-be70-272247052254",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1984,
            "y": 640,
            "speed": 100
        },
        {
            "id": "b07744dd-5e60-4ce3-a259-089b44647048",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1920,
            "y": 512,
            "speed": 100
        },
        {
            "id": "484b55b5-4af1-4331-9034-d8ca00983ec3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1792,
            "y": 480,
            "speed": 100
        },
        {
            "id": "f44460ae-3db4-4a1f-9770-48f69c709d61",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1184,
            "y": 448,
            "speed": 100
        },
        {
            "id": "345ff4f1-f3f9-4762-92d1-e217b2dd41cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1024,
            "y": 544,
            "speed": 100
        },
        {
            "id": "c3478ecd-47a5-4a46-a5e3-db948a2e01a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 800,
            "speed": 100
        },
        {
            "id": "ea08c2ed-ad9e-423e-8141-9b6fafbd79ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 928,
            "speed": 100
        },
        {
            "id": "2a9b594d-9413-4676-a1e5-bc084f499c00",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 1088,
            "speed": 100
        },
        {
            "id": "25bdb18d-c293-4181-95c9-cb4c527f258c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 1824,
            "speed": 100
        },
        {
            "id": "d408f249-cb0e-433e-89f3-bedfeb4d74cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 1952,
            "speed": 100
        },
        {
            "id": "f3affe47-0464-41d2-905b-ea5619aa0cb9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 2016,
            "speed": 100
        },
        {
            "id": "81529e0b-7a98-4805-9143-cf6383d47722",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 2048,
            "speed": 100
        },
        {
            "id": "243826be-51de-405e-bf68-a48c01ee5850",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 2144,
            "speed": 100
        },
        {
            "id": "e764103d-296b-4759-b42c-b723dd68e82a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1088,
            "y": 2240,
            "speed": 100
        },
        {
            "id": "6c3bcce9-50e3-4e38-a258-8560f94c155f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1088,
            "y": 2688,
            "speed": 100
        },
        {
            "id": "bdb9f143-e72b-4985-84bf-499481bc4a48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1088,
            "y": 2816,
            "speed": 100
        },
        {
            "id": "e1c31b7d-1c2e-4c4c-8b82-cf9e52f01cc6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 2880,
            "speed": 100
        },
        {
            "id": "1ac1a655-a504-4339-97b4-68b79000462e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 2912,
            "speed": 100
        },
        {
            "id": "8a7e3b18-db85-4774-b705-1110dc8307d5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 2976,
            "speed": 100
        },
        {
            "id": "6bdfe2f1-6ece-4f95-9700-0c3bea8cd59b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 3104,
            "speed": 100
        },
        {
            "id": "7cc01efb-6cc7-44a0-aa5b-fb38de3cbc9b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 3456,
            "speed": 100
        },
        {
            "id": "5dd3c080-0a0e-4007-a209-085c48de6caa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 3552,
            "speed": 100
        },
        {
            "id": "bd1ba44b-d723-4d63-98f2-feebdcee5fa3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "4e51d77a-c0a2-4ce7-bd52-9429b8dbfad9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "93bc38b8-4e78-481a-8e97-6fc48749eef4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "7491889b-b263-4d6d-b4e4-fdae71f136bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 3584,
            "speed": 100
        },
        {
            "id": "d0ab8f42-f9f2-4292-9643-e6a1f497578f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1760,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "b627b47b-9402-405b-b61f-705c0553f044",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1920,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "baa3b04a-a3cb-4685-904b-99fb97c4f324",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2048,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "0f3df615-da88-412c-9fd9-176928f147de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2208,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "947b2831-9cff-4208-a8cb-0af912e32c47",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2368,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "67d2509e-5423-4165-b05d-8c2a5ec2408a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2496,
            "y": 3616,
            "speed": 100
        },
        {
            "id": "0160b89a-5262-422b-b4ff-f39d2fd11888",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2624,
            "y": 3520,
            "speed": 100
        },
        {
            "id": "01543fc4-67c0-4e4d-bdcb-768e22ad2e63",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2720,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "e99c5f80-1a55-41ef-b532-feba08c94545",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2912,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "ad23fcd4-0cd3-4887-a5b5-4cefdcedf55e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3424,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "4df984ab-a8e2-49fc-b323-901c84b46217",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3552,
            "y": 3488,
            "speed": 100
        },
        {
            "id": "16e6a530-888a-4d7a-afcc-ffe3b9104da7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3648,
            "y": 3552,
            "speed": 100
        },
        {
            "id": "88e8fcda-d643-425b-a1c7-0ff28350e309",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3776,
            "y": 3680,
            "speed": 100
        },
        {
            "id": "dacd8dc6-edd5-465b-9c9a-fcba2badf213",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3840,
            "y": 3744,
            "speed": 100
        },
        {
            "id": "e21211f2-d7af-4c44-804a-113f57986924",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3840,
            "y": 3776,
            "speed": 100
        },
        {
            "id": "e77d2b52-3938-40be-b79a-94578b346c74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3840,
            "y": 3840,
            "speed": 100
        },
        {
            "id": "1ffbd6f8-9907-4439-894b-af18caaa0e00",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3840,
            "y": 3968,
            "speed": 100
        },
        {
            "id": "530aef55-0385-480c-927b-06e364848240",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3808,
            "y": 4064,
            "speed": 100
        },
        {
            "id": "42a01f7d-3469-4ae5-bdbe-c5cc62a756d4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3712,
            "y": 4160,
            "speed": 100
        },
        {
            "id": "af5cbe3d-fe89-48b4-a64a-652fea9c3523",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3584,
            "y": 4256,
            "speed": 100
        },
        {
            "id": "6be4e0c1-58a2-431b-82c7-397662db49d7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3488,
            "y": 4288,
            "speed": 100
        },
        {
            "id": "95958dc0-938c-4d80-8f79-8e9597ec7f9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3424,
            "y": 4288,
            "speed": 100
        },
        {
            "id": "54e37100-c7e2-4cda-8451-8fecb7e758cd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1792,
            "y": 4288,
            "speed": 100
        },
        {
            "id": "2ffe8809-ec84-47ee-8dc7-b9bc0b768fc3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1664,
            "y": 4256,
            "speed": 100
        },
        {
            "id": "199e8982-009e-4bf7-a36f-c57b88a3704f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 4096,
            "speed": 100
        },
        {
            "id": "fadd0384-1de3-4b92-a996-394dfb5f8c9f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1408,
            "y": 4032,
            "speed": 100
        },
        {
            "id": "df6d71e7-22be-4b0c-af12-7152d6afbcfc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1344,
            "y": 4032,
            "speed": 100
        },
        {
            "id": "3881a135-9021-434f-ab70-02905bdf1301",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 4032,
            "speed": 100
        },
        {
            "id": "b9e083b8-6575-4050-93e7-28f1697590d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 4064,
            "speed": 100
        },
        {
            "id": "840bddf2-c155-441f-be7d-b3b0c4b6ad98",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 4128,
            "speed": 100
        },
        {
            "id": "299a1006-9a15-405e-874a-74d7e4ce2838",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 4192,
            "speed": 100
        },
        {
            "id": "53fe3424-86b4-444f-964f-024d082cbcfd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 4576,
            "speed": 100
        },
        {
            "id": "aa1c7023-7f7a-4054-8ccb-aea33d90b56a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 4704,
            "speed": 100
        },
        {
            "id": "53cf1891-8a6f-4848-840e-6d0f1d8c5242",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 4736,
            "speed": 100
        },
        {
            "id": "efd4db87-482d-40a8-a573-ebc6c2af1d99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1024,
            "y": 4736,
            "speed": 100
        },
        {
            "id": "c07ccc88-cf66-47e5-8fa8-e85fe21868d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 4704,
            "speed": 100
        },
        {
            "id": "4c6b1a64-ed9e-48dc-816a-bd5959798648",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 4576,
            "speed": 100
        },
        {
            "id": "67d10a79-cc56-4627-bd34-f3d6c4279f50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1344,
            "y": 4544,
            "speed": 100
        },
        {
            "id": "9a2b78a1-82cd-4e00-bdd9-ea88ed694d86",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1472,
            "y": 4544,
            "speed": 100
        },
        {
            "id": "e0ecb9c3-5376-47eb-beda-cf08563cab00",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1536,
            "y": 4640,
            "speed": 100
        },
        {
            "id": "4fb13d06-305a-4260-948b-4704b728a09a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1568,
            "y": 4768,
            "speed": 100
        },
        {
            "id": "36fbcd66-6ff0-472d-8bec-99d682a3314d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1600,
            "y": 4832,
            "speed": 100
        },
        {
            "id": "4eb6b1ad-934f-4b4a-912d-20534ae1f9e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1696,
            "y": 4928,
            "speed": 100
        },
        {
            "id": "c966b7d4-b7a2-446c-b77e-1fba55980e29",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1888,
            "y": 4928,
            "speed": 100
        },
        {
            "id": "3517920b-7173-440f-8f9e-1c0e4d8cbf04",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2528,
            "y": 4928,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}