/// @description Sottrae HP

if (obj_player.player_dying = 0)
{

if obj_hp.life_player >= 1
{
	
if (obj_player.hp_player <= 1)
{
	obj_player.hp_player--;
	obj_player.player_dying += 1;
	//room_goto(rm_game_over);
	//audio_sound_pitch(snd_game_over, random_range(0.8, 1.2));
	//audio_play_sound(snd_game_over, 1, false);
	timeline_index = tm_player_permadeath;
	timeline_running = true;
	timeline_position = 0;
	with (obj_score)
	{
		exit;
	}
}
else if (obj_player.hp_player > 1)
{
	obj_player.player_dying += 1;
	obj_player.hp_player--;
	timeline_index = tm_player_dying;
	timeline_running = true;
	timeline_position = 0;
	
}
}
}
//obj_player.x = obj_player.xstart;
//obj_player.y = obj_player.ystart;