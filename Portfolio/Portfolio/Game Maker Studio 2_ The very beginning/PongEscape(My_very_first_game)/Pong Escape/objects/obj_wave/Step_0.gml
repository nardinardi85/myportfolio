/// @description interazioni
if(global.pause) exit;
if(global.v321) exit;
x = x+wspd; 

wspd = wave_speed;
if (obj_player.hsp > 0)
{
wspd = wave_speed;

if (obj_player.x - x > 240)
{
	wspd = obj_player.hsp + 1;
}
}


#region Modificatore Suoni
//modificatore HI
if (obj_player.x - x <= 100)
{
	audio_play_sound(snd_wave_hi,3,true);
	audio_stop_sound(snd_wave_med);
	audio_stop_sound(snd_wave_low);

}
//modificatore MED
else if (obj_player.x - x > 100) && (obj_player.x - x <=200)
{
	audio_play_sound(snd_wave_med,2,true);
	audio_stop_sound(snd_wave_hi);
	audio_stop_sound(snd_wave_low);
	
}
//modificatore LOW
else if (obj_player.x - x > 200)
{
	audio_play_sound(snd_wave_low,1,true);
	audio_stop_sound(snd_wave_med);
	audio_stop_sound(snd_wave_hi);

}
#endregion