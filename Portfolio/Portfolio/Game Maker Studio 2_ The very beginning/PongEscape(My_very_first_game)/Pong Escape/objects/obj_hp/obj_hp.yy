{
    "id": "f0dcbdbd-d098-4c24-86de-cc10b6f6c009",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hp",
    "eventList": [
        {
            "id": "b6fd8824-e885-4e3d-84b2-28636087342b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f0dcbdbd-d098-4c24-86de-cc10b6f6c009"
        },
        {
            "id": "7cb66c57-f35f-494e-9999-56ca018db99e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f0dcbdbd-d098-4c24-86de-cc10b6f6c009"
        },
        {
            "id": "99652d33-66e7-4e36-9162-7ebc7ba9c5f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0dcbdbd-d098-4c24-86de-cc10b6f6c009"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4933730-ba75-4604-86d4-7378c0251150",
    "visible": true
}