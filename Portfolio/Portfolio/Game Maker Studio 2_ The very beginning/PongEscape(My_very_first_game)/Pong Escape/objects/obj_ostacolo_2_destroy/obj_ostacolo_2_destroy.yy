{
    "id": "93df4409-e49c-41c5-8fb8-a0fcc1cf10b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ostacolo_2_destroy",
    "eventList": [
        {
            "id": "f9bb2618-eeeb-4ed0-a2d3-146ee95b754d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "93df4409-e49c-41c5-8fb8-a0fcc1cf10b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e75fca8-5bf0-40e2-a3e7-ed7ec2403caf",
    "visible": true
}